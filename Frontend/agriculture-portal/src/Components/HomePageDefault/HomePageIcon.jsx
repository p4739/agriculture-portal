import {
  Stack,
  Grid,
  Card,
  CardMedia,
  CardContent,
  Typography,
} from '@mui/material'

import React from 'react'

export default function HomePageIcon() {
  const Icons = [
    {
      id: 1,
      title: 'Fast Delivery',
      icon: 'https://img.icons8.com/external-konkapp-detailed-outline-konkapp/344/external-fast-delivery-logistic-and-delivery-konkapp-detailed-outline-konkapp.png',
      subtitle: 'Start from ₹40',
    },
    {
      id: 2,
      title: 'Fresh & Genuine',
      icon: 'https://cdn-icons-png.flaticon.com/512/4497/4497553.png',
      subtitle: '100% fresh and organic',
    },
    {
      id: 1,
      title: 'Payment',
      icon: 'https://img.icons8.com/external-justicon-lineal-justicon/344/external-payment-security-cryptocurrency-justicon-lineal-justicon.png',
      subtitle: '100% secured',
    },
  ]
  return (
    <div>
      <Grid container spacing='20'>
        {Icons.map((item, i) => (
          <Grid key={i} item xs={{ sm: 12, md: 4 }}>
            <Card raised={true} sx={{ paddingLeft: 5, borderRadius: 2 }}>
              <Stack direction='row'>
                <CardMedia
                  sx={{ width: 60, height: 60, mt: [0,2] }}
                  component='img'
                  image={item.icon}
                ></CardMedia>
                <CardContent sx={{ width: 150 }}  >
                  <Typography variant='h6' fontSize={17}>
                    {item.title}
                  </Typography>
                  <Typography
                    variant='caption text'
                    fontSize={14}
                    color='#7C869C'
                    sx={{display:['none','contents']}}
                  >
                    {item.subtitle}
                  </Typography>
                </CardContent>
              </Stack>
            </Card>
          </Grid>
        ))}
      </Grid>
    </div>
  )
}
