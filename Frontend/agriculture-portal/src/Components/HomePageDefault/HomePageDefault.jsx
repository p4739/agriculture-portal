import { Box, Container, Grid, GridListTile, ListSubheader } from "@mui/material";
import { useEffect } from "react"
import Carousel from "react-material-ui-carousel"
import Footer from "../Footer"

import HomePageCarousel from "../HomePageCarousel";

import SubCategory from "../SubCategory";
import HomePageFeaturedItems from "./HomePageFeaturedItems";
import HomePageIcon from "./HomePageIcon";
import ShopByCategory from "./ShopByCategory";
import TrendingProducts from './TrendingProducts'
function HomePageDefault({ subCategory, category, CategoryIcons }) {
  const UserId = 212;
//  console.log(subCategory)
  useEffect(() => {
    
  }, []);

  return (
    <Box >
      <Grid container spacing={2} >
        <Grid item xs="12" sx={{mb:{xs:0,sm:0,md:2}}}>
          <HomePageCarousel subCategory={subCategory} />
        </Grid>
        <Grid item xs="12" sx={{mb:[0,2]}}>
          <HomePageIcon />
        </Grid>
        <Grid item xs="12">
          <ShopByCategory category={category} CategoryIcons={CategoryIcons} subCategory={subCategory}  />
        </Grid>
         <Grid item xs="12" sx={{mb:0}}>
          <HomePageFeaturedItems subcats={subCategory} useId={UserId} />
        </Grid>
        <Grid item xs="12" sx={{mb:3}}>
          <TrendingProducts subcats={subCategory} useId={UserId} />
        </Grid>
      </Grid>
      <Footer/>
    </Box>
  );
}

export default HomePageDefault;
