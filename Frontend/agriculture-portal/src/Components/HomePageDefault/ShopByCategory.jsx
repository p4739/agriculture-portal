import { Box, Divider, Grid, ImageList, ImageListItem, Stack, Typography } from '@mui/material'
import React from 'react';
import Category from '../Category'

export default function ShopByCategory(props) {

 const category = props.category
 //debugger
 //console.log(Array.isArray(category))
 const categoryIcons = props.CategoryIcons
 //console.log(categoryIcons[1].icon)

  return <Box >
      <Box sx={{mt:5,mb:2}}>
          <Typography variant='h4' fontWeight={{xs: '600', md:'600'}} color = '#2A3444'>
          Shop By Category
          </Typography>
          <Typography variant='caption' color='#C0969C' >
          All the categories available in your Region !
          </Typography>
      </Box>
      {/* <Divider color="success" sx={{mb:2}}/> */}
      <Grid container spacing={2}>
       {/* <ImageList cols={3} sx={{padding:2,mb:2}} variant='woven'> */}
      {category.map((cat,i)=>(
          <Grid item key={i}> 
          {/* <ImageListItem> */}
        <Category cat={cat} icon={categoryIcons[i].icon} subCategory={props.subCategory}></Category>
          {/* </ImageListItem> */}
          </Grid>
      ))}
      {/* </ImageList> */}
      </Grid>
     

     

  </Box>;
}
