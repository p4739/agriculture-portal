import {
  Box,
  Button,
  Card,
  Divider,
  Grid,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
//import Carousel from "react-material-ui-carousel"
//import Carousel from "react-elastic-carousel";
import Carousel from "react-multi-carousel";
import SubCategory from "../SubCategory";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import 'react-multi-carousel/lib/styles.css'
import Avatar from "@material-ui/core/Avatar";


export default function HomePageFeaturedItems({ subcats, userId }) {
  //console.log(subcats);
  const subcategory = [...subcats];
 // console.log(subcategory);
  const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1280, itemsToShow: 3 },
  ];
  const myArrow = ({ type, onClick, isEdge }) => {
    const pointer =
      type === "PREV" ? (
        <ArrowBackIosNewIcon color="success" />
      ) : (
        <ArrowForwardIosIcon color="success" />
      );
    return (
      <Button
        width={5}
        sx={{ padding: 1, margin: "auto" }}
        onClick={onClick}
        disabled={isEdge}
      >
        {pointer}
      </Button>
    );
  };
 
  const x = 4;
  return (
    <Box>
      <Box sx={{ mt: 5, mb: 2 }}>
        <Typography
          variant="h4"
          fontWeight={{ xs: "600", md: "600" }}
          color="#2A3444"
        >
          Featured Products
        </Typography>
        <Typography variant="caption" color="#C0969C">
          Our Best selling products...
        </Typography>
      </Box>
      <Divider color="success" sx={{ mb: 2 }} />
     

      <Carousel
        additionalTransfrom={0}
        arrows
        autoPlaySpeed={3000}
        centerMode={false}
        className=""
        containerClass="container-with-dots"
        dotListClass=""
        draggable
        focusOnSelect={false}
        infinite
        itemClass=""
        keyBoardControl
        // customDot={<CustomDot />}
        minimumTouchDrag={80}
        renderButtonGroupOutside={false}
        renderDotsOutside={false}
        responsive={{
          desktop: {
            breakpoint: {
              max: 3000,
              min: 1024,
            },
            items: 3,
            partialVisibilityGutter: 40,
          },
          desktopSM: {
            breakpoint: {
              max: 1024,
              min: 900,
            },
            items: 2,
            partialVisibilityGutter: 40,
          },
          tabletL: {
            breakpoint: {
              max: 900,
              min: 700,
            },
            items: 3,
            partialVisibilityGutter: 30,
          },
          mobile: {
            breakpoint: {
              max: 464,
              min: 0,
            },
            items: 1,
            partialVisibilityGutter: 30,
          },
          
          tablet: {
            breakpoint: {
              max: 700,
              min: 464,
            },
            items: 2,
            partialVisibilityGutter: 30,
          },
        }}
        showDots={false}
        sliderClass=""
        slidesToSlide={1}
        swipeable
      >
        {subcats.slice(0, 4).map((item,i) => {
          return (
            <Grid item  key= {i}>
              <SubCategory item={item} UserId={userId} />
            </Grid>
          );
        })}
      </Carousel>
     
     
    </Box>
  );
}
