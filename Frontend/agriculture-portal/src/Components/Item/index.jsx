import {
  Button,
  Grid,
  IconButton,
  Snackbar,
  Stack,
  Typography,
} from '@mui/material'
import Container from '@mui/material/Container'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import {
  useLayoutEffect,
  useEffect,
  useState,
  forwardRef,
  useContext,
} from 'react'
import './index.css' //needed for image structuring
import jwt_decode from 'jwt-decode'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import FormHelperText from '@mui/material/FormHelperText'
import Select from '@mui/material/Select'
import FormControl from '@mui/material/FormControl'
import Dialog from '@mui/material/Dialog'
import { styled } from '@mui/material/styles'
import CurrencyRupeeIcon from '@mui/icons-material/CurrencyRupee'
import AddIcon from '@mui/icons-material/Add'
import RemoveIcon from '@mui/icons-material/Remove'
import axios from 'axios'
import MuiAlert from '@mui/material/Alert'
import 'react-toastify/dist/ReactToastify.css'
import { useLocation, useNavigate } from 'react-router-dom'
import Footer from '../Footer'
import Login from '../../Pages/Signin'
import { CartQuantityFlagContext } from '../../Pages/HomePage'

//Testing purpose Login page imported
//import Login from './Login'

export default function Item(props) {
  const { state } = useLocation()
  const flagContext = useContext(CartQuantityFlagContext)
  const { cartQuantityUpdateFlag, setCartQuantityUpdateFlag } = flagContext
  console.log(state.item)
  const [ProductImage, setProductImage] = useState('')
  const [userId, setUserId] = useState(null) //Set User Id
  const [IsAvailable, setIsAvailable] = useState(false)
  const [ItemCount, setItemCount] = useState(0)
  const [IsVarietyAvailable, setVarietyAvailable] = useState(false)
  const [Variety, setVariety] = useState([])
  const [SelectedVariety, setSelectedVariety] = useState(null)
  const [AlertMessage, setAlertMessage] = useState()
  const [AlertType, setAlertType] = useState('warning')
  const [openSnackbar, setOpenSnackbar] = useState(false)

  let subCategoryId = null
  let varietyId = null

  // const subCategory = {
  //   id: 3,
  //   Name: 'Rice',
  //   Category_id: 1,
  //   images: [
  //     'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRAeCeJYSA98sY-xP2m3NBUKoY6_SPOUNsRFw&usqp=CAU',
  //     'https://vedangprojectimages.s3.amazonaws.com/logo.png',
  //     'https://images.pexels.com/photos/5486/bird-s-eye-view-cars-crossing-crossroad.jpg?auto=compress&cs=tinysrgb&dpr=2&h=100&w=200',
  //     'https://images.pexels.com/photos/914128/pexels-photo-914128.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=100&w=200',
  //   ],
  //   Description:
  //     "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  //   Measurement_id: '1',
  //   Price: '250.00',
  //   hasVariety: 0,
  // }

  const [Price, setItemPrice] = useState(0)

  //fetch the variety
  function fetchVariety() {
    subCategoryId = state.item.subCategoryId
    const body = { subCategoryId, userId }
    //console.log('fetch variety : ' + SelectedVariety)
    const url = 'http://localhost:4000/item/fetch-variety'
    axios.post(url, body).then((response) => {
      //console.log(response)
      const result = response.data
      if (result.status === 'error') {
        setVarietyAvailable(false)
      }
      if (result.status === 'success') {
        if (result.result.length > 0) {
          setVarietyAvailable(true)
          setVariety(result.result)
        }
      }
    })
  }
  useEffect(() => {
    fetchVariety()
  }, [userId])

  //check if item is available
  function isItemAvailable() {
    subCategoryId = state.item.subCategoryId
    //need to also set variety id
    const body = { subCategoryId, userId }
    //console.log('is item Available : ' + userId)
    const url = 'http://localhost:4000/item/check-item-availability'
    axios.post(url, body).then((response) => {
      //console.log(response)
      const result = response.data
      if (result.status === 'error') {
        setIsAvailable(false)
      }
      if (result.status === 'success') {
        if (result.result.length > 0) {
          setIsAvailable(true)
        }
      }
    })
  }
  useEffect(() => {
    isItemAvailable()
  }, [userId])

  //fetch the item quantity from cart if it exist
  function fetchItemQuantity() {
    subCategoryId = state.item.subCategoryId
    varietyId = SelectedVariety
    const body = { subCategoryId, userId, varietyId }
    // console.log(
    //   'fetchItemQuantity(if user has loggedin) : ' + subCategoryId,
    //   varietyId,
    //   userId
    // )
    const url = 'http://localhost:4000/item/fetch-item-count'
    axios.post(url, body).then((response) => {
      //console.log(response)
      const result = response.data
      if (result.status === 'error' && userId != null) {
        setItemCount(0)
      }
      if (result.status === 'success') {
        //setItemCount
        if (result.quantity != null) {
          //console.log(typeof result.quantity)
          setItemCount(parseInt(result.quantity))
        } else {
          setItemCount(0)
        }
      }
    })
  }
  useEffect(() => {
    fetchItemQuantity()
  }, [userId, SelectedVariety])

  //fetch variety details like price
  function fetchVarietyDetails() {
    subCategoryId = state.item.subCategoryId
    varietyId = SelectedVariety
    const body = { subCategoryId, varietyId }
    // console.log(
    //   'fetchVarietyDetails(once variety is selected) : ' + subCategoryId,
    //   varietyId
    // )
    const url = 'http://localhost:4000/item/fetch-variety-details'
    axios.post(url, body).then((response) => {
      const result = response.data
      if (result.status === 'error') {
        setItemPrice(state.item.price)
      }
      if (result.status === 'success') {
        //set price and description
        //console.log(result.status)
        setItemPrice(result.data.price)
      }
    })
  }
  useEffect(() => {
    fetchVarietyDetails()
  }, [SelectedVariety])

  //Will fetch the user details based on token in session storage. Also will fetch the cart details if userId exist
  async function FetchUserDetails() {
    //will need to later fetch it from session storage
    // window.sessionStorage.setItem(
    //   'token',
    //   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyIiwiRk4iOiJKb2huIiwiTE4iOiJQb2ludGluZyIsIlN0YXRlIjoiR29hIiwiUm9sZSI6MX0.9QH5Be4LN0fJEtWd-d923CBIf7o7DFFjyznZuC6EkLw'
    // )
    //console.log('inside fetch user details')
    const Token = await window.sessionStorage.getItem('token')
    if (Token == null) {
      setUserId(null)
    } else {
      if (userId == null) {
        const DecodedToken = await jwt_decode(Token)
        setUserId(DecodedToken.data.userId)
      }
    }
  }
  //This hook will run everytime there is a change is UserId and will call FetchUserDetails function
  useLayoutEffect(() => {
    FetchUserDetails()
  }, [userId])

  //will set the images
  useEffect(() => {
    setProductImage(state.item.imageId[0].link)
    setItemPrice(state.item.price)
  }, [])

  //used for dialog opening and closing
  const [open, setOpen] = useState(false)
  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = (value) => {
    setOpen(false)
  }

  const SubCategoryDescription = state.item.description
    .replace('.', '.#')
    .split('#')

  const setImageFullSize = (e) => {
    if (e.target.src !== ProductImage) {
      //setSelectedImage(e.target.dataset.index)
      setProductImage(e.target.src)
    }
  }

  //Checks done before adding item to cart
  function addItemToCart() {
    if (userId == null) {
      handleClickOpen()
    } else {
      //console.log(Variety.length, SelectedVariety)
      if (
        Variety.length != 0 &&
        (SelectedVariety == null || SelectedVariety == '')
      ) {
        setAlertType('error')
        setAlertMessage('Variety not selected')
        setOpenSnackbar(true)
      } else {
        addItem()
      }
    }
  }

  //Add item to cart
  function addItem() {
    subCategoryId = state.item.subCategoryId
    let varietyId = SelectedVariety
    const body = { subCategoryId, varietyId, userId }
    //console.log('Available : ' + userId)
    const url = 'http://localhost:4000/item/add-item-to-cart'
    axios.post(url, body).then((response) => {
      //console.log(response)
      const result = response.data
      if (result.status === 'error') {
        setAlertType('warning')
        setAlertMessage('Max quantity reached for the item')
        setOpenSnackbar(true)
      }
      if (result.status === 'success') {
        setItemCount(ItemCount + 1)
        setCartQuantityUpdateFlag(!cartQuantityUpdateFlag)
      }
    })
  }

  //Delete item from cart
  function delItem() {
    subCategoryId = state.item.subCategoryId
    let varietyId = SelectedVariety
    const body = { subCategoryId, varietyId, userId }
    //console.log('Available : ' + userId)
    const url = 'http://localhost:4000/item/del-item-from-cart'
    axios.post(url, body).then((response) => {
      //console.log(response)
      const result = response.data
      if (result.status === 'error') {
        setAlertType('warning')
        setAlertMessage('Min quantity has been reached')
        setOpenSnackbar(true)
      }
      if (result.status === 'success') {
        setItemCount(ItemCount - 1)
        setCartQuantityUpdateFlag(!cartQuantityUpdateFlag)
      }
    })
  }

  const StyledSelect = styled(Select)(({ theme }) => ({
    '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
      borderColor: '#2e7d32',
    },

    width: 200,
  }))

  const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant='filled' {...props} />
  })
  const navigate = useNavigate()
  return (
    <div style={{ backgroundColor: '#F6F9FC', textAlign: 'left' }}>
      <Container
        maxWidth={false}
        sx={{
          maxWidth: '1280px',
          marginBottom: '2rem',
        }}
      >
        <Grid container spacing={3} lineHeight={1.5}>
          <Grid item md={6} width={'100%'}>
            <Stack>
              <Box
                sx={{
                  marginBottom: 6,
                  textAlign: 'center',
                  position: 'relative',
                }}
              >
                <img
                  src={ProductImage}
                  alt='Target'
                  height={175}
                  width={175}
                  style={{ padding: 60 }}
                ></img>
              </Box>
              <Stack direction='row' spacing={2} justifyContent='center'>
                {state.item.imageId.map((image) => (
                  <Box
                    key={image}
                    sx={{
                      borderRadius: '10px',
                      position: 'relative',
                      lineHeight: 0,
                      border: '1px solid rgb(218, 225, 231)',
                    }}
                  >
                    <img
                      src={image.link}
                      alt='Thumbnail'
                      height={40}
                      width={40}
                      className={
                        ProductImage == image
                          ? 'SelectedImage'
                          : 'DeSelectedImage'
                      }
                      onClick={(e) => {
                        setImageFullSize(e)
                      }}
                    />
                  </Box>
                ))}
              </Stack>
            </Stack>
          </Grid>
          <Grid item md={6} width={'100%'}>
            <Grid item md={12} marginBottom={3} marginTop={5}>
              <Typography
                variant='h4'
                sx={{ fontWeight: 700, textTransform: 'capitalize' }}
              >
                {state.item.name}
              </Typography>
            </Grid>
            <Grid item md={12} marginBottom={3} fontWeight={400}>
              {IsVarietyAvailable && (
                <FormControl
                  required
                  size='small'
                  color='success'
                  sx={{
                    borderColor: 'red',
                    '& .MuiFormLabel-root': {
                      color: 'none',
                    },
                    '& .Mui-focused': {
                      color: '#2e7d32',
                    },
                  }}
                >
                  <InputLabel id='demo-simple-select-filled-label'>
                    Variety
                  </InputLabel>
                  <StyledSelect
                    labelId='demo-simple-select-required-label'
                    label='Variety'
                    className={'select'}
                    value={SelectedVariety === null ? '' : SelectedVariety}
                    onChange={(e) => {
                      setSelectedVariety(e.target.value)
                      //console.log(e.target.value)
                    }}
                  >
                    <MenuItem value=''>
                      <em>None</em>
                    </MenuItem>
                    {Variety.map((e) => {
                      return (
                        <MenuItem value={e.varietyId} key={e.varietyId}>
                          {e.name}
                        </MenuItem>
                      )
                    })}
                  </StyledSelect>
                  <FormHelperText>Required</FormHelperText>
                </FormControl>
              )}
            </Grid>
            <Grid
              item
              md={12}
              fontSize={'25px'}
              lineHeight={1}
              fontWeight={700}
              color={'#2e7d32'}
            >
              <CurrencyRupeeIcon />
              {Price}
            </Grid>
            {IsAvailable && (
              <Grid item md={12} fontWeight={250}>
                Stock Available
              </Grid>
            )}
            {!IsAvailable && (
              <Grid item md={12} fontWeight={250}>
                Item currently out of stock for this location
              </Grid>
            )}
            <Grid item md={12} marginTop={4}>
              {IsAvailable && ItemCount === 0 && (
                <Button
                  variant='contained'
                  color='success'
                  padding='6px 1.75rem'
                  sx={{
                    textTransform: 'capitalize',
                    height: '40px',
                    paddingLeft: '28px',
                    paddingRight: '28px',
                  }}
                  onClick={() => {
                    //setItemCount(ItemCount + 1)
                    addItemToCart()
                    //call add item
                  }}
                >
                  Add To Cart
                </Button>
              )}

              {IsAvailable && ItemCount !== 0 && (
                <Stack direction='row' spacing={2.5}>
                  <IconButton
                    aria-label='remove from cart'
                    sx={{ padding: 0, '&:hover': { borderRadius: '4px' } }}
                    onClick={() => {
                      delItem()
                    }}
                  >
                    <RemoveIcon
                      color='success'
                      sx={{
                        border: 1,
                        borderRadius: '4px',
                        padding: '6px',
                        fontSize: '1.10em',
                      }}
                    />
                  </IconButton>
                  <Typography variant='h5' sx={{ alignSelf: 'center' }}>
                    {ItemCount}
                  </Typography>
                  <IconButton
                    aria-label='add to cart'
                    sx={{ padding: 0, '&:hover': { borderRadius: '4px' } }}
                    onClick={() => {
                      addItemToCart()
                    }}
                  >
                    <AddIcon
                      color='success'
                      sx={{
                        border: 1,
                        borderRadius: '4px',
                        padding: '6px',
                        fontSize: '1.10em',
                      }}
                    />
                  </IconButton>
                </Stack>
              )}

              {IsAvailable && ItemCount !== 0 && (
                <Button
                  variant='contained'
                  color='success'
                  padding='6px 1.75rem'
                  sx={{
                    textTransform: 'capitalize',
                    height: '40px',
                    paddingLeft: '28px',
                    paddingRight: '28px',
                    marginTop: '28px',
                  }}
                  onClick={() => {
                    navigate('/cart')
                  }}
                >
                  Proceed To checkout
                </Button>
              )}
            </Grid>
            <Grid item md={12}>
              <Box sx={{ width: '100%' }}>
                <Box
                  sx={{
                    borderBottom: 1,
                    borderColor: 'divider',
                    marginTop: 10,
                    marginBottom: 3,
                  }}
                >
                  <Tabs value={false}>
                    <Tab
                      label='Description'
                      sx={{
                        color: 'green',
                        lineHeight: 1.25,
                        borderBottom: 3,
                        textTransform: 'capitalize',
                      }}
                    />
                  </Tabs>
                </Box>
                <Box>
                  {SubCategoryDescription.map((text) => (
                    <Typography key={text}>{text}</Typography>
                  ))}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Container>
      <br />
      {/*Used to display the dialog box that shows login component*/}

      <Dialog open={open} onClose={handleClose} fullWidth>
        <Login />
      </Dialog>
      <Snackbar //Will display snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={() => {
          setOpenSnackbar(false)
        }}
      >
        <Alert severity={AlertType} sx={{ width: '100%' }}>
          {AlertMessage}
        </Alert>
      </Snackbar>
      <Footer />
      <Box
        sx={(theme) => ({
          [theme.breakpoints.down('md')]: {
            marginBottom: 7,
          },
          [theme.breakpoints.up('md')]: {
            marginBottom: 0,
          },
        })}
      ></Box>
    </div>
  )
}
