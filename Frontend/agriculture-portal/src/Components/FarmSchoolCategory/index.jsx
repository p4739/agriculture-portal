import { useNavigate } from 'react-router-dom'
import ImageListItem from '@mui/material/ImageListItem'
import ImageListItemBar from '@mui/material/ImageListItemBar'
import { styled } from '@mui/material/styles'
import { IconButton } from '@mui/material'

const MyImageListItem = styled(ImageListItem)`
  ${({ theme }) => `
  cursor: pointer;
  background-color: ${theme.palette.primary.main};
  transition: ${theme.transitions.create(['background-color', 'transform'], {
    duration: theme.transitions.duration.complex,
    easing: theme.transitions.easing.sharp,
  })};
  &:hover {
    background-color: ${theme.palette.secondary.main};
    transform: scale(1.05);
    minwidth: "110%";
  }
  `}
`

const FarmSchoolCategory = (props) => {
  const { category } = props

  const navigate = useNavigate()

  return (
    <div>
      <MyImageListItem
        key={category.categoryId}
        onClick={() => {
          navigate('/utilpage/FarmSchoolHome/FarmSchoolSubType', {
            state: { id: category.categoryId, flag: category.isMachinery },
          })
        }}
      >
        <img
          src={`${category.url}?w=350?&fit=crop&auto=format`}
          srcSet={`${category.url}`}
          alt={category.name}
          loading='lazy'
        />
        <ImageListItemBar
          title={category.name}
          actionIcon={
            <IconButton
              sx={{
                color: 'rgba(255, 255, 255, 0.54)',
              }}
            ></IconButton>
          }
        />
        {/* <ImageListItemBar position="below" title={category.name} /> */}
      </MyImageListItem>
    </div>
  )
}

export default FarmSchoolCategory
