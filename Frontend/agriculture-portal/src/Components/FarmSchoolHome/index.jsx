import { useEffect, useState } from 'react'
import FarmSchoolCategory from '../FarmSchoolCategory'
//import Container from "@mui/material/Container";
import { Box } from '@mui/system'
import ImageList from '@mui/material/ImageList'
import Typography from '@mui/material/Typography'
import { Container, Stack } from '@mui/material'
import axios from 'axios'

const FarmSchoolHome = () => {
  const [Category, setCategaory] = useState([])

  const breakpoints = {
    xs: 0,
    sm: 600,
    md: 900,
    lg: 1280,
    xl: 1920,
  }

  const getColumns = (width) => {
    if (width < breakpoints.sm) {
      return 1
    } else if (width < breakpoints.md) {
      return 2
    } else if (width < breakpoints.lg) {
      return 3
    } else {
      return 3
    }
  }

  const [columns, setColumns] = useState(getColumns(window.innerWidth))
  const updateDimensions = () => {
    setColumns(getColumns(window.innerWidth))
  }

  const FetchCategoryList = () => {
    //setCategaory(CategoryList)
    //console.log('is item Available : ' + userId)
    const url = 'http://localhost:4000/item/fetch-category-list'
    axios.get(url).then((response) => {
      //console.log(response)
      const result = response.data
      if (result.status === 'error') {
        alert("Couldn't fetch category list")
      }
      if (result.status === 'success') {
        //console.log(result.data)
        setCategaory(result.data)
      }
    })
  }

  useEffect(() => {
    //need to write api to fetch category list
    FetchCategoryList()
    window.addEventListener('resize', updateDimensions)
    return () => window.removeEventListener('resize', updateDimensions)
  }, [])

  return (
    <Box
      sx={{
        backgroundColor: '#f6f9fc',
      }}
    >
      <Stack direction={'row'} alignItems='center' justifyContent={'center'}>
        <img
          src='https://img.icons8.com/nolan/64/school.png'
          style={{ marginRight: 10 }}
        />
        <Typography
          variant='h3'
          color='#696463'
          sx={{
            fontFamily: 'serif',

            letterSpacing: '0.02em',
          }}
        >
          Farm School
        </Typography>
      </Stack>
      <Container
        maxWidth={false}
        sx={{
          maxWidth: '1280px',
          justifyContent: 'center',
          paddingTop: '5%',
          paddingBottom: '1%',
        }}
      >
        <ImageList variant='masonry' cols={columns} gap={42}>
          {Category.map((category) => (
            <FarmSchoolCategory category={category} />
          ))}
        </ImageList>
      </Container>
    </Box>
  )
}

export default FarmSchoolHome
