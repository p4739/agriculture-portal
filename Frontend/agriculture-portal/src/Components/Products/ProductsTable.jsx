import * as React from 'react';
import { green } from '@mui/material/colors';
import { Grid, Hidden, Box, List, LinearProgress } from '@mui/material'
import { useStyles } from '../Dashboard/Styles';
import { useEffect } from 'react';
import { useState } from 'react';
import TableData from './TableData'
import axios from 'axios';
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import styled from 'styled-components';
import Pagination from "@mui/material/Pagination";
import jwtDecode from "jwt-decode";


export default function CustomizedTables() {

  const classes = useStyles();

  let [productDetails, setProductDetails] = useState([]);

  let [userId, setUserId] = useState(null)

  const handleUndefinedUserId = () => {

    const Token = sessionStorage.getItem('token')
    const DecodedToken = jwtDecode(Token)
  
    userId = DecodedToken.data.userId
    setUserId(DecodedToken.data.userId)

    console.log(userId);

  }
  


  const fetchProducDetails = async () => {

    const url = `http://localhost:4000/product/${userId}`

    await axios.get(url).then((response) => {
      const result = response.data;

      if (result['status'] === 'success') {        
        productDetails = result['data']
        setProductDetails(result['data'])        
      }      
      else {
        toast.error(result['error'])
      }
    })


  }


  useEffect(() => {
    handleUndefinedUserId() 
     fetchProducDetails()
    }, []);

  const itemsPerPage = 4;
  const [page, setPage] = React.useState(1);

  // const [noOfPages] = React.useState(
  //   // productsCount = productDetails.length,
  //   console.log(productDetail),
  //   Math.ceil(productsCount / itemsPerPage));

  let productsCount = productDetails.length;
  // console.log(productsCount)
  const noOfPages = Math.ceil(productsCount / itemsPerPage);
  // console.log(noOfPages);


  const handleChange = (event, value) => {
    setPage(value);
  };

  const CssPagination = styled(Pagination)({

    "& .MuiPaginationItem-textPrimary.Mui-selected": {
      backgroundColor: green["800"]
    }
  });

  return (
    <div >
      {/* <Hidden smDown> */}
      <Box sx={{
        widows: "100px",
        marginTop: "25px",
        height: "100%",
        borderRadius: '10px',
        marginLeft: '-16%',
        ml: ['12%', 0, 0],

      }}>
        <Grid container spacing={2} 
        // columns={15}

          sx={(theme) => ({
            fontSize: '18px',
            fontWeight: 600,
            lineHeight: '1.5',
            lineWeight: 600,
            color: 'rgb(125, 135, 156)',
            //  margin: "5px 40px",
            whiteSpace: 'normal',
            marginLeft: '-10%',
            fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif',
            flex: "1 1 0px",
            textTransform: 'capitalize',

            [theme.breakpoints.down("sm")]: {
              marginLeft: '0%',
              marginTop: "-15%",
              // width: '140%',
            },
          })}

        >

          <Grid item xs={2}
            sx={(theme) => ({
              [theme.breakpoints.down("sm")]: {
                marginLeft: '-35%',
                marginTop: "40px",
                width: '110%',
              },
              marginLeft : '-3%'
            })}
          >
            Product#
          </Grid>
          <Grid item xs={3}
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              marginLeft: '12%',
              marginTop: "40px",
              width: '110%',
            },
            marginLeft : '3%'
          })}>
            Name
          </Grid>

          <Grid item xs={3}
          sx={(theme) => ({
            marginLeft : '1%',
            [theme.breakpoints.down("sm")]: {
              marginLeft: '11%',
              marginTop: "40px",
              width: '110%',
            },
          })}>
            Stock
          </Grid>

          <Grid
            // className={classes.tableHeader}
            sx={(theme) => ({
              marginLeft : '-4%',
              [theme.breakpoints.down("sm")]: {
                // marginLeft: '-10%',
                marginTop: "40px",
                width: '110%',
              },
              
            })}
            item xs={2}>
            Selling Price
          </Grid>

          <Hidden smDown>
            <Grid
              // className={classes.tableHeader}
              item xs={1}>
            </Grid>
          </Hidden>

        </Grid>
      </Box>
      {/* </Hidden> */}

      {

        productDetails.length === 0 ?

          <>
            <LinearProgress color="success" />
            <h4 style={{ marginLeft: '30%', marginTop: '3%', color: 'red', size: '10%' }}>No Products Added</h4>
          </>


          :

          <List dense compoent="span">
            {productDetails
              .slice((page - 1) * itemsPerPage, page * itemsPerPage)
              .map((data) => {

                return (
                  <TableData row={data} />
                );
              })}
          </List>
      }

      {
        productDetails.length === 0 ? "" :

          <Box style={{ marginTop: '30px' }}>

            <CssPagination
            sx={(theme) => ({
              marginLeft : '20%',
              [theme.breakpoints.down("sm")]: {
                marginLeft: '-10%',
               
              },
            })}
              // style={{ marginLeft: "20%" }}
              // sx={{ color: '#227b00' }}
              count={noOfPages}
              page={page}
              onChange={handleChange}
              defaultPage={1}
              color="primary"
              size="large"
              showFirstButton
              showLastButton
              classes={{ ul: classes.paginator }}
            />
          </Box>
      }

    </div>
  );
}
