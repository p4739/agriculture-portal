import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Button, ListItem, ListItemIcon } from "@mui/material"
import { green } from '@mui/material/colors';
import Inventory2Icon from '@mui/icons-material/Inventory2';
import { useStyles } from '../Dashboard/Styles';
import ProductTable from './ProductsTable'
import { Routes, Route } from "react-router-dom";
import EditProduct from '../EditProduct/EditProduct';


export default function Product() {
    console.log("in products page");

    const navigate = useNavigate();
    const classes = useStyles();
    return (
        <div>

            <ListItem
                //  className={classes.selectedComponent}
                sx={(theme) => ({
                    [theme.breakpoints.down("sm")]: {
                        fontSize: "110%",                        
                    },
                    marginLeft: '-16%',
                    marginTop : '-5%',
                    textTransform: "capitalize",
                    fontFamily: "Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif",
                    fontSize: "150%",
                    color: 'black',
                })}



            >
                <ListItemIcon
                //  className={classes.selectedComponent}
                    style={{ color: green["800"], marginTop: '-12px' }} 
                    >
                    <Inventory2Icon fontSize='large' />
                </ListItemIcon>
                <h1
                //  className={classes.selectedComponent}
                  style={{ marginLeft: '10px' }} > Products</h1>
            </ListItem>

            <ProductTable ></ProductTable>

            <Routes>
                {/* <Route path='/order-details' element={<OrderDetails />} /> */}
                <Route path='/edit-details' element={< EditProduct />} />
            </Routes>

        </div>
    )
}

