import React from 'react'
import { useStyles } from '../Dashboard/Styles'
import { Paper } from '@mui/material'
import { IconButton, Button, Grid, Hidden } from '@mui/material'

import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos'
import { useNavigate } from 'react-router-dom'
import { Divider, Box, List, ListItem, } from '@mui/material'
import Pagination from "@material-ui/lab/Pagination"
import ListItemText from "@mui/material/ListItemText"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import axios from 'axios'
import { toast } from 'react-toastify'
import { Snackbar } from '@mui/material'
import { Alert } from '@mui/material'
import ConfirmDialog from './ConfirmDialog'
import { useState } from 'react'
import { makeStyles, styled } from '@mui/styles'

export default function TableData(props) {


    const classes = useStyles()
    const navigate = useNavigate()
    const [confirmDialog, setConfirmDialog] = useState({ isOpen: false, title: '', subTitle: '' })

    const { row } = props


    const dataStyle = {
        fontSize: '18px',
        color: 'rgb(43, 52, 69)',
        fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif'
    }

    const handleClickForOrderDetails = () => {
        navigate('edit-details', { state: { id: row } })
    }
    const onDelete = () => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        console.log(row.productId)

        const url = `http://localhost:4000/product/delete-product/${row.productId}`
        axios.delete(url).then((response) => {
            const result = response.data

            if (result['status'] === 'success') {
                toast.success("Product Deleted Successfully !!")
                window.location.reload()
                // navigate('/dashboard/product-list');
            }
            else {
                toast.success("Product Deletion Failed !!")
            }
        })
    }
    // const myStyles = styled(theme => ({

    //     deleteIcon: {
    //         // backgroundColor: theme.palette.warning.main,
    //         color: theme.palette.error.dark,
    //         '& .MuiSvgIcon-root': {
    //             fontSize: '1.7rem',
    //         },
    //         marginLeft: "84%",
    //         marginTop: '-18px',
    //         [theme.breakpoints.down("sm")]: {
    //             marginLeft: "85%",
    //             '& .MuiSvgIcon-root': {
    //                 fontSize: '1.4rem',
    //             },
    //         },
    //     },

    //     editProductIcon: {
    //         // backgroundColor: theme.palette.warning.main,
    //         color: theme.palette.success.dark,
    //         '& .MuiSvgIcon-root': {
    //             fontSize: '1.7rem',
    //         },
    //         marginTop: '-17px',
    //     },


    // }))


    // const myStyle = myStyles();

    if (row.length === 0) {
        // console.log(row.length);
        return <h1> No Products Added</h1>
    } else {
        return (

            <>

                <Paper
                    // className={classes.paperMarginLeft}
                    sx={{
                        widows: "100px",
                        marginTop: "25px",
                        height: "70px",
                        borderRadius: '10px',
                        marginLeft : '-16%',
                        // ml: ['12%', 0, 0],
                        width : '100%'
                    }} >
                    <Grid container spacing={0} 
                    // columns={15}
                        sx={{
                            fontSize: '18px',
                            fontWeight: 600,
                            lineHeight: '1.5',
                            lineWeight: 600,
                            color: 'rgb(125, 135, 156)',                            
                            whiteSpace: 'normal',
                            marginLeft : '4%',
                            fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif',
                            textTransform: 'capitalize'
                        }}
                        >

                        <Grid onClick={handleClickForOrderDetails} style={dataStyle} item xs={2} 
                            sx={{marginTop : '1%'}}
                        >
                            {row.productId}
                        </Grid>

                        <Grid onClick={handleClickForOrderDetails}
                        sx={{marginTop : '1%',color: 'black'}}
                            // className={classes.ordersTableData} 
                            
                            item xs={3}>
                            {row.subName} {(row.hasVariety === 1 && row.vName != null) ? '[' + row.vName + ']' : ''}
                        </Grid>
                        <Grid onClick={handleClickForOrderDetails} style={dataStyle}
                            // className={classes.ordersTableData}
                            sx={(theme) => ({
                                [theme.breakpoints.down("sm")]: {
                                  marginLeft: '10%',
                                  width: '110%',
                                },
                                marginTop : '1%'
                              })}
                            item xs={3}>
                            {row.quantity}
                        </Grid>

                        <Grid onClick={handleClickForOrderDetails} style={dataStyle}
                            // className={classes.ordersTableData}
                            sx={(theme) => ({
                                marginLeft : '-8%',
                                [theme.breakpoints.down("sm")]: {                                  
                                  width: '110%',
                                },
                                marginTop : '1%'
                              })}
                            item xs={1}>
                            {row.hasVariety === 1 && row.vName != null ? row.vPrice : row.subPrice}
                        </Grid>


                        <Grid
                            // className={classes.ordersTableData}                           
                            item xs={1}>


                            <Grid container spacing={0}>
                                <Grid xs={2} sx={{ display: { xs: "none", sm: "contents" } }} >
                                    <IconButton
                                        // className={myStyle.editProductIcon}
                                        sx={(theme) => ({
                                            
                                            color: theme.palette.success.dark,
                                            '& .MuiSvgIcon-root': {
                                                fontSize: '1.7rem',
                                            },
                                            marginTop: '10px',
                                        })}
                                        onClick={handleClickForOrderDetails}>
                                        <EditIcon />
                                    </IconButton>
                                </Grid>
                                <Grid xs={2}
                                >
                                    <IconButton
                                        // className={myStyle.deleteIcon}
                                        sx={(theme) => ({
                                            color: theme.palette.error.dark,
                                            '& .MuiSvgIcon-root': {
                                                fontSize: '1.7rem',
                                            },
                                            marginLeft: "84%",
                                            marginTop: '10px',
                                            [theme.breakpoints.down("sm")]: {
                                                marginLeft: "85%",
                                                '& .MuiSvgIcon-root': {
                                                    fontSize: '1.4rem',
                                                },
                                            },
                                        })}
                                        onClick={() => {
                                            setConfirmDialog({
                                                isOpen: true,
                                                title: 'Are you sure to delete this Product ?',
                                                subTitle: "You can't undo this operation",
                                                onConfirm: () => { onDelete() }
                                            })
                                        }}

                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </Grid>
                            </Grid>
                        </Grid>

                    </Grid>

                    <ConfirmDialog
                        confirmDialog={confirmDialog}
                        setConfirmDialog={setConfirmDialog}
                    />
                </Paper>
            </>
        )
    }
}


