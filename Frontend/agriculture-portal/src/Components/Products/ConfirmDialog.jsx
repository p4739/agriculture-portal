import React from 'react'
import { Dialog, DialogTitle, DialogContent, DialogActions, Typography, IconButton } from '@mui/material'
import { Button } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import { makeStyles } from '@mui/styles'
import { styled } from '@mui/system'

const useStyles = styled((theme) => ({
    dialog: {
        padding: '8px',
        position: 'absolute',
        top: '8px'
    },
    dialogTitle: {
        textAlign: 'center'
    },
    dialogContent: {
        textAlign: 'center'
    },
    dialogAction: {
        justifyContent: 'center'
    },
    titleIcon: {        
        color: theme.palette.secondary.main,
        '&:hover': {
            backgroundColor: theme.palette.error.dark,
            cursor: 'default'
        },
        '& .MuiSvgIcon-root': {
            fontSize: '2rem',
        }
    }
}))

export default function ConfirmDialog(props) {



    const { confirmDialog, setConfirmDialog } = props;
    const classes = useStyles()

    return (
        <Dialog open={confirmDialog.isOpen} classes={{ paper: classes.dialog }}>
            <DialogContent 
            // className={classes.dialogContent}
            sx={(theme) => ({
                textAlign: 'center'
            })}
            >
            <DialogTitle 
            className={classes.dialogTitle}
            sx={(theme) => ({
                textAlign: 'center'
            })}
            >
                <IconButton disableRipple 
                // className={classes.titleIcon}
                sx={(theme) => ({
                    color: theme.palette.error.main,
                    '&:hover': {
                        backgroundColor: theme.palette.error.dark,
                        cursor: 'default'
                    },
                    '& .MuiSvgIcon-root': {
                        fontSize: '2rem',
                    }
                })}
                
                >
                    <HelpOutlineIcon/>
                </IconButton>
            </DialogTitle>
                <Typography variant="h6">
                    {confirmDialog.title}
                </Typography>
                <Typography variant="subtitle2">
                    {confirmDialog.subTitle}
                </Typography>
            </DialogContent>
            <DialogActions 
            // className={classes.dialogAction}
            sx={(theme) => ({
                justifyContent: 'center'
            })}
            >
                <Button           
                 sx={(theme) => ({
                    color: theme.palette.primary.main,
                    '&:hover': {                        
                        cursor: 'default'
                    },
                    '& .MuiSvgIcon-root': {
                        fontSize: '2rem',
                    }
                })}
                         
                    color="primary"
                    onClick={() => setConfirmDialog({ ...confirmDialog, isOpen: false })} >
                        No
                    </Button>
                <Button
                 sx={(theme) => ({
                    color: theme.palette.error.main,
                    '&:hover': {                        
                        cursor: 'default'
                    },
                    '& .MuiSvgIcon-root': {
                        fontSize: '2rem',
                    }
                })}
                
                    startIcon={<DeleteIcon />}                    
                    color="secondary"
                    onClick={confirmDialog.onConfirm} >
                        Yes
                    </Button>
            </DialogActions>
        </Dialog>
    )
}