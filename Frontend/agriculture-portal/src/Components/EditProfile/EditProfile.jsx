import React from 'react'
import { Button, ListItem, ListItemIcon } from "@mui/material"
import { useStyles } from '../Dashboard/Styles';
import SettingsIcon from '@mui/icons-material/Settings';
import { green } from '@mui/material/colors';
import { Paper, Grid } from '@mui/material';
import { useNavigate } from 'react-router';
import { TextField } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';
import { useEffect } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import styled from 'styled-components';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import jwtDecode from "jwt-decode";


export default function EditProfile() {

    const classes = useStyles();
    const navigate = useNavigate();


    let [userId, setUserId] = useState(null)

    const handleUndefinedUserId = () => {
  
      const Token = sessionStorage.getItem('token')
      const DecodedToken = jwtDecode(Token)
    
      userId = DecodedToken.data.userId
      setUserId(DecodedToken.data.userId)
  
      console.log(userId);
  
    }
    
  
    let [result, setResult] = useState([]);

    let [firstName, setFirstName] = useState('')
    let [lastName, setLastName] = useState('')
    let [email, setEmail] = useState('')
    let [password, setPassword] = useState('')
    let [contactNo, setContactNo] = useState('')
    let [addressLine1, setAddressLine1] = useState('')
    let [addressLine2, setAddressLine2] = useState('')
    let [pincode, setPincode] = useState('')

    const fethInitialData = async () => {

        const url = `http://localhost:4000/user/get-details/${userId}`

        await axios.get(url).then((response) => {


            if (response.data['status'] === 'success') {
                setResult(response.data.data[0]);
                result = response.data.data[0];
                console.log(result);

                setFirstName(result.firstName);
                firstName = result.firstName;
                // console.log(result.firstName);

                setLastName(result.lasttName);
                lastName = result.lastName;
                // console.log(lastName);


                setContactNo(result.contactNo)
                contactNo = result.contactNo

                setEmail(result.email)
                email = result.email

                setPassword(result.password)
                password = result.password

                setPincode(result.pincodeId)
                pincode = result.pincodeId

                setAddressLine1(result.addressLine1)
                addressLine1 = result.addressLine1

                setAddressLine2(result.addressLine2)
                addressLine2 = result.addressLine2

            }
            else {
                toast.error(result['error'])
            }
        });
    }

    useEffect(() => { 
        handleUndefinedUserId()
        fethInitialData() }, []);



    const updateUserDetails = () => {
        console.log(lastName);
        if (lastName == undefined) {
            setLastName(result.lasttName);
            lastName = result.lastName;
        }
        console.log(lastName);

        const body = { userId, firstName, lastName, email, password, contactNo, pincode, addressLine1, addressLine2 };
        console.log(body);
        const url = `http://localhost:4000/user/edit-profile`

        axios.put(url, body).then((response) => {
            const result = response.data;

            if (result['status'] === 'success') {
                toast.success('Information updated')
                // navigate to the signin page
                navigate('/')
            } else {
                toast.error(result['error'])
            }
        });
    }

    const CssTextField = styled(TextField)({
        "& label.Mui-focused": {
            color: "green"
        },
        // "& .MuiInput-underline:after": {
        //     borderBottomColor: "green"
        // },
        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "black",
            },
            "&:hover fieldset": {
                borderColor: "orange"
            },
            "&.Mui-focused fieldset": {
                borderColor: "green"
            }
        }
    });



    let [firstNameError, setFirstNameError] = useState(false)
    let [lastNameError, setLastNameError] = useState(false)
    let [emailError, setEmailError] = useState(false)
    let [passwordError, setPasswordError] = useState(false)
    let [contactNoError, setContactNoError] = useState(false)
    let [pincodeError, setPincodeError] = useState(false)
    let [addressLine1Error, setAddressLine1Error] = useState(false)
    let [addressLine2Error, setAddressLine2Error] = useState(false)


    const handleBlankSubmmit = (e) => {
        e.preventDefault()

        setFirstNameError(false)
        firstNameError = false;

        setLastNameError(false)
        lastNameError = false;

        setEmailError(false)
        emailError = false;

        setPasswordError(false)
        passwordError = false;

        setContactNoError(false)
        contactNoError = false

        setAddressLine1Error(false)
        addressLine1Error = false

        setAddressLine2Error(false)
        addressLine2Error = false

        setPincodeError(false)
        pincodeError = false

        console.log(firstName);
        console.log(lastName);
        console.log(email);
        console.log(password);
        console.log(contactNo);
        console.log(pincode);
        console.log(addressLine1);
        console.log(addressLine2);


        if (firstName === '') {
            setFirstNameError(true)
            firstNameError = true;
        }
        if (lastName === '') {
            setLastNameError(true)
            lastNameError = true
        }
        if (email === '') {
            setEmailError(true)
            emailError = true
        }
        if (password === '') {
            setPasswordError(true)
            passwordError = true
        }
        if (contactNo === '') {
            setContactNoError(true)
            contactNoError = true
        }
        if (addressLine1 === '') {
            setAddressLine1Error(true)
            addressLine1Error = true
        }
        if (addressLine2 === '') {
            setAddressLine2Error(true)
            addressLine2Error = true
        }
        if (pincode === '') {
            setPincodeError(true)
            pincodeError = true
        }

        console.log(firstNameError);
        console.log(lastNameError);
        console.log(emailError);
        console.log(passwordError);
        console.log(contactNoError);
        console.log(addressLine1Error);
        console.log(addressLine2Error);



        if (firstNameError === false && lastNameError === false && emailError === false && passwordError === false
            && contactNoError === false && addressLine1Error === false && addressLine2Error === false) {

            updateUserDetails();
        }

    }



    return (
        <div>

            <ListItem
                // className={classes.EditProfileComponent}
                sx={(theme) => ({
                    [theme.breakpoints.down("sm")]: {
                        fontSize: "110%",
                    },
                    marginLeft: '-16%',
                    marginTop : '-5%',
                    textTransform: "capitalize",
                    fontFamily: "Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif",
                    fontSize: "150%",
                    color: 'black',
                })}
            >
                <ListItemIcon style={{ color: green["800"], marginTop: '-12px' }} >
                    <SettingsIcon fontSize='large' />
                </ListItemIcon>
                <h1
                    //  className={classes.EditProfileComponent}
                    style={{ marginLeft: '2px' }} > Edit Profile </h1>
            </ListItem>


            <Grid>

                {result.lastName !== undefined ?

                    <Paper
                        // className={classes.innerPaperMarginLeftEditProfile}
                        sx={(theme) => ({
                            borderRadius: '15px', width: '80%', marginTop: '20px',
                            marginLeft: '-14%',
                            [theme.breakpoints.down("sm")]: {
                                marginLeft: '-8%',
                                width: '93%'
                            },
                        })}
                    >
                        {/* image 860X170  */}

                        <Grid style={{ marginRight: '25px' }} >
                            {/* <form noValidate autoComplete='off'> */}

                            <Grid container spacing={2} columns={12} style={{ marginLeft: "10%" }}>
                                <Grid item xs={6} style={{ marginTop: '20px' }}>
                                    {/* <CssTextField  variant='outlined' label="First Name" value={firstName} style={{ width: '70%' }}
                                    onChange={(e) => { setFirstName(e.target.value) }} />  */}
                                    <TextField variant='outlined' label="First Name" defaultValue={result.firstName}
                                        required error={firstNameError} style={{ width: '70%' }}
                                        onChange={(e) => { setFirstName(e.target.value) }} />
                                </Grid>


                                <Grid item xs={6} style={{ marginTop: '20px' }}>
                                    {/* <CssTextField color='secondary' style={{ width: '70%' }} label="Last Name" value={lastName} variant='outlined'
                                    onChange={(e) => { setLastName(e.target.value) }} /> */}
                                    <TextField color='primary' style={{ width: '70%' }} label="Last Name" defaultValue={result.lastName} variant='outlined'
                                        error={lastNameError} onChange={(e) => { setLastName(e.target.value) }} />

                                </Grid>

                                <Grid item xs={6} >
                                    {/* <CssTextField color='secondary' style={{ width: '70%' }} label="email" variant='outlined'
                                    onChange={(e) => { setEmail(e.target.value) }} /> */}
                                    <TextField color='primary' style={{ width: '70%' }} label="email" variant='outlined'
                                        error={emailError} defaultValue={result.email}
                                        onChange={(e) => { setEmail(e.target.value) }} />
                                </Grid>
                                <Grid item xs={6}>
                                    {/* <CssTextField color='secondary' type='password' style={{ width: '70%' }} id="outlined-required" label="Password" variant='outlined'
                                    onChange={(e) => { setPassword(e.target.value) }} /> */}
                                    <TextField color='primary' type='password' style={{ width: '70%' }} id="outlined-required" label="Password" variant='outlined'
                                        error={passwordError} defaultValue={result.password}
                                        onChange={(e) => { setPassword(e.target.value) }} />
                                </Grid>


                                <Grid item xs={6}>
                                    {/* <CssTextField label="Contact No" variant='outlined' color='secondary' style={{ width: '70%' }}
                                    onChange={(e) => { setContact(e.target.value) }} /> */}
                                    <TextField label="Contact No" variant='outlined' color='primary' style={{ width: '70%' }}
                                        error={contactNoError} defaultValue={result.contactNo}
                                        onChange={(e) => { setContactNo(e.target.value) }} />
                                </Grid>
                                <Grid item xs={6}>
                                    {/* <CssTextField color='secondary' type='Number' style={{ width: '70%' }} label="Pincode" variant='outlined'
                                    onChange={(e) => { setPincode(e.target.value) }} /> */}
                                    <TextField color='primary' type='Number' style={{ width: '70%' }} label="Pincode" variant='outlined'
                                        defaultValue={result.pincodeId} error={pincodeError}
                                        onChange={(e) => { setPincode(e.target.value) }} ></TextField>
                                </Grid>

                                <Grid item xs={6}>
                                    {/* <CssTextField color='secondary' style={{ width: '70%' }} label="Address Line 1" variant='outlined'
                                    onChange={(e) => { setAddressLine1(e.target.value) }} /> */}
                                    <TextField color='primary' style={{ width: '70%' }} label="Address Line 1" variant='outlined'
                                        error={addressLine1Error} defaultValue={result.addressLine1}
                                        onChange={(e) => { setAddressLine1(e.target.value) }} />
                                </Grid>
                                <Grid item xs={6}>
                                    {/* <CssTextField color='secondary' style={{ width: '70%' }} label="Address Line 2" variant='outlined'
                                    onChange={(e) => { setAddressLine2(e.target.value) }} /> */}
                                    <TextField color='primary' style={{ width: '70%' }} label="Address Line 2" variant='outlined'
                                        defaultValue={result.addressLine2} error={addressLine2Error}
                                        onChange={(e) => { setAddressLine2(e.target.value) }} />
                                </Grid>

                            </Grid>

                            <Grid
                                //  className={classes.saveChanges} 
                                sx={(theme) => ({
                                    marginTop: '8%', marginBottom: '6%', marginLeft: '25%',
                                    [theme.breakpoints.down("sm")]: {
                                        marginTop: '15%',
                                        marginLeft: '-10%',
                                        marginBottom: '8%'
                                    },
                                })}
                            >
                                {/* <button onClick={updateUserDetails}> Save changes  </button> */}
                                <Button type="submit" style={{
                                    width: '220px', backgroundColor: 'rgb(50, 150, 70)', color: 'rgb(255, 255, 255)'
                                    , marginLeft: '25%'
                                }}
                                    onClick={handleBlankSubmmit} > Save Changes </Button>
                            </Grid>
                            <br></br>


                            {/* </form> */}


                        </Grid>

                    </Paper>
                    :
                    <LinearProgress color="success" />
                }
            </Grid>
        </div >
    )
}
