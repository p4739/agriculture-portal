import React from "react";
import { useEffect } from "react";
import { Box } from "@mui/material";
import CartListItem from "../CartListItem";

const CartList = (props) => {
  const { cartUpdater, setCartUpdater, cartDetails, userId } = props;

  return (
    <Box sx={{ marginTop: "3%", marginLeft: "10%" }}>
      {cartDetails.map((item) => (
        <CartListItem
          key={item.cartId}
          item={item}
          cartUpdater={cartUpdater}
          setCartUpdater={setCartUpdater}
          userId={userId}
        />
      ))}
    </Box>
  );
};

export default CartList;
