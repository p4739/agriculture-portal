import { Box, Button, Card, CardMedia, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";
import Paper from "@mui/material/Paper";
import React, { useEffect } from "react";
import { styled } from "@mui/system";
import axios from "axios";
import { URL } from "../../config";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
//const CartPaper = styled(Paper)({});

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const CartListBox = styled(Box)(({ theme }) => ({
  width: "85%",
  marginTop: "1%",
  marginBottom: "1%",
  display: "flex",
  padding: "4px",
  [theme.breakpoints.down("md")]: {
    width: "100%",
    border: "none",
  },
}));

const MyTypography = styled(Typography)({
  variant: "body1",
  fontSize: "16px",
  fontWeight: "500",
  color: "#085E7D",
});

function CartListItem(props) {
  const { item, cartUpdater, setCartUpdater, userId } = props;
  const navigate = useNavigate();
  var value = item.price;
  const [quantity, setQuantity] = React.useState(item.quantity);
  const [itemTotal, setItemTotal] = React.useState(value * item.quantity);
  const [SnackBar, setSnackbar] = React.useState(false);
  const [msg, setmsg] = React.useState("");
  const [severity, setSeverity] = React.useState("");

  const OpenSnackBar = () => {
    setSnackbar(true);
  };
  const CloseSnackbar = () => {
    setSnackbar(false);
  };
  function addItem() {
    let subCategoryId = item.subCategoryId;
    let varietyId = item.varietyId;
    const body = { subCategoryId, varietyId, userId };
    //console.log('Available : ' + userId)
    const url = `${URL}/item/add-item-to-cart`;
    axios.post(url, body).then((response) => {
      //console.log(response)
      const result = response.data;
      if (result.status === "error") {
        setmsg("Max quantity reached for the item");
        setSeverity("error");
        OpenSnackBar();
      }
      if (result.status === "success") {
        setQuantity(quantity + 1);
        setItemTotal(value * (quantity + 1));
      }
    });
  }
  function delItem() {
    let subCategoryId = item.subCategoryId;
    let varietyId = item.varietyId;
    const body = { subCategoryId, varietyId, userId };
    //console.log('Available : ' + userId)
    const url = `${URL}/item//del-item-from-cart`;
    axios.delete(url, body).then((response) => {
      //console.log(response)
      const result = response.data;
      if (result.status === "error") {
        setmsg("Min quantity has been reached");
        setSeverity("error");
        OpenSnackBar();
      }
      if (result.status === "success") {
        setQuantity(quantity - 1);
        setItemTotal(value * (quantity - 1));
      }
    });
  }
  function removeItem() {
    let cartId = item.cartId;
    const body = { cartId };
    const url = `${URL}/cart/remove-item`;
    axios.delete(url, body).then((response) => {
      const result = response.data;
      if (result.status === "error") {
        setmsg("Something went Wrong");
        setSeverity("error");
        OpenSnackBar();
      }
      if (result.status === "success") {
        setmsg("Item removed Successfully");
        setSeverity("success");
        OpenSnackBar();
      }
    });
  }

  const incQuantity = () => {
    addItem();
    //SubcategoryId, VarietyId and UserId  to “item/AddItemToCart” api
    setCartUpdater(!cartUpdater);
  };
  const decQuantity = () => {
    delItem();
    //SubcategoryId, VarietyId and UserId to “item/DelItemFromCart” api as request body.
    setCartUpdater(!cartUpdater);
  };
  const deleteItem = () => {
    console.log("Deleted this item " + item.name);
    removeItem();
    setCartUpdater(!cartUpdater);
  };

  return (
    <CartListBox>
      <Paper elevation={3} sx={{ width: "100%" }}>
        <Box sx={{ display: "flex" }}>
          <Box>
            <Card
              variant="outlined"
              sx={(theme) => ({
                [theme.breakpoints.down("sm")]: {
                  width: 100,
                  border: "none",
                },
                [theme.breakpoints.up("sm")]: {
                  width: 150,
                  border: "none",
                },
              })}
            >
              <CardMedia
                component="img"
                height="120"
                width="80"
                image={`${item.imageId}`}
                alt={item.name}
                loading="lazy"
              />
            </Card>
          </Box>
          <Box sx={{ display: "flex", flexGrow: 1, pr: 1, py: 1 }}>
            <Box
              sx={{
                display: "block",
                width: "85%",
                marginLeft: "3%",
                textAlign: "start",
              }}
            >
              <Box sx={{ height: "75%" }}>
                <Typography
                  variant="h6"
                  color="initial"
                  sx={{ fontWeight: "500", cursor: "pointer" }}
                  onClick={() => {
                    console.log("navigate");
                    /*  navigate("/item/"`${itemId}`, {
                      state: {
                         send Id 
                      },
                    }); */
                  }}
                >
                  {item.name}
                </Typography>
              </Box>
              <Box sx={{ alignSelf: "revert", display: "flex" }}>
                <MyTypography
                  sx={{ fontWeight: "400", mr: "1%", color: "black" }}
                >
                  &#8377; {value} &#215; {quantity} =
                </MyTypography>
                <MyTypography>&#8377; {itemTotal}</MyTypography>
              </Box>
            </Box>
            <Box
              sx={{
                display: "blocks",
                height: "100%",
                marginRight: "1%",
              }}
            >
              <Box
                sx={{
                  height: "75%",
                }}
              >
                <IconButton
                  aria-label="delete"
                  size="small"
                  onClick={deleteItem}
                >
                  <DeleteIcon fontSize="small" />
                </IconButton>
              </Box>
              <Box sx={{ display: "flex" }}>
                <Button
                  id="decbtn"
                  disabled={quantity < 2}
                  variant="outlined"
                  color="secondary"
                  size="small"
                  onClick={decQuantity}
                  sx={{ p: 0, mr: 1.2, minWidth: "32px" }}
                >
                  <RemoveIcon fontSize="small" />
                </Button>
                <Box id="qauntity">{quantity}</Box>
                <Button
                  id="incbtn"
                  variant="outlined"
                  color="secondary"
                  size="small"
                  sx={{ p: 0, ml: 1.2, minWidth: "32px" }}
                  onClick={incQuantity}
                >
                  <AddIcon fontSize="small" />
                </Button>
              </Box>
            </Box>
          </Box>
        </Box>
        <Snackbar
          open={SnackBar}
          autoHideDuration={6000}
          onClose={CloseSnackbar}
        >
          <Alert
            onClose={CloseSnackbar}
            severity={severity}
            sx={{ width: "100%" }}
          >
            {msg}
          </Alert>
        </Snackbar>
      </Paper>
    </CartListBox>
  );
}
export default CartListItem;
