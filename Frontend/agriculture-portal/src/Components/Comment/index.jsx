import { Avatar, Grid, IconButton, Paper } from '@mui/material'
import axios from 'axios'
import moment from 'moment'

export default function Comment(props) {
  const { comment, userId, CommentUpdater, setCommentUpdater } = props
  //console.log(comment, userId)

  function deleteComment() {
    let commentId = comment.commentId
    const body = { commentId }
    console.log('delete comment ran')
    const url = 'http://localhost:4000/expertsopinion/delete-comment'

    axios.post(url, body).then((response) => {
      const result = response.data
      if (result.status === 'error') {
        alert(result.error)
      }
      if (result.status === 'success') {
        setCommentUpdater(!CommentUpdater)
      }
    })
  }
  return (
    <Paper style={{ padding: '40px 20px', marginTop: 10 }}>
      <Grid container wrap='nowrap' spacing={2}>
        <Grid item>
          <Avatar>
            {comment.userId.firstName.slice(0, 1) +
              '' +
              comment.userId.lastName.slice(0, 1)}
          </Avatar>
        </Grid>
        <Grid justifyContent='left' item xs zeroMinWidth>
          <h4 style={{ margin: 0, textAlign: 'left' }}>
            {comment.userId.firstName + ' ' + comment.userId.lastName}
          </h4>
          <p style={{ textAlign: 'left' }}>{comment.comment}</p>
          <p style={{ textAlign: 'left', color: 'gray' }}>
            {moment(comment.createdTimestamp).fromNow()}
          </p>
        </Grid>
        <Grid item justifyContent={'right'}>
          {userId == comment.userId.userId && (
            <IconButton
              onClick={() => {
                deleteComment()
              }}
            >
              <img
                src='https://img.icons8.com/flat-round/64/000000/delete-sign.png'
                height={20}
              />
            </IconButton>
          )}
        </Grid>
      </Grid>
    </Paper>
  )
}
