import { useNavigate } from 'react-router-dom'
import ImageListItem from '@mui/material/ImageListItem'
import ImageListItemBar from '@mui/material/ImageListItemBar'
import { styled } from '@mui/material/styles'
import { IconButton } from '@mui/material'

const MyImageListItem = styled(ImageListItem)`
  ${({ theme }) => `
  cursor: pointer;
  background-color: ${theme.palette.primary.main};
  transition: ${theme.transitions.create(['background-color', 'transform'], {
    duration: theme.transitions.duration.complex,
    easing: theme.transitions.easing.sharp,
  })};
  &:hover {
    background-color: ${theme.palette.secondary.main};
    transform: scale(1.05);
    minwidth: "110%";
  }
  `}
`

const FarmSchoolSubCategory = (props) => {
  const { subCategory } = props;
  const {isMachinery} = props;
  const navigate = useNavigate();
//console.log(subCategory)

  return (
    <div>
      {subCategory==null? <></>:
      <MyImageListItem
        key={subCategory.subCategoryId}
        onClick={() => {
         // console.log(isMachinery);
          
            if(!isMachinery){
          navigate("/utilpage/FarmSchoolHome/FarmSchoolSubType/FarmSchoolAgricultureSubTopic", {
            state: { id: subCategory.subCategoryId },
          });}else{
            navigate("/utilpage/FarmSchoolHome/FarmSchoolSubType/FarmSchoolMachinerySubVariety", {
                state: { id: subCategory.subCategoryId }
          });
        }}}
      >
        <img
          src={`${subCategory.imageId[0].link}?w=350?&fit=crop&auto=format`}
          srcSet={`${subCategory.imageId[0].link}`}
          alt={subCategory.name}
          loading="lazy"
        />
        <ImageListItemBar
          sx={{textTransform:'capitalize'}}
          title={subCategory.name}
          actionIcon={
            <IconButton
              sx={{
                color: "rgba(255, 255, 255, 0.54)",
              }}
            />
          }
        />
      </MyImageListItem>}
    </div>
  )
}

export default FarmSchoolSubCategory
