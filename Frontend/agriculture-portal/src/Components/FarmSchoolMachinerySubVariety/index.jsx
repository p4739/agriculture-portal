import { useEffect, useState } from 'react'
import FarmSchoolVariety from '../FarmSchoolVariety'
import Container from '@mui/material/Container'
import { Box } from '@mui/system'
import ImageList from '@mui/material/ImageList'
import axios from 'axios'
import { useLocation } from 'react-router-dom'
import { Stack, Typography } from '@mui/material'

export default function FarmSchoolMachinerySubVariety(props) {
  const { state } = useLocation()
  console.log(state.id)
  const SubCategoryId = state.id //will later get it from props const { SubCategoryId } = props
  const [machineryVariety, setmachineryVariety] = useState([])

  function fetchMachineryVariety() {
    console.log(SubCategoryId)
    const body = { SubCategoryId }
    const url = 'http://localhost:4000/farmschool/fetch-machinery-variety'
    axios.post(url, body).then((response) => {
      const result = response.data
      if (result.status === 'error') {
        alert("data couldn't be fetched")
      }
      if (result.status === 'success') {
        console.log(result.variety)
        setmachineryVariety(result.variety)
      }
    })
  }

  const breakpoints = {
    xs: 0,
    sm: 600,
    md: 900,
    lg: 1280,
    xl: 1920,
  }

  const getColumns = (width) => {
    if (width < breakpoints.sm) {
      return 1
    } else if (width < breakpoints.md) {
      return 2
    } else if (width < breakpoints.lg) {
      return 3
    } else {
      return 3
    }
  }

  const [columns, setColumns] = useState(getColumns(window.innerWidth))
  const updateDimensions = () => {
    setColumns(getColumns(window.innerWidth))
  }

  useEffect(() => {
    fetchMachineryVariety()
    window.addEventListener('resize', updateDimensions)
    return () => window.removeEventListener('resize', updateDimensions)
  }, [])

  return (
    <Box
      sx={{
        backgroundColor: '#F6F9FC',
      }}
    >
      <Stack direction={'row'} alignItems='center' justifyContent={'center'}>
        <img
          src='https://img.icons8.com/nolan/64/school.png'
          style={{ marginRight: 10 }}
        />
        <Typography
          variant='h3'
          color='#696463'
          sx={{
            fontFamily: 'serif',

            letterSpacing: '0.02em',
          }}
        >
          Farm School
        </Typography>
      </Stack>
      <Container
        maxWidth={false}
        sx={{
          maxWidth: '1280px',
          justifyContent: 'center',
          paddingTop: '5%',
          paddingBottom: '1%',
        }}
      >
        <ImageList variant='masonry' cols={columns} gap={42}>
          {machineryVariety.map((variety) => (
            <FarmSchoolVariety variety={variety} key={variety.varietyId} />
          ))}
        </ImageList>
      </Container>
    </Box>
  )
}
