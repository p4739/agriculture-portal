import { styled } from '@mui/material' 
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import MenuItem from '@mui/material/MenuItem'
import Menu from '@mui/material/Menu'
import useScrollTrigger from '@mui/material/useScrollTrigger'
import { useState, useEffect, cloneElement } from 'react'
import jwt_decode from 'jwt-decode'
import Avatar from '@mui/material/Avatar'
import Dialog from '@mui/material/Dialog'
import { ButtonBase } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import Login from '../../Pages/Signin'

//Logo which is imported
import logo from '../../Assets/HeaderLogo.png'

//Testing purpose Login page imported
//import Login from './Login'

//Icons imported for MUI
import SchoolIcon from '@mui/icons-material/School'
import QuestionAnswerOutlinedIcon from '@mui/icons-material/QuestionAnswerOutlined'
import PermIdentityOutlinedIcon from '@mui/icons-material/PermIdentityOutlined'
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined'

//Used for scroll elevation app bar (App bar functionality)
function ElevationScroll(props) {
  const { children } = props
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  })

  return cloneElement(children, {
    elevation: trigger ? 4 : 1,
  })
}

//Used to style box used for dividing mobile options evenly
const StyledMobileAppBarBox = styled(Box)(() => ({
  flexGrow: 1,
  justifyContent: 'space-evenly',
  display: 'grid',
  alignItems: 'center',
}))

//Used to style icon button which are displayed in the mobile app bar
const StyledIconButton = styled(IconButton)(() => ({
  '&:hover': {
    color: 'green',
  },
}))

//Used to style menu item
const StyledMenuItem = styled(MenuItem)(() => ({
  justifyContent: 'center',
  height: '50px',
}))

export default function UtilHeader() {
  const [UserId, setUserId] = useState(null) //Set User Id
  const [UserNameInitials, setUserNameInitials] = useState(null) //Set User name initials
  //const [UserRole, setUserRole] = useState(null) //Set User role id
  const [anchorEl, setAnchorEl] = useState(null) //Used to set the position of menu

  //used for dialog opening and closing
  const [open, setOpen] = useState(false)
  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = (value) => {
    setOpen(false)
  }

  const navigate = useNavigate()

  //will be used to check if there is any value in anchorE1, if present--> set it to true, else false
  const isMenuOpen = Boolean(anchorEl)

  //This hook will run everytime there is a change is UserId and will call FetchUserDetails function
  useEffect(() => {
    FetchUserDetails()
  }, [UserId])

  //Will fetch the user details based on token in session storage. Also will fetch the cart details if userId exist
  function FetchUserDetails() {
    //will need to later fetch it from session storage
    // window.sessionStorage.setItem(
    //   'token',
    //   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyIiwiRk4iOiJKb2huIiwiTE4iOiJQb2ludGluZyIsIlN0YXRlIjoiR29hIiwiUm9sZSI6MX0.9QH5Be4LN0fJEtWd-d923CBIf7o7DFFjyznZuC6EkLw'
    // )

    const Token = window.sessionStorage.getItem('token')
    if (Token == null) {
      setUserId(null)
    } else {
      const DecodedToken = jwt_decode(Token)
      //console.log(DecodedToken)
      const Name =
        DecodedToken.data.firstName.charAt(0) +
        ' ' +
        DecodedToken.data.lastName.charAt(0)
      setUserId(DecodedToken.data.userId)
      
      setUserNameInitials(Name)
      console.log(UserNameInitials)

      //Call User/Cart api,
      //compute the total rows fetched
      
    }
    console.log(UserId)
  }

  //Used to show the menu when user clicks on Avatar (Set anchorEl variable)
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget)
  }

  //Used to close the menu (Set anchorEl variable to null)
  const handleMenuClose = () => {
    setAnchorEl(null)
  }

  //Used to navigate to Logout page
  const handleLogoutOption = () => {
    navigate('/logout')
    setAnchorEl(null)
  }

  //will render the menu based on anchorE1 variable
  const renderMenu = UserId && (
    <Menu
      anchorEl={anchorEl}
      MenuListProps={{
        disablePadding: true,
      }}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 20,
      }}
      PaperProps={{
        style: {
          width: '175px',
          borderRadius: 0,
        },
      }}
      keepMounted
      open={isMenuOpen} //will be used to open/close based on anchorE1 value
      onClose={handleMenuClose}
    >
      <StyledMenuItem onClick={handleLogoutOption} divider autoFocus>
        Logout
      </StyledMenuItem>
    </Menu>
  )

  //Main rendering of header component
  return (
    <Box sx={{ flexGrow: 1 }}>
      <ElevationScroll>
        <AppBar
          position='fixed'
          style={{
            backgroundColor: 'white',
            paddingTop: '10px',
            paddingBottom: '10px',
          }}
        >
          {/* Used to display logo, profileicon, avatar ...*/}
          <Toolbar>
            <Box sx={{ flexGrow: 1 }} />
            {/* Created a simple logo button*/}
            <ButtonBase
              sx={{
                '&:hover': { backgroundColor: 'inherit' },
                height: '0px',
                width: '0px',
                marginTop: '15px',
              }}
              color='#fff'
            >
              <img
                component='img'
                src={`${logo}`} //Added imported logo here
                height={125}
                width={150}
                alt=''
                onClick={() => {
                  navigate('/')
                }}
              />
            </ButtonBase>
            <Box sx={{ flexGrow: 4 }} />
            {/*This part will be show if the width is greater than 900px*/}
            <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
              {UserId == null && (
                <IconButton //Will display the profile icon button
                  size='small'
                  aria-haspopup='true'
                  onClick={handleClickOpen}
                  sx={{
                    padding: '11px',
                    backgroundColor: '#F3F5F9',
                    '&:hover': { backgroundColor: '#F3F5F9' },
                  }}
                >
                  <PermIdentityOutlinedIcon color='black' fontSize='small' />
                </IconButton>
              )}

              {UserId && (
                <Avatar //Will display the avatar when user has logged in
                  aria-haspopup='true'
                  onClick={handleProfileMenuOpen}
                  sx={{
                    color: 'rgba(0, 0, 0, 0.54)',
                    padding: '2px',
                    fontSize: 'medium',
                    fontWeight: 'bold',
                    backgroundColor: '#F3F5F9',
                    '&:hover': { backgroundColor: '#F3F5F9' },
                  }}
                >
                  {UserNameInitials}
                  {/*Will show the initials of logged in user*/}
                </Avatar>
              )}
            </Box>

            {/* responsive app drawer, used for mobile devices and will be shown if width is less than 900px */}
            <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
              <AppBar
                position='fixed'
                width='100%'
                style={{
                  backgroundColor: 'white',
                }}
                sx={{ top: 'auto', bottom: 0 }} //Will fix the app bar at the bottom of the page
              >
                <Toolbar>
                  <StyledMobileAppBarBox>
                    <StyledIconButton //Home icon button
                      onClick={() => {
                        navigate('/')
                      }}
                    >
                      <HomeOutlinedIcon color='black' />
                    </StyledIconButton>
                    <Typography variant='div' color='#757575'>
                      Home
                    </Typography>
                  </StyledMobileAppBarBox>

                  <StyledMobileAppBarBox>
                    <StyledIconButton //farmschool icon button
                      onClick={() => {
                        //navigate to /FarmSchoolHome page
                        navigate('/utilpage/FarmSchoolHome')
                      }}
                    >
                      <SchoolIcon color='black' />
                    </StyledIconButton>

                    <Typography variant='div' color='#757575'>
                      Farm School
                    </Typography>
                  </StyledMobileAppBarBox>

                  <StyledMobileAppBarBox>
                    <StyledIconButton //expertsopinion icon button
                      onClick={() => {
                        //navigate to /ExpertsOpinionHome page
                        navigate('/utilpage/ExpertsOpinionHome')
                      }}
                    >
                      <QuestionAnswerOutlinedIcon color='black' />
                    </StyledIconButton>

                    <Typography variant='div' color='#757575'>
                      Experts Opinion
                    </Typography>
                  </StyledMobileAppBarBox>

                  <StyledMobileAppBarBox>
                    <StyledIconButton //profile icon button 
                      onClick={() => {
                        if (UserId != null) {
                          //navigate to /ProfileInfo page
                          navigate('/logout')
                        } else {
                          alert('please login')
                        }
                      }}
                    >
                      <PermIdentityOutlinedIcon color='black' />
                    </StyledIconButton>

                    <Typography variant='div' color='#757575'>
                      logout
                    </Typography>
                  </StyledMobileAppBarBox>
                </Toolbar>
              </AppBar>
            </Box>

            <Box sx={{ flexGrow: 1 }} />
          </Toolbar>
        </AppBar>
      </ElevationScroll>

      {/*Used for rendering menu options*/}
      {renderMenu}

      {/*Used to display the dialog box that shows login component*/}
      <Dialog open={open} onClose={handleClose} fullWidth>
        <Login />
      </Dialog>
    </Box>
  )
}
