import * as React from 'react'
import Typography from '@mui/material/Typography'
import { useEffect, useState } from 'react'
import { styled, alpha } from '@mui/material/styles'
import InputBase from '@mui/material/InputBase'
import Autocomplete from '@mui/material/Autocomplete'
import Paper from '@mui/material/Paper'
import { Box, createFilterOptions, Stack } from '@mui/material'
import { Snackbar } from '@mui/material'
import MuiAlert from '@mui/material/Alert'
import jwt_decode from 'jwt-decode'
import { useNavigate } from 'react-router-dom'
//icons imported
import QuestionAnswerTwoToneIcon from '@mui/icons-material/QuestionAnswerTwoTone'
import SearchIcon from '@mui/icons-material/Search'
import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined'
import SaveAltOutlinedIcon from '@mui/icons-material/SaveAltOutlined'
// for accordion
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import Button from '@mui/material/Button'
//for dialog box
import TextField from '@mui/material/TextField'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import Container from '@mui/material/Container'

//for connecting backend
import { URL } from '../../config'
import axios from 'axios'

const MyAccordion = styled(Accordion)(({ theme }) => ({
  backgroundColor: '#fbfcfd',
  maxWidth: '1280px',
  marginBottom: 10,
}))

const MyButton = styled(Button)(({ theme }) => ({
  marginLeft: '4%',
  fontSize: '14px',
  [theme.breakpoints.down('md')]: { fontSize: '11px' },
  [theme.breakpoints.down('sm')]: { fontSize: '8px' },
}))

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderTopLeftRadius: 20,
  borderBottomLeftRadius: 20,
  borderTopRightRadius: 0,
  borderBottomRightRadius: 0,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
    borderColor: '#2e7d32',
  },
  borderColor: 'grey',
  borderStyle: 'solid',
  borderWidth: 'thin',
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('xs')]: {
    width: '66ch',
  },
}))

//Used to design search icon (Searh bar functionality)
const SearchIconWrapper = styled('div')(({ theme }) => ({
  paddingLeft: '10px',
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: 'grey',
}))

//Used to deisgn the input base that takes input from user in searh bar (Searh bar functionality)
const StyledInputBase = styled(InputBase)(({ theme }) => ({
  //padding: theme.spacing(1, 1, 1, 0),

  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(2)})`,
    //border: '10px',
    transition: theme.transitions.create('width'),
    fontSize: 'medium',
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '60ch',
    },
  },
}))

//Used to design the drop down shown when user searches for an item (Searh bar functionality)
const CustomPaper = (props) => {
  return (
    <Paper
      elevation={3}
      {...props}
      sx={[
        {
          width: 'auto',
          marginTop: 0.2,
          borderRadius: 3,
          paddingLeft: '17px',
          paddingRight: '20px',
        },

        (theme) => ({
          [theme.breakpoints.up('md')]: {
            width: '72.75ch',
          },
        }),
      ]}
    />
  )
}

const SearchButton = styled(Button)(({ theme }) => ({
  height: '41px',
  borderTopLeftRadius: 0,
  borderBottomLeftRadius: 0,
  borderTopRightRadius: 18,
  borderBottomRightRadius: 18,
  textAlign: 'right',
  width: '120px',
  display: 'inline-flex',
  fontWeight: 400,
  textTransform: 'capitalize',
  boxShadow: 'none',
  '&:hover': { boxShadow: 'none' },
}))

//Used to limit search elements to 4 such that no scoll bar is shown (Searh bar functionality)
const OPTIONS_LIMIT = 4
const defaultFilterOptions = createFilterOptions()

const filterOptions = (options, state) => {
  return defaultFilterOptions(options, state).slice(0, OPTIONS_LIMIT)
}

function ExpertsOpinionHome() {
  //Used to set searched text
  const [searchText, setSearchText] = useState('') //for search text input inside searchbar
  const [SearchSnackbar, setSearchSnackbar] = useState(false) //to open/close SearchSnackbar
  const [QuestionSnackbar, setQuestionSnackbar] = useState(false) //to open/close QuestionSnackbar
  let msg1 = 'Question Cannot be empty'
  const [severity, setSeverity] = useState('error')
  const [questionMsg, setQuestionMsg] = useState(msg1)
  const [DialogBox, setDialogBox] = useState(false) // to oen/close DialogBox
  const [Question, setQuestion] = useState('') ////for Question text input inside DialogBox
  const [UserId, setUserId] = useState() //for setting the user ID
  const [UserRole, setUserRole] = useState() // for setting the Role of User
  const [QuestionAndAnswer, setQuestionAndAnswer] = useState([]) //

  const navigate = useNavigate()

  function ValidateAndCallSearchPage() {
    if (searchText === '' || searchText == null) {
      setSearchSnackbar(true)
    } else {
      const body = {
        searchText,
      }
      const url = `${URL}/question/add-question`
      /*  axios.post(url, body).then((response) => {
        // get the data from the response
        const result = response.data;
        console.log(result);
        if (result["status"] === "success") {
          // navigate to the question page page
          //navigate("/questionPage");
        } else {
          setSearchSnackbar(true);
        }
      }); */
      console.log(searchText)
      //navigate to /search page and pass SearchText
    }
  }
  const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant='filled' {...props} />
  })

  useEffect(() => {
    CheckUserExist()
    //fetch all questions and answers
    fetchQuestionAndAnswers()
  }, [UserId])

  const fetchQuestionAndAnswers = () => {
    setQuestionAndAnswer(SampleQuestionAndAnswer)
    console.log(QuestionAndAnswer)
  }
  const CheckUserExist = () => {
    const Token = window.sessionStorage.getItem('token')
    if (Token == null) setUserId(null)
    else {
      const DecodedToken = jwt_decode(Token)
      setUserId(DecodedToken.id)
      setUserRole(DecodedToken.Role)
    }
  }
  // dialog box open/close functions
  const handleClickOpen = () => {
    setDialogBox(true)
  }
  const handleClose = () => {
    setDialogBox(false)
    setQuestion(null)
  }

  // setting value of textfield
  const handleChange = (event) => {
    setQuestion(event.target.value)
  }
  //saving question fucntions
  const saveQuestion = () => {
    if (Question === '' || Question == null) {
      setQuestionSnackbar(true)
    } else {
      const body = {
        UserId,
        Question,
      }
      //call axios from here to saving question API */
      const url = `${URL}/question/add-question`
      /* axios.post(url, body).then((response) => {
        // get the data from the response
        const result = response.data;
        console.log(result);
        if (result["status"] === "success") {
          setQuestionMsg("Question Added Successfully");
          setSeverity("success");
          setQuestionSnackbar(true);
        } else {
          setQuestionSnackbar(true);
        }
      }); */
      setQuestionMsg('Question Added Successfully')
      setSeverity('success')
      setQuestionSnackbar(true)
      console.log(Question)
      handleClose()
    }
  }

  let CategoryList = [
    {
      category_id: '1',
      name: 'Cereal',
      util_image_url:
        'https://images.all-free-download.com/images/graphiclarge/agriculture_bread_cereals_214850.jpg',
      isMachinery: 0,
    },
    {
      category_id: '2',
      name: 'Animal husbandry products',
      util_image_url:
        'https://media.istockphoto.com/photos/woman-feeding-animals-in-a-goat-farm-picture-id1325266555?b=1&k=20&m=1325266555&s=170667a&w=0&h=lE45qAeEX4wDIznun4zx26gzES4n8vcsKbIXbH9Ktng=',
      isMachinery: 0,
    },
    {
      category_id: '3',
      name: 'Fruits',
      util_image_url:
        'https://images.all-free-download.com/images/graphiclarge/apples_fruit_fruit_trees_239694.jpg',
      isMachinery: 0,
    },
    {
      category_id: '4',
      name: 'Pulses',
      util_image_url:
        'https://www.calgarystampede.com/blog/wp-content/uploads/2016/06/Peas.jpg',
      isMachinery: 0,
    },
    {
      category_id: '5',
      name: 'Vegetables',
      util_image_url:
        'https://images.unsplash.com/photo-1529313780224-1a12b68bed16?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8dmVnZXRhYmxlJTIwZ2FyZGVufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
      isMachinery: 0,
    },
    {
      category_id: '6',
      name: 'Millets',
      util_image_url:
        'https://media.istockphoto.com/photos/millet-crop-in-the-field-picture-id1046793032?k=20&m=1046793032&s=612x612&w=0&h=E8d7vUvaKdeEPB6nkgeGjoU6d1ScgTUBl9bD80bYaQw=',
      isMachinery: 0,
    },
    {
      category_id: '7',
      name: 'Pulses seeds',
      util_image_url:
        'https://media.gettyimages.com/photos/top-view-of-leguminous-seeds-on-rustic-wood-table-picture-id1178796552?k=20&m=1178796552&s=612x612&w=0&h=ltPasGKDc7C3jhQYJJCjkbKPIw99osBlmiM2QiojDAo=',
      isMachinery: 0,
    },
    {
      category_id: '8',
      name: 'Vegetable crop seeds',
      util_image_url:
        'https://media.gettyimages.com/photos/cilantro-seeds-picture-id155375515?k=20&m=155375515&s=612x612&w=0&h=OLCI7i60iM2EEazOLzolO1k4CwpAG5VtOoP0F9gp5kg=',
      isMachinery: 0,
    },
    {
      category_id: '9',
      name: 'Cereals & Millets seeds',
      util_image_url:
        'https://previews.123rf.com/images/9dreamstudio/9dreamstudio1910/9dreamstudio191000307/130810909-healthy-food-concept-cereals-in-bowls-on-cereals-background-top-view.jpg',
      isMachinery: 0,
    },
    {
      category_id: '10',
      name: 'Farm Machinery',
      util_image_url:
        'https://media.istockphoto.com/photos/farmer-operating-combine-harvester-picture-id1302785121?b=1&k=20&m=1302785121&s=170667a&w=0&h=PJebFE9L0MmUT_BjmdGvjNjlKXaI946dkj_bzhur3hE=',
      isMachinery: 1,
    },
  ]
  let SampleQuestionAndAnswer = [
    {
      question_id: '1',
      question: 'What are the factors affecting cereal cultivation?',
      answer:
        'The field establishment of grain crops is the percentage of the sown seed that goes on to produce established plants. At sowing, management and environment are the key issues.Factors affecting the establishment percentage include management factors such as depth of sowing, row spacing, seed size and herbicide application as well as environmental factors such as soil moisture and temperature. The presence of pests and diseases also affect crop establishment.',
      user_id_solver: '3',
      user_id_querier: '1',
      date: '2021-04-25T21:30:05',
      category_id: '1',
    },
    {
      question_id: '2',
      question:
        'Why dairy animals especially buffaloes do not show estrus in summer season?',
      answer:
        'The main reason is due to heat stress and lack of green fodder availability. Farmers are advised to reduce stress by keeping animals in shady areas especially under trees, providing fans/coolers and clean drinking water facilities in the shed. Provision of green fodder (Hay/silage can be used as an alternative in summer months) and allowing the animals for wallowing in ponds especially to buffaloes are also helpful to reduce the effect of summer stress.',
      user_id_solver: '3',
      user_id_querier: '5',
      date: '2021-04-25T21:30:05',
      category_id: '2',
    },
    {
      question_id: '3',
      question: 'How to improve and maintain of Fruit Germplasm?',
      answer:
        'Fruit crops have been cultivated for centuries, both commercially and in amateur gardens, and many species covering a wide range of trees, shrubs, and nonwoody perennials have been the long-term subjects of germplasm improvement programs. For example, different varieties of apples (Malus spp.) were recorded by the ancient Greeks, and selection and breeding of apple have continued since then. In contrast, the cultivated strawberry Fragaria ×  ananassa, an octoploid interspecific hybrid, was developed in the eighteenth century, and the highbush blueberry Vaccinium corymbosum was only domesticated in the twentieth century.Fruit crop species have certain common characteristics: they are long-lived, usually highly heterozygous, and clonally propagated from an elite mother plant. They are also invariably economically minor crops compared with large-scale arable species such as cereals or potatoes, as reflected in the relative research and development resources available. However, fruit is increasingly recognized as an important component of a healthy diet, and consumption of temperate fruit and fruit products, especially juices and dairy derivatives, has risen sharply over the past decade.',
      user_id_solver: '3',
      user_id_querier: '6',
      date: '2021-04-25T21:30:05',
      category_id: '3',
    },
    {
      question_id: '4',
      question: 'Climate requirement for Pulses?',
      answer:
        'Pulse crops are cultivated in Kharif, Rabi and Zaid seasons of the Agricultural year. Rabi crops require mild cold climate during sowing period, during vegetative to pod development cold climate and during maturity / harvesting warm climate. Similarly, Kharif pulse crops require warm climate throughout their life from sowing to harvesting. Summer pulses are habitants of warm climate. Seed is required to pass many stages to produce seed like germination, seedling, vegetative, flowering, fruit setting, pod development and grain maturity / harvesting.',
      user_id_solver: '3',
      user_id_querier: '7',
      date: '2021-04-25T21:30:05',
      category_id: '4',
    },
    {
      question_id: '5',
      question: 'How do I manage pests and diseases on vegetable crops? ',
      answer:
        'Maintaining a healthy farm or garden is the first step to prevent pest or diseases in vegetables. Some important things to handle so as to keep the plants healthy are:Maintaining the soil fertility which includes loose soil, humus rich content, balanced nutrient levels, the addition of trace elements and most importantly treating the soil organically.Creating a rich biodiversity around the farm or garden can keep the vegetable plants healthy and some measures for biodiversity include creating beneficial habitats (ponds, trees), crop rotations, polyculture, mixed or companion farming.Improving hygiene around the cultivation area such as weed control, removal and disposal of damaged or diseased leaves and plants.Using good quality seeds or stocks as planting material. Also not using too old seeds for planting because they become dormant when stored for more than two years.Always the climate and soil should be thoroughly verified for being suitable for a particular variety of crops before planting.It is always advisable to use resistant varieties to keep the level of disease occurrence to the minimum.Diseases could be avoided by sowing the plants and seeds at the right time.There should be a break in cycle of growing vegetables to prevent re- infection to the new plants.Proper plant and row spacing in the farms could prevent spread of diseases and pests among plants.',
      user_id_solver: '3',
      user_id_querier: '8',
      date: '2021-04-25T21:30:05',
      category_id: '5',
    },
    {
      question_id: '6',
      question: 'what are the conditions required for millet farming?',
      answer:
        'Millets mainly grow well on well-drained loamy soils. Proso Millet does not grow well on sandy soils. For the sprouting and germination of the Millet seeds, warm and temperate climate conditions are essential since they are susceptible to damage by cold weather conditions and frosts.Most Millets have a short growing season and they can be grown well in areas where other crops fail to grow. For example, the Sorghum crop can be cultivated even in drought conditions. Most Millets can do with little moisture since they have effective water utilization abilities.',
      user_id_solver: '3',
      user_id_querier: '1',
      date: '2021-04-25T21:30:05',
      category_id: '6',
    },
  ]

  /* for date formatting purpose
  let newDate = new Date("2021-01-09T14:56:23");
  newDate.toDateString();
  // "Sat Jan 09 2021" */

  return (
    <Box>
      <Box
        sx={{
          // bgcolor: "#dfeff8",
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          p: '10px',
        }}
      >
        <Search>
          {/* Start of search bar*/}
          <SearchIconWrapper>
            <SearchIcon fontSize='small' />
          </SearchIconWrapper>
          {/* Autocomplete feature*/}
          <Autocomplete
            filterOptions={filterOptions}
            freeSolo
            disableClearable
            onInputChange={(newInputValue) => {
              //will set the text selected by the user shown in the dropdown
              setSearchText(newInputValue.target.innerText)
            }}
            options={CategoryList.map((option) => option.name)} //this is used to map the sub category to the available options
            PaperComponent={CustomPaper} //this is used to display custom paper
            renderInput={(params) => {
              const { InputLabelProps, InputProps, ...rest } = params
              return (
                <StyledInputBase
                  {...params.InputProps}
                  {...rest}
                  placeholder='Searching for...'
                  onChange={(e) => {
                    setSearchText(e.target.value)
                  }}
                />
              )
            }}
          />
        </Search>
        {/*Search button designed*/}
        <SearchButton
          position='fixed'
          color='success'
          variant='contained'
          onClick={ValidateAndCallSearchPage}
        >
          Search
        </SearchButton>
        <Snackbar //Will display snackbar when searched text is empty and user clicks on search buttons
          open={SearchSnackbar}
          autoHideDuration={6000}
          onClose={() => {
            setSearchSnackbar(false)
          }}
        >
          <Alert severity='warning'>Search Text Cannot Be Empty</Alert>
        </Snackbar>

        {/* render below Ask A Question button after checking USERID */}
        {/* {UserRole == "customer" || UserRole == "farmer"  && ( */}
        <MyButton
          variant='contained'
          color='primary'
          startIcon={<QuestionAnswerTwoToneIcon />}
          onClick={handleClickOpen}
        >
          Ask Question
        </MyButton>
        {/* )} */}

        {/* render below PendingQuestion button when user is expert */}
        {UserRole === 'expert' && (
          <MyButton
            variant='contained'
            color='error'
            startIcon={<QuestionAnswerTwoToneIcon />}
            onClick={() => {
              navigate('/ExpertOpinion/PendingQuestions', {
                state: { state: QuestionAndAnswer },
              })
            }}
          >
            Pending Question
          </MyButton>
        )}
      </Box>

      <Dialog open={DialogBox} onClose={handleClose} fullWidth maxWidth='sm'>
        <DialogTitle>Add Question</DialogTitle>
        <DialogContent>
          <DialogContentText>Write your Question Below</DialogContentText>
          <TextField
            id='dialogQuestion'
            type='text'
            label='Question'
            value={Question}
            onChange={handleChange}
            autoFocus
            margin='dense'
            multiline
            rows={4}
            fullWidth
            variant='filled'
          />
        </DialogContent>
        <DialogActions>
          <Button startIcon={<CancelOutlinedIcon />} onClick={handleClose}>
            Cancel
          </Button>
          <Button startIcon={<SaveAltOutlinedIcon />} onClick={saveQuestion}>
            Save
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        open={QuestionSnackbar}
        autoHideDuration={6000}
        onClose={() => {
          setQuestionSnackbar(false)
          setQuestionMsg(msg1)
          setSeverity('error')
        }}
      >
        <Alert severity={severity}>{questionMsg}</Alert>
      </Snackbar>

      <Container
        sx={{
          paddingY: '10px',
          alignItems: 'center',
          justifyContent: 'center',
          justifyItems: 'center',
        }}
      >
        <Stack direction={'row'} justifyContent='center' alignItems={'center'}>
          <img
            src='https://img.icons8.com/metro/52/000000/faq.png'
            style={{ height: '50px', width: '50px' }}
          />
          <Typography
            variant='h4'
            color='initial'
            align='center'
            mt={5}
            ml={3}
            mb={3}
            textTransform='capitalize'
          >
            Most Frequently Asked Questions
          </Typography>
        </Stack>

        {SampleQuestionAndAnswer.map((question) => {
          return (
            <MyAccordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls='panel1a-content'
                id='panel1a-header'
              >
                <Typography
                  align='left'
                  textTransform='capitalize'
                  variant='h6'
                >
                  {
                    //Fetch questions and update the question over here
                  }
                  {question.question}
                </Typography>
              </AccordionSummary>
              <AccordionDetails
                onClick={() => {
                  navigate(
                    '/utilpage/ExpertsOpinionHome/ExpertOpinionQuestionDetail',
                    {
                      state: {
                        question_id: question.question_id,
                      },
                    }
                  )
                }}
              >
                <Typography align='left'>
                  {
                    //Fetch answer and update the answer over here
                  }
                  {question.answer}
                </Typography>
                <Typography color={'blue'} marginTop={2}>
                  <u>Click to know more</u>
                </Typography>
              </AccordionDetails>
            </MyAccordion>
          )
        })}
      </Container>
    </Box>
  )
}

export default ExpertsOpinionHome
