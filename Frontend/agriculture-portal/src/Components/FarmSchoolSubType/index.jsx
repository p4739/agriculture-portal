import { useEffect, useState } from 'react'
import Container from '@mui/material/Container'
import { Box, styled } from '@mui/system'
import ImageList from '@mui/material/ImageList'
import Typography from '@mui/material/Typography'
import FarmSchoolSubCategory from '../FarmSchoolSubCategory'
import axios from 'axios'
import { HelpOutlineTwoTone } from '@material-ui/icons'
import { useLocation } from 'react-router-dom'
import { Stack } from '@mui/material'

const MyHeading = styled(Typography)(({ theme }) => ({
  fontFamily: 'serif',
  fontWeight: 300,
  fontSize: '6rem',
  lineHeight: 1.167,
  letterSpacing: '0.02em',
  [theme.breakpoints.down('md')]: {
    fontSize: '50px',
  },
  [theme.breakpoints.down('sm')]: {
    fontSize: '40px',
  },
}))
const FarmSchoolSubType = (props) => {
    //const {categoryId} = props
    const {state} = useLocation()
   // console.log(state);
    const categoryId = state.id
    const isMachinery = state.flag
  const [subCategory, setSubCategaory] = useState([]);

  let subCategoryList = [
    {
      sub_category_id: '1',
      name: 'Test',
      url: 'https://www.macmillandictionary.com/external/slideshow/thumb/138920_thumb.jpg',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '1',
    },
    {
      sub_category_id: '2',
      name: 'Tomato',
      url: 'https://m.media-amazon.com/images/I/71DYmqoq-VL._SL1024_.jpg',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '2',
    },
    {
      sub_category_id: '3',
      name: 'Rice',
      url: 'https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/publications/food-beverage-nutrition/foodnavigator-asia.com/headlines/markets/legacy-vs-price-rice-exports-from-vietnam-and-india-vie-for-asean-trade-post-covid-19/11547949-1-eng-GB/Legacy-vs-price-Rice-exports-from-Vietnam-and-India-vie-for-ASEAN-trade-post-COVID-19.jpg',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '3',
    },
    {
      sub_category_id: '4',
      name: 'Wheat',
      url: 'https://www.netmeds.com/images/product-v1/600x600/909012/regular_and_seasonal_vegetables_combo_0_0.jpg',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '4',
    },
    {
      sub_category_id: '5',
      name: 'Apple',
      url: 'https://images.indianexpress.com/2021/08/bhindi_1200_getty.jpg',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '5',
    },
    {
      sub_category_id: '6',
      name: 'Milk',
      url: 'https://miro.medium.com/max/1400/1*0h19diyBx5Ni3LaS2UifCA.jpeg',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '6',
    },
    {
      sub_category_id: '7',
      name: 'Eggs',
      url: 'abcd.html',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '7',
    },
    {
      sub_category_id: '8',
      name: 'Tractor',
      url: 'abcd.html',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '8',
    },
    {
      sub_category_id: '9',
      name: 'Orange',
      url: 'abcd.html',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '9',
    },
    {
      sub_category_id: '10',
      name: 'Pineapple',
      url: 'abcd.html',
      description: 'jingalala huhu',
      measurement_name: 'kg',
      price: '20rs',
      quantity: '30',
      category_id: '10',
    },
  ]

  const FetchsubCategoryList = () => {
    let URL = 'http://localhost:4000'
    let urlFetchSubCategoryList = URL + '/farmschool/fetch-subtype'
    let body = {
      categoryId: categoryId,
    }
    axios.post(urlFetchSubCategoryList, body).then((response) => {
      const result = response.data
      if (result['status'] === 'success') {
        setSubCategaory(result['data'])
      }
      // console.log(subCategory[0].imageId[0].link)
    })
    // setSubCategaory(subCategoryList);
  }

  const breakpoints = {
    xs: 0,
    sm: 600,
    md: 900,
    lg: 1280,
    xl: 1920,
  }

  const getColumns = (width) => {
    if (width < breakpoints.sm) {
      return 1
    } else if (width < breakpoints.md) {
      return 2
    } else if (width < breakpoints.lg) {
      return 3
    } else {
      return 3
    }
  }

  const [columns, setColumns] = useState(getColumns(window.innerWidth))
  const updateDimensions = () => {
    setColumns(getColumns(window.innerWidth))
  }

  useEffect(() => {
    FetchsubCategoryList()
    window.addEventListener('resize', updateDimensions)
    return () => window.removeEventListener('resize', updateDimensions)
  }, [])

  return (
    <Box
      sx={{
        backgroundColor: '#F6F9FC',
      }}
    >
      <Stack direction={'row'} alignItems='center' justifyContent={'center'}>
        <img
          src='https://img.icons8.com/nolan/64/school.png'
          style={{ marginRight: 10 }}
        />
        <Typography
          variant='h3'
          color='#696463'
          sx={{
            fontFamily: 'serif',

            letterSpacing: '0.02em',
          }}
        >
          Farm School
        </Typography>
      </Stack>
      <Container
        maxWidth={false}
        sx={{
          maxWidth: '1280px',
          //minWidth: "1280px",// creating issue in my laptop regarding responsiveness
          justifyContent: 'center',
          paddingTop: '5%',
          paddingBottom: '5%',
          marginLeft: 'auto',
          marginRight: 'auto',
        }}
      >
        <ImageList variant='masonry' cols={columns} gap={42}>
          {subCategory.map((subCategory) => (
            <FarmSchoolSubCategory
              subCategory={subCategory}
              isMachinery={isMachinery}
            />
          ))}
        </ImageList>
      </Container>
    </Box>
  )
}

export default FarmSchoolSubType
