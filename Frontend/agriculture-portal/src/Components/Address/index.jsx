import React from "react";
import { useEffect } from "react";
import { Box } from "@mui/material";
import { Paper, Typography } from "@mui/material";
import { styled } from "@mui/system";
import { TextField, Button } from "@mui/material";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import axios from "axios";
import { URL } from "../../config";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
const AddressBox = styled(Box)({
  width: "85%",
  minHeight: "500px",
  marginTop: "3%",
  marginLeft: "10%",
  marginBottom: "1%",
  padding: "4px",
});
const AddressTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#085E7D",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "#085E7D",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#085E7D",
    },
    "&:hover fieldset": {
      borderColor: "green",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#085E7D",
    },
  },
});

const Address = (props) => {
  const { userId } = props;
  const [Address, setAddress] = React.useState("");
  const [contact, setContact] = React.useState("");
  const [addline1, setAddLine1] = React.useState("");
  const [addline2, setAddLine2] = React.useState("");
  const [landmark, setLandmark] = React.useState("");
  const [pincodeId, setPincodeId] = React.useState("");
  const [pincodeData, setPincodeData] = React.useState("");
  const [pincodes, setPincode] = React.useState([]);
  const [SnackBar, setSnackbar] = React.useState(false);
  const [msg, setmsg] = React.useState("");
  const [severity, setSeverity] = React.useState("");

  const OpenSnackBar = () => {
    setSnackbar(true);
  };
  const CloseSnackbar = () => {
    setSnackbar(false);
  };

  function fetchAddress() {
    //call axios from here and fetch the address
    //use axios here and after that set Cart instead of sample
    const url = `${URL}/user/get-details/${userId}`;
    axios.get(url).then((response) => {
      const result = response.data;
      if (result["status"] === "success") {
        setAddress(result.data);
        // console.log(Address);
        setContact(result.data.contactNo);
        setAddLine1(result.data.addressLine1);
        console.log(result.data);
        setAddLine2(result.data.addressLine2);
        setLandmark(result.data.landmark);
        setPincodeId(result.data.pincodeId);
      } else {
        //user didnt updated address
        //use alert or snackbar
        setmsg("You may have not entered your address");
        setSeverity("error");
        OpenSnackBar();
      }
    });
  }
  function fetchPincodes() {
    //call axios from here and fetch the address
    //use axios here and after that set Cart instead of sample
    const url = `${URL}/user/fetch-pincodes`;
    axios.get(url).then((response) => {
      const result = response.data;
      if (result["status"] === "success") {
        console.log(result.data);
        setPincode(result["data"]);
      } else {
        setmsg("something issue with the pincodes");
        setSeverity("error");
        OpenSnackBar();
      }
    });
  }
  useEffect(() => {
    //fetch address of user and assign it to the
    fetchAddress();
    fetchPincodes();
  }, []);

  const handleChangeContact = (event) => {
    setContact(event.target.value);
  };
  const handleChangeAddLine1 = (event) => {
    setAddLine1(event.target.value);
  };
  const handleChangeAddLine2 = (event) => {
    setAddLine2(event.target.value);
  };
  const handleChangeLandmark = (event) => {
    setLandmark(event.target.value);
  };
  const handleChangePincode = (event) => {
    //set value of pincodeId
    setPincodeId(event.target.value);

    //call axios and fetch required details
    const url = `${URL}/user//fetch-pincode-API`;
    const body = { pincodeId };
    axios.post(url, body).then((response) => {
      const result = response.data;
      if (result["status"] === "success") {
        setPincodeData(result["data"][0]);
        console.log(pincodeData);
      } else {
        setmsg("error in fetching areas");
        setSeverity("error");
        OpenSnackBar();
      }
    });
  };
  const updateAddress = () => {
    if (typeof parseInt(contact) === typeof 1 && contact.length !== 10) {
      setmsg("Contact should be 10 digits only");
      setSeverity("error");
      OpenSnackBar();
    } else if (addline1.length === 0) {
      setmsg("addressLine1 cannot be empty");
      setSeverity("error");
      OpenSnackBar();
    } else if (addline2.length === 0) {
      setmsg("addressLine1 cannot be empty");
      setSeverity("error");
      OpenSnackBar();
    } else if (landmark.length === 0) {
      setmsg("landmark cannot be empty");
      setSeverity("error");
      OpenSnackBar();
    } else {
      const body = {
        userId,
        addline1,
        addline2,
        landmark,
        pincodeId,
        contact,
      };
      console.log(body);
      const url = `${URL}/user/update-address`;
      axios.post(url, body).then((response) => {
        const result = response.data;
        if (result["status"] === "success") {
          //use alert or snackbar
          setmsg("Address Updated Successfully");
          setSeverity("success");
          OpenSnackBar();
        } else {
          //use alert or snackbar
          setmsg("Something went wrong while updating address");
          setSeverity("error");
          OpenSnackBar();
        }
      });
    }
  };

  return (
    <AddressBox>
      <Paper sx={{ width: "100%", minHeight: "300px" }}>
        <Box sx={{ display: "flex", ml: "7%" }}>
          <LocalShippingIcon
            sx={{ mt: "5px", marginX: "2%" }}
            style={{ color: "064635" }}
          />
          <Typography
            variant="subtitle1"
            color="initial"
            sx={{ fontSize: "21px" }}
          >
            Shipping Address
          </Typography>
        </Box>
        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "40%" },
          }}
        >
          <AddressTextField
            id="firstname"
            placeholder="First-Name"
            value={Address.firstName}
            disabled={true}
            type="text"
            autoComplete="on"
          />
          <AddressTextField
            id="lastname"
            placeholder="Last-Name"
            value={Address.lastName}
            disabled={true}
            type="text"
            autoComplete="on"
          />
          <AddressTextField
            id="emailId"
            placeholder="EmailId"
            value={Address.email}
            disabled={true}
            type="text"
            autoComplete="on"
          />
          <AddressTextField
            id="contactnumber"
            placeholder="Contact Number"
            value={contact}
            type="text"
            autoComplete="on"
            onChange={handleChangeContact}
          />
          <AddressTextField
            id="addressline1"
            placeholder="Address Line1"
            value={addline1}
            type="text"
            autoComplete="on"
            onChange={handleChangeAddLine1}
          />
          <AddressTextField
            id="addressline2"
            placeholder="Address Line2"
            value={addline2}
            type="text"
            autoComplete="on"
            onChange={handleChangeAddLine2}
          />
          <AddressTextField
            id="landmark"
            placeholder="Landmark"
            value={landmark}
            type="text"
            autoComplete="on"
            onChange={handleChangeLandmark}
          />
          <AddressTextField
            id="pincode"
            select
            placeholder="Pincode"
            value={pincodeId}
            onChange={handleChangePincode}
            SelectProps={{
              native: true,
            }}
            sx={{ textAlign: "left" }}
            helperText="Please select your pincode"
          >
            {pincodes.map((option) => (
              <option key={option.pincodeId} value={option.value}>
                {option.pincodeId}
              </option>
            ))}
          </AddressTextField>
          <AddressTextField
            id="state"
            type="text"
            disabled={true}
            placeholder="State"
            value={pincodeData.state}
            sx={{ textAlign: "left" }}
            helperText="will update according to pincode"
          />
          <AddressTextField
            id="district"
            placeholder="District"
            type="text"
            disabled={true}
            value={pincodeData.district}
            sx={{ textAlign: "left" }}
            helperText="will update according to pincode"
          />
          <AddressTextField
            id="city"
            placeholder="City"
            type="text"
            disabled={true}
            value={pincodeData.city}
            sx={{ textAlign: "left" }}
            helperText="will update according to pincode"
          />
        </Box>
        <Button
          variant="outlined"
          color="success"
          sx={{ marginY: "10px" }}
          onClick={updateAddress}
        >
          Update Address
        </Button>
      </Paper>
      <Snackbar open={SnackBar} autoHideDuration={6000} onClose={CloseSnackbar}>
        <Alert
          onClose={CloseSnackbar}
          severity={severity}
          sx={{ width: "100%" }}
        >
          {msg}
        </Alert>
      </Snackbar>
    </AddressBox>
  );
};
export default Address;
