import { ArrowForward } from '@material-ui/icons'
import { Divider, Grid, Stack, Typography } from '@mui/material'

export default function InfoItem(props) {
  return (
    <div>
      <Grid container>
        <Grid item xs={12} md={3}>
          <Typography
            variant='h6'
            fontWeight={450}
            noWrap={false}
            textTransform='capitalize'
          >
            {props.name}{' '}
            <ArrowForward
              htmlColor='Green'
              style={{ verticalAlign: 'text-top' }}
            />
          </Typography>
        </Grid>
        <Grid item xs={12} md={9}>
          <Typography
            variant='subtitle1'
            fontSize={18}
            noWrap={false}
            align='left'
          >
            {props.info}
          </Typography>
        </Grid>
      </Grid>
      {/* <Divider variant="middle"  light={true}/> */}
    </div>
  )
}
