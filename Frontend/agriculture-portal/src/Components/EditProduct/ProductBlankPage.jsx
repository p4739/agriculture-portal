import React from 'react'
import { Routes, Route } from 'react-router-dom'
import EditProduct from './EditProduct'
import Product from '../Products/Product'

export default function BlankPage() {
  
  return (
    <div>
      <Routes>
        <Route path='/edit-details' element={<EditProduct />} />
      </Routes>
    </div>
  )
}
