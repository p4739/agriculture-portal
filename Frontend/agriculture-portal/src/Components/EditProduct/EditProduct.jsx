import React from 'react'
import {
  Grid,
  Paper,
  Button,
  ListItem,
  ListItemIcon,
  TextField,
} from '@mui/material'
import { useStyles } from '../Dashboard/Styles'
import { green } from '@mui/material/colors'
import AddBusinessIcon from '@mui/icons-material/AddBusiness'
import { useState } from 'react'
import FormControl from '@mui/material/FormControl'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import InputLabel from '@mui/material/InputLabel'
import axios from 'axios'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useEffect } from 'react'
import { useNavigate } from 'react-router'
import { useLocation } from 'react-router'
import styled from 'styled-components'
import CircularProgress from '@mui/material/CircularProgress'
import LinearProgress from '@mui/material/LinearProgress'
import jwtDecode from "jwt-decode";

export default function EditProduct() {
  const { state } = useLocation()
  const order = state
  // console.log(order.id.productId);

  const classes = useStyles()

  let [productDetails, setProductDetails] = useState([])

  let [category, setCategory] = useState(0)
  let [subCategory, setSubCategory] = useState('')
  let [variety, setVariety] = useState('')
  let [varietyId, setVarietyId] = useState(0)
  let [stock, setStock] = useState(0)
  let [price, setPrice] = useState(0)

  let [productId, setProductId] = useState(0)
  let [subCategoryId, setSubCategoryId] = useState(0)


  let [userId, setUserId] = useState(null)

  const handleUndefinedUserId = () => {

    const Token = sessionStorage.getItem('token')
    const DecodedToken = jwtDecode(Token)
  
    userId = DecodedToken.data.userId
    setUserId(DecodedToken.data.userId)

    console.log(userId);

  }
  

  const fetchProducDetails = async () => {


    const url = `http://localhost:4000/product/get-product/${order.id.productId}`

    await axios.get(url).then((response) => {
      const result = response.data

      if (result['status'] === 'success') {
        productDetails = result['data']
        setProductDetails(result['data'])

        console.log(productDetails[0])

        setSubCategoryId(productDetails[0].subCategoryId)
        subCategoryId = productDetails[0].subCategoryId

        setProductId(productDetails[0].productId)
        productId = productDetails[0].productId

        setCategory(productDetails[0].catName)
        category = productDetails[0].catName
        // console.log(category);

        setSubCategory(productDetails[0].subName)
        subCategory = productDetails[0].subName
        // console.log(subCategory);

        if (productDetails[0].hasVariety === 1) {
          setVariety(productDetails[0].vName)
          variety = productDetails[0].vName

          setVarietyId(productDetails[0].varietyId)
          varietyId = productDetails[0].varietyId

          // console.log(varietyId);
        }
      } else {
        toast.error(result['error'])
      }
    })
  }

  useEffect(() => {
    handleUndefinedUserId() 
    fetchProducDetails()
  }, [])


  const navigate = useNavigate()

  let stockError = false
  let priceError = false

  const handleBlankSubmmit = (e) => {
    e.preventDefault()

    console.log(price)
    console.log(stock)

    if (stock <= 0) {
      stockError = true
      toast.error('Stock should be greater than 0')
      // setStockError(true);
    }
    if (price <= 0) {
      priceError = true
      toast.error('Price should be greater than 0')
      // setPriceError(true);
    }

    console.log(stockError)
    console.log(priceError)

    if (stockError === false && priceError === false) {
      updateProduct()
    }
  }

  const updateProduct = () => {
    price = parseInt(price)
    stock = parseInt(stock)
    userId = parseInt(userId)
    console.log(varietyId)

    const body = { varietyId, productId, price, stock, userId }
    console.log(body)
    const url = `http://localhost:4000/product/edit-product`

    axios.put(url, body).then((response) => {
      const result = response.data
      if (result['status'] === 'success') {
        console.log(result)
        toast.success('Product Updated')
        navigate('/dashboard/product-list')
      } else {
        toast.error(result['error'])
      }
    })
  }

  const setMeasurement = () => {
    // console.log(subCategoryData);

    if (productDetails.length !== 0) {
      if (productDetails[0].measurementId == 1) {
        return 'Kg'
      }
      if (productDetails[0].measurementId == 2) {
        return 'Litre'
      }
      if (productDetails[0].measurementId == 3) {
        return 'Units'
      }
    }
  }

  const CssTextField = styled(TextField)({
    '& label.Mui-focused': {
      color: 'green',
    },
    // "& .MuiInput-underline:after": {
    //     borderBottomColor: "green"
    // },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'green',
      },
      '&:hover fieldset': {
        borderColor: 'green',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'green',
      },
    },
  })

  return (
    <div>
      <Button disabled>
        <ListItem
          // className={classes.editProductSelected}
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              fontSize: "110%",
              marginLeft: '-40%',
            },
            marginLeft: '-120%',
            marginTop: '-22%',
            textTransform: "capitalize",
            fontFamily: "Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif",
            fontSize: "150%",
            color: 'black',
          })}

        >
          <ListItemIcon style={{ color: green['800'], marginTop: '-12px' }}>
            <AddBusinessIcon fontSize='large' />
          </ListItemIcon>
          <h1
            //  className={classes.editProductSelected}

            style={{ marginLeft: '2px' }}> Edit Product</h1>
        </ListItem>
      </Button>

      {productDetails.length !== 0 ? (
        <Paper
          // className = {classes.innerEditProductSelected}
          sx={(theme) => ({
            [theme.breakpoints.down("sm")]: {
              fontSize: "150%",
              marginLeft: '-9%',
            },
            borderRadius: '15px',
            width: '90%',
            marginTop: '1.5%',
            marginLeft: '-15%',

          })}
        >
          <Grid style={{ marginLeft: '25px', marginRight: '25px' }}>
            <form noValidate autoComplete='off' onSubmit={handleBlankSubmmit}>
              <Grid container spacing={2} columns={12}>
                <Grid
                  style={{ marginTop: '4%', marginLeft: '25%' }}
                  align='center'
                  item
                  xs={6}
                >
                  <FormControl fullWidth>
                    <CssTextField
                      // className={classes.addProductEditProductTextField}
                      sx={(theme) => ({
                        width: '90%',
                        [theme.breakpoints.down("sm")]: {
                          width: '100%'
                        },
                      })}

                      label='Category'
                      value={category}
                      variant='outlined'
                    />
                  </FormControl>
                </Grid>
              </Grid>

              <Grid
                style={{ marginLeft: '15%' }}
                container
                spacing={2}
                columns={12}
              >
                <Grid style={{ marginTop: '20px' }} item xs={4}>
                  <FormControl fullWidth>
                    <CssTextField
                      // className={classes.addProductEditProductTextField}
                      sx={(theme) => ({
                        width: '90%',
                        [theme.breakpoints.down("sm")]: {
                          width: '100%'
                        },
                      })}
                      color='primary'
                      label='Sub Category'
                      value={subCategory}
                      variant='outlined'
                    />
                  </FormControl>
                </Grid>

                <Grid style={{ marginTop: '20px' }} item xs={4}>
                  {productDetails[0].hasVariety === 1 ? (
                    <CssTextField
                      // className={classes.addProductEditProductTextField}
                      sx={(theme) => ({
                        width: '90%',
                        [theme.breakpoints.down("sm")]: {
                          width: '100%'
                        },
                      })}
                      color='primary'
                      label='Variety'
                      value={variety}
                      variant='outlined'
                    />
                  ) : (
                    <CssTextField
                      disabled
                      color='primary'
                      style={{ width: '70%', color: 'red' }}
                      label='Variety'
                      value='NA'
                      variant='outlined'
                    />
                  )}
                </Grid>
              </Grid>

              <Grid
                style={{
                  marginTop: '25px',
                  marginLeft: '5%',
                  marginBottom: '50px',
                }}
                container
                spacing={2}
                columns={12}
              >
                <Grid item xs={4}>
                  <TextField
                    style={{ width: '70%' }}
                    required
                    error={stockError}
                    id='outlined-number'
                    label='Stock'
                    type='Number'
                    onChange={(e) => {
                      setStock(e.target.value)
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    style={{ width: '80%' }}
                    required
                    error={priceError}
                    id='outlined-number'
                    label='Price'
                    type='Number'
                    onChange={(e) => {
                      setPrice(e.target.value)
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>

                <Grid item xs={4}>
                  <TextField
                    disabled
                    style={{ width: '50%' }}
                    label={setMeasurement()}
                  ></TextField>
                </Grid>
              </Grid>

              <Grid
                style={{ marginTop: '20px', marginBottom: '6%' }}
                align='center'
              >
                <Button
                  type='submit'
                  style={{
                    width: '270px',
                    backgroundColor: 'rgb(50, 150, 70)',
                    color: 'rgb(255, 255, 255)',
                  }}
                  onClick={handleBlankSubmmit}
                >
                  {' '}
                  Update Product{' '}
                </Button>
              </Grid>
              <br></br>
            </form>
          </Grid>
        </Paper>
      ) : (
        <LinearProgress color='success' />
      )}
    </div>
  )
}