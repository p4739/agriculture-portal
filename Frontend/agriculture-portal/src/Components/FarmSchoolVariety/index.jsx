import { useNavigate } from 'react-router-dom'
import ImageListItem from '@mui/material/ImageListItem'
import ImageListItemBar from '@mui/material/ImageListItemBar'
import { styled } from '@mui/material/styles'
import { IconButton } from '@mui/material'

const MyImageListItem = styled(ImageListItem)`
  ${({ theme }) => `
  cursor: pointer;
  background-color: ${theme.palette.primary.main};
  transition: ${theme.transitions.create(['background-color', 'transform'], {
    duration: theme.transitions.duration.complex,
    easing: theme.transitions.easing.sharp,
  })};
  &:hover {
    background-color: ${theme.palette.secondary.main};
    transform: scale(1.05);
  }
  `}
`

const FarmSchoolVariety = (props) => {
  const { variety } = props
  const navigate = useNavigate()

  return (
    <div>
      <MyImageListItem
        key={variety.varietyId}
        onClick={() => {
          //console.log(variety.varietyId)
          navigate('/utilpage/FarmSchoolHome/FarmSchoolSubType/FarmSchoolMachinerySubVariety/FarmSchoolMachinerySubTopic', {
            state: { id: variety.varietyId },
          })
        }}
      >
        <img
          src={`${variety.link}?w=350?&fit=crop&auto=format`}
          srcSet={`${variety.link}`}
          alt={variety.name}
          loading='eager'
        />
        <ImageListItemBar
          title={variety.name}
          sx={{ textAlign: 'center', filter: 'unset' }}
          actionIcon={
            <IconButton
              sx={{
                color: 'rgba(255, 255, 255, 0.54)',
              }}
            />
          }
        />
      </MyImageListItem>
    </div>
  )
}

export default FarmSchoolVariety
