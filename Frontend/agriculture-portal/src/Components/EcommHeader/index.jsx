import { styled, alpha } from '@mui/material/styles'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import InputBase from '@mui/material/InputBase'
import Badge from '@mui/material/Badge'
import MenuItem from '@mui/material/MenuItem'
import Menu from '@mui/material/Menu'
import Autocomplete from '@mui/material/Autocomplete'
import { Button } from '@mui/material'
import useScrollTrigger from '@mui/material/useScrollTrigger'
import Paper from '@mui/material/Paper'
import { createFilterOptions } from '@mui/material'
import {
  useState,
  useEffect,
  cloneElement,
  forwardRef,
  useContext,
} from 'react'
import jwt_decode from 'jwt-decode'
import Avatar from '@mui/material/Avatar'
import Dialog from '@mui/material/Dialog'
import MuiAlert from '@mui/material/Alert'
import Snackbar from '@mui/material/Snackbar'
import { ButtonBase } from '@mui/material'
import MenuOpenIcon from '@mui/icons-material/MenuOpen'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

//Testing purpose Login page imported
import Login from '../../Pages/Signin'

//Logo which is imported
import logo from '../../Assets/HeaderLogo.png'

//Icons imported for MUI
import SearchIcon from '@mui/icons-material/Search'
import SchoolIcon from '@mui/icons-material/School'
import QuestionAnswerOutlinedIcon from '@mui/icons-material/QuestionAnswerOutlined'
import ShoppingBagOutlinedIcon from '@mui/icons-material/ShoppingBagOutlined'
import PermIdentityOutlinedIcon from '@mui/icons-material/PermIdentityOutlined'
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined'
import { CartQuantityFlagContext } from '../../Pages/HomePage'

//Used for scroll elevation app bar (App bar functionality)
function ElevationScroll(props) {
  const { children } = props
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  })

  return cloneElement(children, {
    elevation: trigger ? 4 : 1,
  })
}

//Used to design the drop down shown when user searches for an item (Searh bar functionality)
const CustomPaper = (props) => {
  return (
    <Paper
      elevation={3}
      {...props}
      sx={[
        {
          width: 'auto',
          marginTop: 0.2,
          borderRadius: 3,
          paddingLeft: '17px',
          paddingRight: '20px',
        },

        (theme) => ({
          [theme.breakpoints.up('md')]: {
            width: '72.75ch',
          },
        }),
      ]}
    />
  )
}

//Used to limit search elements to 4 such that no scoll bar is shown (Searh bar functionality)
const OPTIONS_LIMIT = 4
const defaultFilterOptions = createFilterOptions()

const filterOptions = (options, state) => {
  return defaultFilterOptions(options, state).slice(0, OPTIONS_LIMIT)
}

//Used to design search bar (Searh bar functionality)
const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderTopLeftRadius: 20,
  borderBottomLeftRadius: 20,
  borderTopRightRadius: 0,
  borderBottomRightRadius: 0,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
    borderColor: '#2e7d32',
  },
  borderColor: 'grey',
  borderStyle: 'solid',
  borderWidth: 'thin',
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('xs')]: {
    width: '66ch',
  },
}))

//Used to design search icon (Searh bar functionality)
const SearchIconWrapper = styled('div')(({ theme }) => ({
  paddingLeft: '10px',
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: 'grey',
}))

//Used to deisgn the input base that takes input from user in searh bar (Searh bar functionality)
const StyledInputBase = styled(InputBase)(({ theme }) => ({
  //padding: theme.spacing(1, 1, 1, 0),

  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(2)})`,
    //border: '10px',
    transition: theme.transitions.create('width'),
    fontSize: 'medium',
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '60ch',
    },
  },
}))

//Used to style menu item
const StyledMenuItem = styled(MenuItem)(() => ({
  justifyContent: 'center',
  height: '50px',
}))

//Used to style box used for dividing mobile options evenly
const StyledMobileAppBarBox = styled(Box)(() => ({
  flexGrow: 1,
  justifyContent: 'space-evenly',
  display: 'grid',
  alignItems: 'center',
}))

//Used to style icon button which are displayed in the mobile app bar
const StyledIconButton = styled(IconButton)(() => ({
  '&:hover': {
    color: 'green',
  },
}))

export default function EcommHeader(props) {
  const { subCategory, toggleDrawer } = props
  const flagContext = useContext(CartQuantityFlagContext)
  const { cartQuantityUpdateFlag, setCartQuantityUpdateFlag } = flagContext
  //console.log(subCategory)
  const [UserId, setUserId] = useState(null) //Set User Id
  const [UserNameInitials, setUserNameInitials] = useState(null) //Set User name initials
  const [UserRole, setUserRole] = useState(null) //Set User role id
  const [anchorEl, setAnchorEl] = useState(null) //Used to set the position of menu
  const [BadgeValue, setBadgeValue] = useState(null) //Used to set the cart count
  const [SearchText, setSearchText] = useState('') //Used to set searched text
  const [openSnackbar, setOpenSnackbar] = useState(false)
  const FarmerRoleId = 2 //Farmer role which can see the products option in menu(Currently set to 2--> Farmer)
  //console.log(cartQuantityUpdateFlag)
  //used for dialog opening and closing
  const [open, setOpen] = useState(false)
  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = (value) => {
    setOpen(false)
  }
  const navigate = useNavigate()

  //will be used to check if there is any value in anchorE1, if present--> set it to true, else false
  const isMenuOpen = Boolean(anchorEl)

  //Will fetch SubCategory from props and below provided is sample data
  //let { SubCategory } = props

  //This hook will run everytime there is a change is UserId and will call FetchUserDetails function
  useEffect(() => {
    FetchUserDetails()
  }, [UserId])

  //Will fetch the user details based on token in session storage. Also will fetch the cart details if userId exist
  function FetchUserDetails() {
    const Token = sessionStorage.getItem('token')
    if (Token == null) {
      setUserId(null)
    } else {
      const DecodedToken = jwt_decode(Token)
      console.log(DecodedToken)
      const Name =
        DecodedToken.data.firstName.charAt(0) +
        ' ' +
        DecodedToken.data.lastName.charAt(0)
      setUserId(DecodedToken.data.userId)
      setUserRole(DecodedToken.data.roleId)
      setUserNameInitials(Name)
      console.log(UserNameInitials)

      //Call User/Cart api,
      //compute the total rows fetched
      //setBadgeValue(7)
    }
    //console.log(UserId)
  }

  function FetchCartQuantity() {
    const url = 'http://localhost:4000/cart/fetch-cart-quantity'
    const userId = UserId
    const body = { userId }
    axios.post(url, body).then((response) => {
      //console.log(response)
      const result = response.data
      if (result.status === 'error') {
        alert('No quantity to fetch')
      } else if (result.status === 'success') {
        //console.log(result.count)
        setBadgeValue(result.count)
      }
    })
  }

  useEffect(() => {
    if (UserId != null) {
      FetchCartQuantity()
    }
  }, [UserId, cartQuantityUpdateFlag])

  //Used to show the menu when user clicks on Avatar (Set anchorEl variable)
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget)
  }

  //Used to close the menu (Set anchorEl variable to null)
  const handleMenuClose = () => {
    setAnchorEl(null)
  }

  //Used to navigate to OrderList page
  const handleOrderOption = () => {
    //navigate to /orderslist page and pass UserId as state
    navigate('/dashboard/orders')
    setAnchorEl(null)
  }

  //Used to navigate to ProfileInfo page
  const handleProfileOption = () => {
    //navigate to /ProfileInfo page and pass UserId as state
    navigate('/dashboard/edit-profile')
    setAnchorEl(null)
  }

  //Used to navigate to Products page
  const handleProductsOption = () => {
    //navigate to /Products page and pass UserId as state
    navigate('/dashboard/product-list')
    setAnchorEl(null)
  }

  //Used to navigate to Logout page
  const handleLogoutOption = () => {
    //navigate to /Logout page.
    navigate('/logout')
    setAnchorEl(null)
  }

  //will render the menu based on anchorE1 variable
  const renderMenu = UserId && (
    <Menu
      anchorEl={anchorEl}
      MenuListProps={{
        disablePadding: true,
      }}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 20,
      }}
      PaperProps={{
        style: {
          width: '175px',
          borderRadius: 0,
        },
      }}
      keepMounted
      open={isMenuOpen} //will be used to open/close based on anchorE1 value
      onClose={handleMenuClose}
    >
      <StyledMenuItem onClick={handleOrderOption} divider>
        Orders
      </StyledMenuItem>
      <StyledMenuItem onClick={handleProfileOption} divider>
        Profile
      </StyledMenuItem>
      {UserRole === FarmerRoleId && (
        <StyledMenuItem onClick={handleProductsOption} divider>
          Products
        </StyledMenuItem>
      )}
      <StyledMenuItem onClick={handleLogoutOption} divider autoFocus>
        Logout
      </StyledMenuItem>
    </Menu>
  )

  //will check if user is logged in or not. If it is not, then will show the dialog box(of login).
  //this function is used on cart, profile button
  function isUserLoggedIn() {
    if (UserId == null) {
      handleClickOpen()
    } else {
      navigate('/cart')
    }
  }

  //will validate if the input text is not null and will call search page
  function ValidateAndCallSearchPage() {
    if (SearchText === '' || SearchText == null) {
      setOpenSnackbar(true)
    } else {
      console.log(SearchText)
      //navigate to /search page and pass SearchText, UserId as state
      navigate('/search', { state: { searchText: SearchText } })
    }
  }

  const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant='filled' {...props} />
  })

  //Main rendering of header component
  return (
    <Box sx={{ flexGrow: 1 }}>
      <ElevationScroll>
        <AppBar
          position='fixed'
          style={{
            backgroundColor: 'white',
            paddingTop: '10px',
            paddingBottom: '10px',
          }}
        >
          {/* Used to display searchbar, profileicon, carticon ...*/}
          <Toolbar>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }} />
            <Box sx={{ width: '75px', display: { xs: 'none', md: 'flex' } }} />
            {/* can add the logo here and make it route to home page*/}
            <ButtonBase
              sx={{
                '&:hover': { backgroundColor: 'inherit' },
                height: '0px',
                width: '0px',
                marginTop: '20px',
                display: { xs: 'none', md: 'flex' },
              }}
              color='#fff'
            >
              <img
                component='img'
                src={`${logo}`} //Added imported logo here
                height={125}
                width={175}
                alt=''
                onClick={() => {
                  navigate('/')
                }}
              />
            </ButtonBase>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }} />
            <Box sx={{ width: '80px', display: { xs: 'none', md: 'flex' } }} />
            <Box sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }}>
              <IconButton //Will display the menu icon button
                size='small'
                aria-haspopup='true'
                onClick={props.toggleDrawer}
                sx={{
                  padding: '11px',
                  size: '500',
                  backgroundColor: '#1B5E20',
                  color: '#ffffff',
                  '&:hover': { backgroundColor: '#F3F5F9' },
                }}
              >
                <MenuOpenIcon color='black' fontSize='medium' />
              </IconButton>
            </Box>
            <Search>
              {/* Start of search bar*/}
              <SearchIconWrapper>
                <SearchIcon fontSize='small' />
              </SearchIconWrapper>
              {/* Autocomplete feature*/}
              <Autocomplete
                filterOptions={filterOptions}
                freeSolo
                disableClearable
                onInputChange={(newInputValue) => {
                  //will set the text selected by the user shown in the dropdown
                  setSearchText(newInputValue.target.innerText)
                }}
                options={subCategory.map((option) => option.name)} //this is used to map the sub category to the available options
                PaperComponent={CustomPaper} //this is used to display custom paper
                renderInput={(params) => {
                  const { InputLabelProps, InputProps, ...rest } = params
                  return (
                    <StyledInputBase
                      {...params.InputProps}
                      {...rest}
                      placeholder='Searching for...'
                      onChange={(e) => {
                        setSearchText(e.target.value)
                      }}
                    />
                  )
                }}
              />
            </Search>

            {/*Search button designed*/}
            <Button
              position='fixed'
              color='success'
              variant='contained'
              sx={{
                height: '41px',
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0,
                borderTopRightRadius: 18,
                borderBottomRightRadius: 18,
                textAlign: 'right',
                width: '120px',
                display: 'inline-flex',
                fontWeight: 400,
                textTransform: 'capitalize',
                boxShadow: 'none',
                '&:hover': { boxShadow: 'none' },
              }}
              onClick={ValidateAndCallSearchPage}
            >
              Search
            </Button>
            <Snackbar //Will display snackbar when searched text is empty and user clicks on search buttons
              open={openSnackbar}
              autoHideDuration={6000}
              onClose={() => {
                setOpenSnackbar(false)
              }}
            >
              <Alert severity='warning' sx={{ width: '100%' }}>
                Searched text cant be empty
              </Alert>
            </Snackbar>

            <Box sx={{ flexGrow: 1 }} />

            {/*This part will be show if the width is greater than 900px*/}
            <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
              {UserId == null && (
                <IconButton //Will display the profile icon button
                  size='small'
                  aria-haspopup='true'
                  onClick={handleClickOpen}
                  sx={{
                    padding: '11px',
                    backgroundColor: '#F3F5F9',
                    '&:hover': { backgroundColor: '#F3F5F9' },
                  }}
                >
                  <PermIdentityOutlinedIcon color='black' fontSize='small' />
                </IconButton>
              )}

              {UserId && (
                <Avatar //Will display the avatar when user has logged in
                  aria-haspopup='true'
                  onClick={handleProfileMenuOpen}
                  sx={{
                    color: 'rgba(0, 0, 0, 0.54)',
                    padding: '2px',
                    fontSize: 'medium',
                    fontWeight: 'bold',
                    backgroundColor: '#F3F5F9',
                    '&:hover': { backgroundColor: '#F3F5F9' },
                  }}
                >
                  {UserNameInitials}
                  {/*Will show the initials of logged in user*/}
                </Avatar>
              )}

              <IconButton //Will display the cart icon button
                size='large'
                aria-label='show 4 new mails'
                sx={{
                  padding: '10px',
                  backgroundColor: '#F3F5F9',
                  marginLeft: '20px',
                  '&:hover': { backgroundColor: '#F3F5F9' },
                }}
                onClick={isUserLoggedIn} //will first check if the user has logged in or not
              >
                <Badge badgeContent={BadgeValue} color='success'>
                  {/*Used to display the count of items in cart*/}
                  <ShoppingBagOutlinedIcon color='black' />
                </Badge>
              </IconButton>
            </Box>

            {/* responsive app drawer, used for mobile devices and will be shown if width is less than 900px */}
            <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
              <AppBar
                position='fixed'
                width='100%'
                style={{
                  backgroundColor: 'white',
                }}
                sx={{ top: 'auto', bottom: 0 }} //Will fix the app bar at the bottom of the page
              >
                <Toolbar>
                  {/*if we need to seperate it eqully, add flex = 1 to each StyledMobileAppBarBox  */}
                  <StyledMobileAppBarBox>
                    <StyledIconButton //Home icon button
                      onClick={() => {
                        navigate('/')
                      }}
                    >
                      <HomeOutlinedIcon color='black' />
                    </StyledIconButton>
                    <Typography
                      variant='div'
                      color='#757575'
                      sx={{ display: { xs: 'none', sm: 'flex' } }}
                    >
                      Home
                    </Typography>
                  </StyledMobileAppBarBox>

                  <StyledMobileAppBarBox>
                    <StyledIconButton //farmschool icon button
                      onClick={() => {
                        navigate('/utilpage/FarmSchoolHome')
                      }}
                    >
                      <SchoolIcon color='black' />
                    </StyledIconButton>

                    <Typography
                      variant='div'
                      color='#757575'
                      sx={{ display: { xs: 'none', sm: 'flex' } }}
                    >
                      Farm School
                    </Typography>
                  </StyledMobileAppBarBox>

                  <StyledMobileAppBarBox>
                    <StyledIconButton //expertsopinion icon button
                      onClick={() => {
                        navigate('/utilpage/ExpertsOpinionHome')
                      }}
                    >
                      <QuestionAnswerOutlinedIcon color='black' />
                    </StyledIconButton>

                    <Typography
                      variant='div'
                      color='#757575'
                      sx={{ display: { xs: 'none', sm: 'flex' } }}
                    >
                      Experts Opinion
                    </Typography>
                  </StyledMobileAppBarBox>

                  <StyledMobileAppBarBox>
                    <StyledIconButton //cart icon button
                      onClick={() => {
                        if (UserId != null) {
                          navigate('/cart')
                        } else {
                          navigate('/signin')
                        }
                      }}
                    >
                      <ShoppingBagOutlinedIcon color='black' />
                    </StyledIconButton>

                    <Typography
                      variant='div'
                      color='#757575'
                      sx={{ display: { xs: 'none', sm: 'flex' } }}
                    >
                      Cart
                    </Typography>
                  </StyledMobileAppBarBox>

                  <StyledMobileAppBarBox>
                    <StyledIconButton //profile icon button
                      onClick={() => {
                        if (UserId != null) {
                          navigate('/dashboard/edit-profile')
                        } else {
                          navigate('/signin')
                        }
                      }}
                    >
                      <PermIdentityOutlinedIcon color='black' />
                    </StyledIconButton>

                    <Typography
                      variant='div'
                      color='#757575'
                      sx={{ display: { xs: 'none', sm: 'flex' } }}
                    >
                      Profile
                    </Typography>
                  </StyledMobileAppBarBox>
                </Toolbar>
              </AppBar>
            </Box>

            <Box sx={{ flexGrow: 1 }} />
          </Toolbar>

          {/* Used to display farmschool, experts opinion and will be only displayed when width is greater than 900px*/}
          <Toolbar
            sx={{
              justifyContent: 'flex-end',
              display: { xs: 'none', md: 'flex' },
            }}
          >
            <Box sx={{ width: '85ch' }} />
            <Box sx={{ flex: 3 }} />

            {/*Will only be displayed if width is greater than 900px*/}
            <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
              <Button //Farm school button design
                variant='text'
                sx={{
                  height: '40px',
                  width: '130px',
                  color: 'text.secondary',
                  fontWeight: 500,
                  textTransform: 'capitalize',
                  boxShadow: 'none',
                  '&:hover': { boxShadow: 'none', color: 'success.main' },
                }}
                onClick={() => {
                  navigate('/utilpage/FarmSchoolHome')
                }}
              >
                Farm School
              </Button>
              <Button //Experts Opinion button design
                variant='text'
                sx={{
                  height: '40px',
                  width: '130px',
                  color: 'text.secondary',
                  fontWeight: 500,
                  textTransform: 'capitalize',
                  boxShadow: 'none',
                  '&:hover': { boxShadow: 'none', color: 'success.main' },
                }}
                onClick={() => {
                  navigate('/utilpage/ExpertsOpinionHome')
                }}
              >
                Experts Opinion
              </Button>
            </Box>
            <Box sx={{ flex: '1' }} />
          </Toolbar>
        </AppBar>
      </ElevationScroll>

      {/*Used for rendering menu options*/}
      {renderMenu}

      {/*Used to display the dialog box that shows login component*/}
      <Dialog open={open} onClose={handleClose} fullWidth>
        <Login />
      </Dialog>
    </Box>
  )
}
