import { Box, Grid, Snackbar } from '@mui/material'
import axios from 'axios'
import { forwardRef, useEffect, useState } from 'react'
import SubCategory from '../SubCategory'
import Typography from '@mui/material/Typography'
import Footer from '../Footer'
import MuiAlert from '@mui/material/Alert'
import { useLocation } from 'react-router-dom'

export default function CategoryList(props) {
  //will get subCategory and userId as args
  const { state } = useLocation()
  //console.log(state)

  const [openSnackbar, setOpenSnackbar] = useState(false)
  const [AlertMessage, setAlertMessage] = useState()
  const [AlertType, setAlertType] = useState('warning')

  const UserId = 1

  const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant='filled' {...props} />
  })
  return (
    <div>
      {state.subCategory.length === 0 && (
        <Typography
          variant='h3'
          color='#606674'
          align='center'
          sx={{ textTransform: 'capitalize' }}
          mb={4}
        >
          Oops!!! No result found
        </Typography>
      )}
      {state.subCategory.length !== 0 && (
        <div>
          <Grid
            container
            rowSpacing={4}
            columnSpacing={{ xs: 2, md: 2 }}
            direction={{ xs: 'column', sm: 'row', md: 'row' }}
            alignContent='center'
            mb={4}
          >
            {state.subCategory.map((subcategory) => {
              return (
                <Grid item md={4} sm={6} textAlign={'-webkit-right'}>
                  <SubCategory
                    item={subcategory}
                    UserId={UserId}
                    key={subcategory.subCategoryId}
                  />
                </Grid>
              )
            })}
          </Grid>
        </div>
      )}

      <Snackbar //Will display snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={() => {
          setOpenSnackbar(false)
        }}
      >
        <Alert severity={AlertType} sx={{ width: '100%' }}>
          {AlertMessage}
        </Alert>
      </Snackbar>

      <Footer />

      <Box
        sx={(theme) => ({
          [theme.breakpoints.down('md')]: {
            marginBottom: 7,
          },
          [theme.breakpoints.up('md')]: {
            marginBottom: 0,
          },
        })}
      ></Box>
    </div>
  )
}
