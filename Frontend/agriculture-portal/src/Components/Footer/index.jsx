import Paper from '@mui/material/Paper'
import { Grid } from '@mui/material'
import logo from '../../Assets/FooterLogo.png'
import { styled } from '@mui/material/styles'

//Grid item styling
const Blocks = styled(Grid)(({ theme }) => ({
  color: 'white',
  fontFamily:
    'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif',
  fontWeight: 400,
  fontSize: 14,
  lineHeight: 1.75,
}))

export default function Footer() {
  return (
    //#141850 --> different color
    <Paper
      sx={{ backgroundColor: '#152D35', padding: '40px', textAlign: 'left' }}
    >
      {/*Logo to be added over here */}
      <Grid container>
        <Grid item marginTop={-3} marginLeft={-2}>
          <img
            component='img'
            src={`${logo}`} //Added imported logo here
            height={125}
            width={175}
            alt=''
          />
        </Grid>
      </Grid>
      <Grid container spacing={2} marginBottom={-2.5}>
        {/*About us section */}
        <Blocks
          item
          md={6}
          //color={'#AEB4BE'}
        >
          <div style={{ fontSize: 22 }}>About Us</div>
          <br />
          <div>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </div>
        </Blocks>
        {/*Reach us section */}
        <Blocks
          item
          md={6}
          //color={'#AEB4BE'}
        >
          <div style={{ fontSize: 22 }}>Reach Us</div>
          <br />
          <div>Phone : 630-885-9200</div>
          <div>Email : info@brandcatmedia.com</div>
          <div>Fax : 630-830.2006</div>
        </Blocks>
        <Grid item md={6}></Grid>
      </Grid>
    </Paper>
  )
}
