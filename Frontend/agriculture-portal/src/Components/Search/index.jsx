import { Box, Grid, Snackbar } from '@mui/material'
import axios from 'axios'
import { forwardRef, useEffect, useState } from 'react'
import SubCategory from '../SubCategory'
import Typography from '@mui/material/Typography'
import Footer from '../Footer'
import MuiAlert from '@mui/material/Alert'
import { useLocation } from 'react-router-dom'

export default function Search(props) {
  const { state } = useLocation()
  //console.log(state)
  //will fetch this properties as args
  const UserId = 1
  const SearchText = 'i'

  const [SearchResult, SetSearchResult] = useState([])
  const [openSnackbar, setOpenSnackbar] = useState(false)
  const [AlertMessage, setAlertMessage] = useState()
  const [AlertType, setAlertType] = useState('warning')

  function fetchSearchResult() {
    const url = 'http://localhost:4000/item/search-item'
    const searchText = state.searchText
    const userId = UserId
    const body = { searchText, userId }
    axios.post(url, body).then((response) => {
      //console.log(response)
      const result = response.data
      //console.log(result)
      if (result.status === 'error') {
        setAlertType('error')
        setAlertMessage('No result found')
        setOpenSnackbar(true)
      }
      if (result.status === 'success') {
        //console.log(result.data)
        SetSearchResult(result.data)
      }
    })
  }

  useEffect(() => {
    fetchSearchResult()
  }, [state.searchText])

  const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant='filled' {...props} />
  })
  return (
    <div>
      {SearchResult.length === 0 && (
        <Typography
          variant='h3'
          color='#606674'
          align='center'
          sx={{ textTransform: 'capitalize' }}
          mb={4}
        >
          Oops!!! No result found
        </Typography>
      )}
      {SearchResult.length !== 0 && (
        <div>
          <Grid
            container
            rowSpacing={4}
            columnSpacing={{ xs: 2, md: 2 }}
            direction={{ xs: 'column', sm: 'row', md: 'row' }}
            alignContent='center'
            mb={4}
          >
            {SearchResult.map((result) => {
              return (
                <Grid item md={4} sm={6} textAlign={'-webkit-right'}>
                  <SubCategory
                    item={result}
                    UserId={UserId}
                    key={result.subCategoryId}
                  />
                </Grid>
              )
            })}
          </Grid>
        </div>
      )}
      <Snackbar //Will display snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={() => {
          setOpenSnackbar(false)
        }}
      >
        <Alert severity={AlertType} sx={{ width: '100%' }}>
          {AlertMessage}
        </Alert>
      </Snackbar>

      <Footer />

      <Box
        sx={(theme) => ({
          [theme.breakpoints.down('md')]: {
            marginBottom: 7,
          },
          [theme.breakpoints.up('md')]: {
            marginBottom: 0,
          },
        })}
      ></Box>
    </div>
  )
}
