import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom"

export default function Category({ cat, icon,subCategory }) {
  // const cat = props.cat
  // const icon = props.icon
  //debugger
  //console.log(cat)
  // console.log(icon)
  const navigate = useNavigate()
  const [raised, setraised] = React.useState(false);
  const handleHover = () => {
    setraised(!raised);
  };
  const getSubCategories = ((cat)=>{
      return subCategory.filter((sbcat)=>sbcat.categoryId===cat.categoryId)
  })
  //console.log(subCategory);
  //console.log(getSubCategories(cat))
  return (
    <div>
      <Card
        elevation={raised ? 21 : 2}
        onMouseLeave={handleHover}
        onMouseEnter={handleHover}
        sx={{ borderRadius: 2, maxWidth: [200,250,280], height: [75,80,100] }}
      >
        <CardActionArea sx={{ height: "100%" }} onClick={()=>{
          navigate('/categorylist',{state:{categoryId:cat.categoryId,subCategory:getSubCategories(cat)}})
        }}>
          <Stack direction="row">
            <CardMedia
              sx={{ width: [40,60], height: [40,60], mt: [1,2], ml: 2 }}
              component="img"
              image={icon}
            ></CardMedia>
            <CardContent sx={{ width: [120,150,160,180,190] }}>
              <Typography variant="h6" fontSize={{xs: 12, sm:17}}>
                {cat.name}
              </Typography>
            </CardContent>
          </Stack>
        </CardActionArea>
      </Card>
    </div>
  );
}

{
  /* <Typography
                    variant="caption text"
                    fontSize={12}
                    color="#DF7689"
                  >
                    {cat.subtitle}
                  </Typography> */
}
