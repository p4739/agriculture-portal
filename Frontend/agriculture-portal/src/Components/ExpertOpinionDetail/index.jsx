import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  Paper,
  Snackbar,
  Stack,
  TextField,
  Typography,
} from '@mui/material'
import MuiAlert from '@mui/material/Alert'
import Container from '@mui/material/Container'
import axios from 'axios'
import { forwardRef, useEffect, useLayoutEffect, useState } from 'react'
import Comment from '../Comment'
import jwt_decode from 'jwt-decode'
//Need to later link login page
//import Login from './Login'

export default function ExpertsOpinionDetail(props) {
  //Will take it as args
  const questionId = 1

  const [Question, setQuestion] = useState('')
  const [QuerierName, setQuerierName] = useState('')
  const [ExpertName, setExpertName] = useState('')
  const [QuestionTimestamp, setQuestionTimestamp] = useState('')
  const [Answer, setAnswer] = useState()
  const [CommentArr, setCommentArr] = useState([])
  const [CommentInputText, setCommentInputText] = useState('')
  const [CommentUpdater, setCommentUpdater] = useState(false)
  const [userId, setUserId] = useState(null) //Set User Id
  const [userRoleId, setUserRoleId] = useState(null) //Set User Role Id
  const [open, setOpen] = useState(false)
  const [answerOpen, setanswerOpen] = useState(false)
  const [AnswerAddedByExpert, setAnswerAddedByExpert] = useState('')
  const [AnswerUpdater, setAnswerUpdater] = useState(false)
  const [openSnackbar, setOpenSnackbar] = useState(false)
  const [AlertMessage, setAlertMessage] = useState()
  const [AlertType, setAlertType] = useState('warning')

  const expertRoleId = 3

  const handleClose = () => {
    setOpen(false)
  }

  const handleClickOpen = () => {
    setOpen(true)
  }

  //will fetch the question and answer using question Id
  function fetchQnA() {
    const body = { questionId }
    const url =
      'http://localhost:4000/expertsopinion/fetch-question-specific-details'

    axios.post(url, body).then((response) => {
      const result = response.data
      if (result.status === 'error') {
        setAlertType('error')
        setAlertMessage('Error fetching question data')
        setOpenSnackbar(true)
        //setAlertMessage('')
      }
      if (result.status === 'success') {
        //alert('success')
        setQuestion(result.result.question)
        setQuestionTimestamp(
          new Date(result.result.createdTimestamp).toDateString()
        )
        setQuerierName(
          result.result.querierId.firstName +
            ' ' +
            result.result.querierId.lastName
        )
        setAnswer(result.result.answer)
        if (result.result.expertId != null) {
          setExpertName(
            result.result.expertId.firstName +
              ' ' +
              result.result.expertId.lastName
          )
        }
      }
    })
  }
  useEffect(() => {
    fetchQnA()
  }, [questionId, AnswerUpdater])

  //will fetch comments specific to the question
  function fetchComments() {
    const body = { questionId }
    //console.log('fetch comment ran')
    const url =
      'http://localhost:4000/expertsopinion/fetch-question-specific-comment'

    axios.post(url, body).then((response) => {
      const result = response.data
      if (result.status === 'error') {
        setCommentArr([])
        //console.log(result.error)
      }
      if (result.status === 'success') {
        //console.log(result.comment)
        setCommentArr(result.comment)
      }
    })
  }

  useEffect(() => {
    fetchComments()
  }, [CommentUpdater])

  async function FetchUserDetails() {
    //will need to later fetch it from session storage
    // window.sessionStorage.setItem(
    //   'token',
    //   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyIiwiRk4iOiJKb2huIiwiTE4iOiJQb2ludGluZyIsIlN0YXRlIjoiR29hIiwiUm9sZSI6MX0.9QH5Be4LN0fJEtWd-d923CBIf7o7DFFjyznZuC6EkLw'
    // )
    //console.log('inside fetch user details')
    const Token = await window.sessionStorage.getItem('token')
    if (Token == null) {
      setUserId(null)
    } else {
      if (userId == null) {
        const DecodedToken = await jwt_decode(Token)
        setUserId(DecodedToken.id)
        setUserRoleId(DecodedToken.Role)
      }
    }
  }
  //This hook will run everytime there is a change is UserId and will call FetchUserDetails function
  useLayoutEffect(() => {
    FetchUserDetails()
  }, [userId])

  //will add the comment
  function addComment() {
    if (userId == null) {
      handleClickOpen()
    } else {
      if (CommentInputText == '') {
        //alert('empty comment')
        setAlertType('info')
        setAlertMessage('Cannot add empty comment')
        setOpenSnackbar(true)
      } else {
        let comment = CommentInputText
        const body = { questionId, userId, comment }
        console.log('insert comment ran')
        const url = 'http://localhost:4000/expertsopinion/insert-comment'

        axios.post(url, body).then((response) => {
          const result = response.data
          if (result.status === 'error') {
            //alert(result.error)
            setAlertType('error')
            setAlertMessage(result.error)
            setOpenSnackbar(true)
          }
          if (result.status === 'success') {
            setCommentUpdater(!CommentUpdater)
            setCommentInputText('')
          }
        })
      }
    }
  }

  //will add the answer
  function addAnswer() {
    if (AnswerAddedByExpert == '') {
      setAlertType('info')
      setAlertMessage('Cannot add empty answer')
      setOpenSnackbar(true)
    } else {
      let answer = AnswerAddedByExpert
      const body = { questionId, userId, answer }
      console.log('answer ran')
      const url = 'http://localhost:4000/expertsopinion/save-answer'

      axios.put(url, body).then((response) => {
        const result = response.data
        if (result.status === 'error') {
          //alert(result.error)
          setAlertType('error')
          setAlertMessage(result.error)
          setOpenSnackbar(true)
        }
        if (result.status === 'success') {
          setanswerOpen(false)
          setAnswerUpdater(!AnswerUpdater)
          //setCommentInputText('')
        }
      })
    }
  }

  const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant='filled' {...props} />
  })

  return (
    <div style={{ backgroundColor: '#F6F9FC' }}>
      <Container
        maxWidth={false}
        sx={{
          maxWidth: '1280px',
          marginBottom: '2rem',
        }}
      >
        <Paper
          variant='elevation'
          sx={{ padding: 3, textTransform: 'capitalize' }}
        >
          <Stack direction={'column'}>
            <Typography variant='h5' sx={{ textAlign: 'center', mb: 2 }}>
              {Question}
            </Typography>
            <Stack direction={'row'} mb={2}>
              <Typography flexGrow={1} />
              <Paper
                sx={{ backgroundColor: '#D9EAF7', padding: 1 }}
                variant='outlined'
              >
                <Stack direction={'column'}>
                  <Typography
                    variant='caption'
                    color={'#6A737C'}
                    textAlign={'left'}
                  >
                    {QuestionTimestamp}
                  </Typography>
                  <Typography variant='subtitle1' color={'#152D35'}>
                    {QuerierName}
                  </Typography>
                </Stack>
              </Paper>
            </Stack>
            {Answer != null && (
              <>
                <Typography variant='h6' textAlign={'left'}>
                  Answer
                </Typography>
                <Grid container spacing={2}>
                  <Grid item sm={1} xs={2}>
                    <img src='https://img.icons8.com/doodle/48/000000/checkmark.png' />
                  </Grid>
                  <Grid item sm={11} xs={10}>
                    <Paper
                      variant='outlined'
                      sx={{ backgroundColor: '#F6F6F6', padding: 0.75 }}
                    >
                      <Stack direction={'column'}>
                        <Typography variant='subtitle1'>{Answer}</Typography>
                        <Stack direction={'row'}>
                          <Typography flexGrow={1} />
                          <Paper
                            sx={{
                              backgroundColor: '#D9EAF7',
                              padding: 1,
                              mt: 1,
                            }}
                            variant='outlined'
                          >
                            <Typography variant='subtitle1' color={'#152D35'}>
                              Answered by expert: {ExpertName}
                            </Typography>
                          </Paper>
                        </Stack>
                      </Stack>
                    </Paper>
                  </Grid>
                </Grid>
              </>
            )}
            {Answer == null && userRoleId === expertRoleId && (
              <>
                <Stack direction={'row'}>
                  <Typography flexGrow={1} />
                  <Button
                    variant='contained'
                    color='success'
                    onClick={() => {
                      setanswerOpen(true)
                    }}
                  >
                    Answer
                  </Button>
                </Stack>
              </>
            )}
            <Typography variant='h6' mt={2} textAlign={'left'}>
              Comments
            </Typography>
            {CommentArr.map((comment) => {
              return (
                <Comment
                  comment={comment}
                  userId={userId}
                  key={comment.commentId}
                  setCommentUpdater={setCommentUpdater}
                  CommentUpdater={CommentUpdater}
                />
              )
            })}
            {CommentArr.length == 0 && (
              <Typography
                variant='subtitle1'
                mt={2}
                textAlign='center'
                color={'#6A737C'}
              >
                No comments
              </Typography>
            )}
            <Typography variant='h6' mt={3} mb={2} textAlign={'left'}>
              Your Comment
            </Typography>
            <Grid container spacing={2}>
              <Grid item sm={9} md={10} xs={12}>
                <textarea
                  style={{ width: '98%', fontSize: 17, padding: 8 }}
                  rows={10}
                  value={CommentInputText}
                  onChange={(e) => {
                    setCommentInputText(e.target.value)
                    //console.log(CommentInputText)
                  }}
                  placeholder='Enter your comment here'
                ></textarea>
              </Grid>
              <Grid item sm={1.5} md={1} alignSelf={'self-end'}>
                <Button
                  variant='contained'
                  color='success'
                  sx={{ textTransform: 'capitalize', width: 85 }}
                  onClick={() => {
                    addComment()
                  }}
                >
                  Save
                </Button>
              </Grid>
              <Grid item sm={1.5} md={1} alignSelf={'self-end'}>
                <Button
                  variant='outlined'
                  sx={{
                    color: 'text.secondary',
                    textTransform: 'capitalize',
                    width: 85,
                  }}
                  onClick={() => {
                    setCommentInputText('')
                  }}
                >
                  Clear
                </Button>
              </Grid>
            </Grid>
          </Stack>
        </Paper>
      </Container>
      <Dialog
        open={open}
        close={handleClose}
        fullWidth
        onBackdropClick={handleClose}
      >
        {/* <Login /> */}
      </Dialog>
      {
        //Answer dialog
      }
      <Dialog
        open={answerOpen}
        close={() => {
          setanswerOpen(false)
        }}
        onBackdropClick={() => {
          setanswerOpen(false)
        }}
      >
        <DialogTitle>Expert Opinion Answer</DialogTitle>
        <DialogContent>
          <DialogContentText>Please enter your answer here.</DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='answer'
            label='Answer'
            type='text'
            fullWidth
            variant='standard'
            value={AnswerAddedByExpert}
            onChange={(e) => {
              setAnswerAddedByExpert(e.target.value)
              //console.log(AnswerAddedByExpert)
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setAnswerAddedByExpert('')
              setanswerOpen(false)
            }}
          >
            Cancel
          </Button>
          <Button
            onClick={() => {
              addAnswer()
            }}
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar //Will display snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={() => {
          setOpenSnackbar(false)
        }}
      >
        <Alert severity={AlertType} sx={{ width: '100%' }}>
          {AlertMessage}
        </Alert>
      </Snackbar>
    </div>
  )
}
