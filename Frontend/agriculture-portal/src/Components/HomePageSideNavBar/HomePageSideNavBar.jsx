import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import { useNavigate } from "react-router-dom";
import {
  Collapse,
  Container,
  Grid,
  IconButton,
  ListItemButton,
  ListSubheader,
  Paper,
} from "@mui/material";
import StarBorder from "@mui/icons-material/StarBorder";
import { yellow } from "@mui/material/colors";
import MenuIcon from "@mui/icons-material/Menu";
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import { Icon } from "@material-ui/core";
import { findAllByDisplayValue } from "@testing-library/react";
import { breakpoints, display, height } from "@mui/system";

const drawerWidth = "300";

export default function HomePageSideNavBar(props) {
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [open, setOpen] = React.useState();
  const navigate = useNavigate()
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const subCategory = props.subCategory;
  const {userId} = sessionStorage
  const CategoryIcons = props.CategoryIcons;
  const [categoryIcons, setCategoryIcons] = React.useState(CategoryIcons);
  const handleClick = (id) => {
    //let catIcons = [categoryIcons]
    // debugger
    // console.log(categoryIcons);
    categoryIcons.at(id).open = !categoryIcons.at(id).open;
    setCategoryIcons([...categoryIcons]);
  };
  let category = props.category;
  const getSubCategories = (id) => {
    const subcatArray = [...subCategory];
    // debugger
    //   console.log(Array.isArray(subcatArray))
    const subcats = subcatArray.filter((subcat) => subcat.categoryId == id);
    return subcats;
  };
  // console.log(Object.values(category));

  const categoryArray = Object.values(category);
  const drawer = (categoryArray, subCategory) => (
    <Box
      sx={{
        height: "100%",
        width: "100%",
      }}
    >
      
      <List
        sx={{
          overflow: "auto",
          '&::-webkit-scrollbar': {
            width: '0.4em',
            borderRadius:'1px'
          },
          '&::-webkit-scrollbar-track': {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
            webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
            borderRadius:'1px'
          },
          '&::-webkit-scrollbar-thumb': {
            backgroundColor: 'rgb(128,129,129)',
            outline: '1px solid slategrey',
            borderRadius:8
          }
        }}
      >
        {categoryArray.map((item, i) => {
          return (
            <div key={i}>
              <ListItem >
                <ListItemButton onClick={() => handleClick(i)}>
                  <ListItemIcon>
                    {/* <InboxIcon /> */}
                    <span>
                      <Icon>
                        <img width="30px" src={categoryIcons[i].icon} />
                      </Icon>
                    </span>
                  </ListItemIcon>
                  <ListItemText>
                    <Typography variant="h6" fontWeight={500} fontSize={17} textTransform='capitalize'>{item.name}</Typography></ListItemText>
                  {categoryIcons[i].open ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
              </ListItem>

              <Collapse
                sx={{ overflow: "auto" }}
                orientation="vertical"
                in={categoryIcons[i].open}
                timeout="auto"
                unmountOnExit
              >
                <List
                  component="div"
                  disablePadding
                  sx={{ maxHeight: "100%", overflow: "auto" }}
                >
                  {getSubCategories(item.categoryId).map((subItem,i) => {
                    return (
                      <div key={i}>
                        <ListItemButton sx={{ pl: 7 }} onClick={()=>{
                            navigate('/item',{state:{item:subItem, userId:userId}})
                        }}>
                          <ListItemIcon>
                            <StarBorder />
                          </ListItemIcon>
                          <ListItemText>
                               <Typography variant="h6" fontWeight={450} fontSize={15} textTransform='capitalize'>{subItem.name}</Typography></ListItemText>
                        </ListItemButton>
                      </div>
                    );
                  })}
                </List>
              </Collapse>
            </div>
          );
        })}
      </List>
    </Box>
  );

  return (
    <Grid container>
      <Grid item xs={12}>
        <Box height="100%" sx={{}}>
         
          <Drawer
            position="relative"
            // container={container}
            variant="temporary"
            open={props.drawerState}
            onClose={props.toggleDrawer}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
            sx={{
              display: { xs: "block", sm: "block", md: "none" },
              "& .MuiDrawer-paper": {
                boxSizing: "border-box",
                width: drawerWidth,
                position: "inherit",
              },
              "& .MuiPaper-root": {
                position: "fixed",
                top: "auto",
                left: "auto",
              },
            }}
          >
            {drawer(categoryArray, categoryIcons, subCategory)}
          </Drawer>

          <Box position="fixed" sx={{ width: { lg: 300, md: 245 }, ml:{ lg:-3 , md: -2}}}>
            <Paper
              elevation={2}
              sx={{
                display: { xs: "none", sm: "none", md: "block" },
                "& .MuiList-root": {
                  position: "relative",
                  bgcolor: "white",
                  maxHeight: "500px",
                },
                marginRight: 1,
              }}
            >
              {drawer(categoryArray, categoryIcons, subCategory)}
            </Paper>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
}
