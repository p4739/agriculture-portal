import React, { useEffect, useState } from "react";
import { Paper, Typography } from "@mui/material";
import { styled } from "@mui/system";
import { Box } from "@mui/material";

const Cartdetails = styled(Typography)({
  textAlign: "start",
  color: "black",
  fontWeight: "400",
  fontFamily: "revert",
  fontSize: "20px",
  marginTop: "1%",
});

const Basket = (props) => {
  const { Sbtotal, gst, total } = props;

  const [discount, setDiscount] = useState(100);
  const [shipping, setShipping] = useState(70);

  function fetchSubtotal() {}
  //console.log(Sbtotal);
  /* setGST(Sbtotal * 0.18);
  setTotal(Sbtotal + Sbtotal * 0.18 + shipping - discount); */
  /* function findTotal() {
    setTotal(subtotal + gst + shipping - discount);
  } */
  /*  useEffect(() => {
    fetchSubtotal();
  }, []); */
  /*  useEffect(() => {
    fetchSubtotal();
    // findTotal();
  }, []); */

  return (
    <div>
      <Paper
        elevation={6}
        sx={{ width: "100%", minHeight: "250px", height: "100%", mb: "3%" }}
      >
        <Box sx={{ display: "blocks", mr: "5%", ml: "5%" }}>
          <Box display={"flex"}>
            <Cartdetails
              sx={{
                flexGrow: 1,
                color: "#7D879C",
                marginTop: "7%",
                fontSize: "17px",
              }}
            >
              Subtotal :
            </Cartdetails>
            <Cartdetails sx={{ marginTop: "7%" }}>
              &#8377; {Sbtotal}
            </Cartdetails>
          </Box>
          <Box display={"flex"}>
            <Cartdetails
              sx={{ flexGrow: 1, color: "#7D879C", fontSize: "17px" }}
            >
              Shipping :
            </Cartdetails>
            <Cartdetails>&#8377; {shipping}</Cartdetails>
          </Box>
          <Box display={"flex"}>
            <Cartdetails
              sx={{ flexGrow: 1, color: "#7D879C", fontSize: "17px" }}
            >
              GST :
            </Cartdetails>
            <Cartdetails>&#8377;{gst}</Cartdetails>
          </Box>
          <Box display={"flex"}>
            <Cartdetails
              sx={{ flexGrow: 1, color: "#7D879C", fontSize: "17px" }}
            >
              Discount :
            </Cartdetails>
            <Cartdetails>&#8377; {discount}</Cartdetails>
          </Box>
          <hr />
          <Cartdetails sx={{ fontSize: "33px", textAlign: "right" }}>
            &#8377; {total}
          </Cartdetails>
        </Box>
      </Paper>
    </div>
  );
};

export default Basket;
