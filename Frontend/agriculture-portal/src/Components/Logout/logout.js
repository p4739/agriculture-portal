import React from 'react'
import { Button, ListItem, ListItemIcon } from "@mui/material"
import { useStyles } from '../Dashboard/Styles';
import SettingsIcon from '@mui/icons-material/Settings';
import { green } from '@mui/material/colors';
import { Paper, Grid } from '@mui/material';
import { useNavigate } from 'react-router';
import { TextField } from '@mui/material';
import { useState } from 'react';
import axios from 'axios';
import { useEffect } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import styled from 'styled-components';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';

export default function Logout() {

    const navigate = useNavigate();


    const logoutUser = () => {
        // remove the logged users details from session storage
        // sessionStorage.removeItem('userId')
        // sessionStorage.removeItem('firstName')
        // sessionStorage.removeItem('lastName')
        // sessionStorage.removeItem('pincode')
        // sessionStorage.removeItem('state')
        // sessionStorage.removeItem('roleId')
        sessionStorage.removeItem('token')

        // navigate to sign in component
        //window.document.reload();
        navigate('/')
        window.location.reload();
        
    }

    useEffect(() => { logoutUser() }, []);



    return (
        <div>
        </div >
    )

}
