import { Routes, Route } from "react-router-dom";

import HomePageDefault from "../HomePageDefault/HomePageDefault";

import { makeStyles } from "@mui/styles";
import { Stack, Container, Grid, Box, styled } from "@mui/material";
import HomePageIcon from "../HomePageCarousel/index.jsx";
import HomePageSideNavBar from "../HomePageSideNavBar/HomePageSideNavBar";
import { grid } from "@mui/system";
import Footer from "../Footer";
import SubCategory from "../SubCategory";
import Search from "../Search"
import CategoryList from '../CategoryList'
const categoryIcons = [
  {
    id: "1",
    icon: "https://cdn-icons-png.flaticon.com/512/3662/3662282.png",
    open: false,
  },
  {
    id: "2",
    icon: "https://img.icons8.com/color/344/feeding-chicken.png",
    open: false,
  },
  {
    id: "3",
    icon: "https://cdn-icons-png.flaticon.com/512/1625/1625048.png",
    open: false,
  },
  {
    id: "4",
    icon: "https://img.icons8.com/external-photo3ideastudio-lineal-color-photo3ideastudio/344/external-beans-cholesterol-photo3ideastudio-lineal-color-photo3ideastudio.png",
    open: false,
  },
  {
    id: "5",
    icon: "https://cdn-icons-png.flaticon.com/512/2329/2329865.png",
    open: false,
  },
  {
    id: "6",
    icon: "https://cdn-icons-png.flaticon.com/512/465/465807.png",
    open: false,
  },

  {
    id: "7",
    icon: "https://img.icons8.com/external-justicon-lineal-color-justicon/344/external-red-beans-healthy-food-and-vegan-justicon-lineal-color-justicon.png",
    open: false,
  },
  {
    id: "8",
    icon: "https://img.icons8.com/external-icongeek26-linear-colour-icongeek26/344/external-plant-ecology-icongeek26-linear-colour-icongeek26.png",
    open: false,
  },
  {
    id: "9",
    icon: "https://img.icons8.com/color/344/flax-seeds.png",
    open: false,
  },
  {
    id: "10",
    icon: "https://img.icons8.com/fluency/344/tractor.png",
    open: false,
  },
];

function HomePageLayout({ subCategory, category,drawerState,toggleDrawer }) {
  //console.log(subCategory);
  // xs={marginTop: "35%"} sm={marginTop: "12%"}
  return (
    <Container maxWidth={false} sx={{ maxWidth: "1280px" }}>

    <Box
      // sx={(theme) => ({
      //   [theme.breakpoints.down("sm")]: {
      //     marginTop: "35%",
      //   },
      //   [theme.breakpoints.down("md")]: {
      //     marginTop: "25%",
      //   },
      //   [theme.breakpoints.down("lg")]: {
      //     marginTop: "16%",
      //   },
      //   [theme.breakpoints.up("lg")]: {
      //     marginTop: "13%",
      //   },
      // })}
    >
      {/* <Stack  direction='row' spacing={2}> */}
      <Grid container columns={12}>
        <Grid item xs={0} sm={0} md={3}>
          <HomePageSideNavBar
            subCategory={subCategory}
            category={category}
            CategoryIcons={categoryIcons}
            drawerState={drawerState}
            toggleDrawer={toggleDrawer}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={9}>
          <Routes>
            <Route
              path="/*"
              element={
                <HomePageDefault
                  subCategory={subCategory}
                  category={category}
                  CategoryIcons={categoryIcons}
                  
                />
              }
            />
            <Route path="/search" element={<Search />} />
             <Route path="/categorylist" element={<CategoryList />} />
             {/* <Route path = "/item" element = {<Item/>}/>
             <Route path = "/categorylist" elemeny = {<CategoryList/>}/>  */}
          </Routes>
         
        </Grid>
      </Grid>

     {/* </Stack>  */}
    </Box>
    </Container>
  );
}

export default HomePageLayout;
