import { Grid, Stack, Typography } from '@mui/material'
import InfoItem from '../FarmSchoolSubTopicInfoItem'
import Carousel from 'react-material-ui-carousel'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
export default function Index(props) {
  //const {id} = props
  const { state } = useLocation()
  const id = state.id
  const [subTopic, setsubTopic] = useState()
  // const subTopic = {
  //     sub_category: 'Wheat',
  //     image:
  //       [
  //         "https://thumbs.dreamstime.com/z/wheat-varieties-detail-three-35224705.jpg",
  //         "https://thumbs.dreamstime.com/z/wheat-varieties-detail-three-35224705.jpg",
  //         "https://thumbs.dreamstime.com/z/wheat-varieties-detail-three-35224705.jpg",
  //       ],
  //     season: 'Dry season with assured irrigation is more suitable',
  //     irrigation : 'Irrigation only to moist the soil in the early period of 10 days. Restoring irrigation to a maximum depth of 2.5cm after development of hairline cracks in the soil until panicle initiation.Increasing irrigation depth to 5.0cm after PI one day after disappearance of ponded water',
  //     diseases : 'Sheath spot',
  //     weed_Management: 'Using rotary weeder / Cono weeder / power operated two row weeder',
  //     ferti_pesti : 'Lambda-cyhalothrin, malathion and zeta-cypermethrin',
  // }
  useEffect(() => {
    fetchSubTopicDetails(id)
  }, [])

  function fetchSubTopicDetails(id) {
    const body = {
      subCategoryId: id,
    }
    const URL = 'http://localhost:4000'
    let fetchSubTopicurl = URL + '/farmschool/fetch-crop-detail'
    axios.post(fetchSubTopicurl, body).then((response) => {
      const result = response.data
      if (result['status'] === 'success') {
        setsubTopic(result['data'][0])
      }
    })
  }
  console.log(subTopic)
  return (
    <div>
      {subTopic == null ? (
        <></>
      ) : (
        <>
          <Typography
            sx={{ textAlign: 'center' }}
            variant='h2'
            fontWeight={400}
            textTransform='capitalize'
            mt={5}
            mb={4}
          >
            {subTopic.name}
          </Typography>
          <Grid container mb={5}>
            <Grid item md={9} xs={12} sm={12}>
              <Stack direction={'column'} spacing={2}>
                <InfoItem name='Season' info={subTopic.seasons}></InfoItem>
                <InfoItem
                  name='Irrigation'
                  info={subTopic.irrigation}
                ></InfoItem>
                <InfoItem name='Diseases' info={subTopic.diseases}></InfoItem>
                <InfoItem
                  name='Weed Management'
                  info={subTopic.weedManagement}
                ></InfoItem>
                <InfoItem
                  name='Fertilizers and Pesticides'
                  info={subTopic.fertiliezersAndPesticides}
                ></InfoItem>
              </Stack>
            </Grid>
            <Grid item md={3} xs={12} sm={12}>
              <Carousel sx={{ width: 300 }}>
                {subTopic.imageId == null ? (
                  <></>
                ) : (
                  subTopic.imageId.map((img, i) => {
                    return (
                      <img width={400} height={300} id={i} src={img.link}></img>
                    )
                  })
                )}
              </Carousel>
            </Grid>
          </Grid>
        </>
      )}
    </div>
  )
}
