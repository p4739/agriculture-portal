import { Box, Grid, Stack, Typography } from '@mui/material'
import InfoItem from '../FarmSchoolSubTopicInfoItem'
import Carousel from 'react-material-ui-carousel'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
export default function Index(props) {
  //const {variety_id} = props
  const { state } = useLocation()

  console.log(state)
  const id = state.id
  const [subTopic, setsubTopic] = useState()
  useEffect(() => {
    fetchSubTopicDetails(id)
  }, [])

  function fetchSubTopicDetails(id) {
    const body = {
      varietyId: id,
    }
    const URL = 'http://localhost:4000'
    let fetchSubTopicurl = URL + '/farmschool/fetch-machinery-detail'
    axios.post(fetchSubTopicurl, body).then((response) => {
      const result = response.data
      if (result['status'] === 'success') {
        setsubTopic(result['data'][0])
      }
    })
  }
  console.log(subTopic)
  // const subTopic = {
  //   sub_category: "Tractor",
  //   image:
  //     [
  //       "https://thumbs.dreamstime.com/z/wheat-varieties-detail-three-35224705.jpg",
  //       "https://thumbs.dreamstime.com/z/wheat-varieties-detail-three-35224705.jpg",
  //       "https://thumbs.dreamstime.com/z/wheat-varieties-detail-three-35224705.jpg",
  //     ],
  //   function: "Dry season with assured irrigation is more suitable",
  //   sp_type: "Irrigation only to moist the soil in the early period of 10 days",
  //   sp_power_requirement: "Sheath spot",
  //   sp_weight: "100 ",
  //   sp_capacity: "100 ",
  //   feature:
  //     "Using rotary weeder / Cono weeder / power operated two row weeder",
  //   info: "Compact tractors, as their name implies, are small, high-powered tractors that can assist with all the basic functions needed on a farm. ",
  // xs:12,sm:12,md:6,lg:6};
  return (
    <Box mb={5}>
      {subTopic == null ? (
        <></>
      ) : (
        <>
          <Typography
            sx={{ textAlign: 'center' }}
            variant='h2'
            fontWeight={400}
            textTransform='capitalize'
            mt={5}
            mb={4}
          >
            {subTopic.name}
          </Typography>
          <Grid container mb={5}>
            <Grid item md={9} xs={12} sm={12}>
              <Stack direction={'column'} spacing={2}>
                <InfoItem name='Function' info={subTopic.functions}></InfoItem>
                <InfoItem name='Feature' info={subTopic.feature}></InfoItem>
                <InfoItem name='Info' info={subTopic.info}></InfoItem>
                <Typography
                  sx={{ textAlign: 'center' }}
                  fontWeight={500}
                  fontSize={25}
                >
                  Specifications
                </Typography>
                <hr />
                <InfoItem name='Type' info={subTopic.specType}></InfoItem>
                <InfoItem
                  name='Power Requirements'
                  info={subTopic.specPower}
                ></InfoItem>
                <InfoItem
                  name='Weight'
                  info={subTopic.specWeight + ' Kg'}
                ></InfoItem>
                <InfoItem
                  name='Capacity'
                  info={subTopic.specCapacity + ' HP'}
                ></InfoItem>
              </Stack>
            </Grid>
            <Grid item md={3} xs={12} sm={12}>
              <Carousel sx={{ width: 300 }}>
                {subTopic.imageId == null ? (
                  <></>
                ) : (
                  subTopic.imageId.map((img, i) => {
                    return (
                      <img width={400} height={300} id={i} src={img.link}></img>
                    )
                  })
                )}
              </Carousel>
            </Grid>
          </Grid>
        </>
      )}
    </Box>
  )
}
