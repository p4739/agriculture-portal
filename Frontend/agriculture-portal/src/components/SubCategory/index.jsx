import Card from '@mui/material/Card'
import React, { forwardRef, useContext, useEffect, useState } from 'react'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import {
  Alert,
  Box,
  CardActionArea,
  IconButton,
  Snackbar,
  Stack,
} from '@mui/material'
import Typography from '@mui/material/Typography'
import AddIcon from '@mui/icons-material/Add'
import RemoveIcon from '@mui/icons-material/Remove'
import CurrencyRupeeIcon from '@mui/icons-material/CurrencyRupee'
import { useNavigate } from 'react-router-dom'
import jwt_decode from 'jwt-decode'
import axios from 'axios'
import MuiAlert from '@mui/material/Alert'
import { toast } from 'react-toastify'
import { CartQuantityFlagContext } from '../../Pages/HomePage'

function SubCategory({ item }) {
  //console.log(item)

  useEffect(() => {
    authorizeUser()
    console.log(userId)
    fetchCartQuantity()
    hasVariety()
    // console.log(UserId)
    // console.log(item)
  }, [])
  const [IsUserAuthorized, setIsUserAuthorized] = useState(false)
  var [userId, setUserId] = useState()
  const [role, setRole] = useState()

  const [alertContent, setAlertContent] = useState()
  const [openSnackBar, setOpenSnackBar] = useState(false)
  const [hasVarities, setHasVarities] = useState(false)
  const [cartQuantity, setcartQuantity] = useState(0) //default is set to true to add item to cart
  const flagContext = useContext(CartQuantityFlagContext)
  const { cartQuantityUpdateFlag, setCartQuantityUpdateFlag } = flagContext
  const navigate = useNavigate()
  //write based on jwt auth

  // function FetchUserDetails() {

  // }

  const fetchCartQuantity = async () => {
    let URL = 'http://localhost:4000'
    let urlfetchCartQuantity = URL + '/item/fetch-item-count'
    var body = null
    console.log(userId)

    if (item != null) {
      body = {
        subCategoryId: item.subCategoryId,
        userId: userId,
      }
    }

    //console.log(body)
    axios.post(urlfetchCartQuantity, body).then((response) => {
      const result = response.data
      //console.log(result)
      if (result.status == 'success') {
        if (result.quantity == null) setcartQuantity(0)
        else setcartQuantity(result.quantity)
      }
    })
  }
  const authorizeUser = () => {
    const Token = sessionStorage.getItem('token')
    if (Token == null) {
      setUserId(null)
    } else {
      const DecodedToken = jwt_decode(Token)

      setUserId(DecodedToken.data.userId)
      userId = DecodedToken.data.userId
    }
  }

  const hasVariety = () => {
    if (item != null) {
      if (item.hasVariety == 0) return setHasVarities(false)
      else return setHasVarities(true)
    }
  }
  const AddToCart = () => {
    if (userId != null) {
      if (!hasVarities) {
        var body = null

        body = {
          subCategoryId: item.subCategoryId,
          userId: userId,
        }
        let URL = 'http://localhost:4000'
        let urlAddtoCart = URL + '/item/add-item-to-cart'
        axios.post(urlAddtoCart, body).then((response) => {
          const result = response.data
          if (result.status === 'success') {
            setcartQuantity(cartQuantity + 1)
            setCartQuantityUpdateFlag(!cartQuantityUpdateFlag)
          } else {
            toast.warn(result.result, {
              position: toast.POSITION.BOTTOM_RIGHT,
            })
          }
        })
      } else {
        navigate('/item', { state: { item: item } })
      }
    } else {
      setOpenSnackBar(true)
      setAlertContent('Please Login... :)')

      navigate('/signin')
    }
  }
  const RemoveFromCart = () => {
    var body = null

    body = {
      subCategoryId: item.subCategoryId,
      userId: userId,
    }
    let URL = 'http://localhost:4000'
    let urlAddtoCart = URL + '/item/del-item-from-cart'
    axios.post(urlAddtoCart, body).then((response) => {
      const result = response.data
      if (result.status === 'success') {
        setcartQuantity(cartQuantity - 1)
        setCartQuantityUpdateFlag(!cartQuantityUpdateFlag)
      }
    })
  }
  // console.log(item.name)

  //onclick image -  write forward url in below fn
  const detailsPage = (id, userId) => {}
  const Alert = forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant='filled' {...props} />
  })
  return (
    <>
      {item != null ? (
        <>
          <Card
            elevation={2}
            sx={{
              width: [190, 210, 230, 250],
              height: [290, 300, 310, 330],
              margin: 2,
            }}
          >
            <Stack direction='column'>
              <CardActionArea
                onClick={() => {
                  navigate('/item', { state: { item: item } })
                }}
              >
                <Box>
                  <CardMedia
                    sx={{
                      height: [170, 180, 200, 220],
                      aspectRatio: 1,
                    }}
                    component='img'
                    image={item.imageId[0].link}
                    alt={item.name}
                    onClick={detailsPage(item.subCategoryId, userId)}
                  />
                </Box>
              </CardActionArea>
              <CardContent sx={{ maxHeight: 100, paddingTop: 1 }}>
                <Stack direction='column' spacing={0.5}>
                  <Stack direction='row'>
                    <Typography
                      variant='subtitle1'
                      color='#606674'
                      sx={{ textTransform: 'capitalize' }}
                    >
                      {item.name}
                    </Typography>
                    <Typography flexGrow={1} />
                    <IconButton
                      aria-label='remove from cart'
                      onClick={RemoveFromCart}
                      sx={{
                        display: cartQuantity === 0 ? 'none' : 'flex',
                        padding: 0,
                      }}
                    >
                      <RemoveIcon
                        fontSize='medium'
                        color='success'
                        sx={{ border: 2, borderRadius: 1 }}
                      />
                    </IconButton>
                  </Stack>
                  <Stack direction='row'>
                    <Typography
                      variant='subtitle1'
                      color='#858FA2'
                      sx={{ textTransform: 'capitalize' }}
                    >
                      {item.unit}
                    </Typography>
                    <Typography flexGrow={1} />
                    <Typography
                      variant='body1'
                      hidden={cartQuantity == 0 ? true : false}
                    >
                      {cartQuantity}
                    </Typography>
                  </Stack>
                  <Stack direction='row'>
                    <CurrencyRupeeIcon
                      color='success'
                      fontSize='small'
                      sx={{ mt: 0.4, marginLeft: '-3px' }}
                    ></CurrencyRupeeIcon>
                    <Typography variant='subtitle1' color='success'>
                      {item.price}
                    </Typography>
                    <Typography flexGrow={1} />
                    <IconButton
                      aria-label='add to cart'
                      onClick={AddToCart}
                      sx={{ padding: 0 }}
                    >
                      <AddIcon
                        fontSize='medium'
                        color='success'
                        sx={{ border: 2, borderRadius: 1 }}
                      />
                    </IconButton>
                  </Stack>
                </Stack>
              </CardContent>
            </Stack>
          </Card>
        </>
      ) : (
        'loading...'
      )}
    </>
  )
}
export default SubCategory
