import { makeStyles } from "@mui/styles";
//import Carousel from "react-material-ui-carousel";
import { Paper, Button, Card, CardMedia, CardActionArea, Box, Grid } from "@mui/material";
import { useNavigate } from "react-router-dom"
import Carousel from "react-multi-carousel";

 

function Item(props) {

const subCategory = props.subCategory
  const getSubCategories = ((cat)=>{
    return subCategory.filter((sbcat)=>sbcat.categoryId===cat.categoryId)
})
  const navigate = useNavigate()
  return (
    <div>
      <Card elevation={3} sx={{maxHeight:{sm:'350px',md:'400px', lg:'450px'}}}>
        <CardActionArea 
        onClick={()=>{
           navigate('/categorylist',{state:{categoryId:props.item.categoryId,subCategory:getSubCategories(props.item)}})
        }}>
        <CardMedia
          sx={{
          //  height:{xl:'450px',xs:'200px',sm:'300px',md:'400px'},
          height:'100%',
          aspectRatio:'2.2',
          backgroundColor:'red'//Image size for sub cat comp can be changed here
        }}
          component="img"
          image={props.item.image}
        ></CardMedia>
        
        
        </CardActionArea> 
      </Card>
    </div>
  );
}

function HomePageCarousel({subCategory}) {
  
  var items = [
    {
      name: "Vegess",
      description: "Veggies...!",
      categoryId: 3,
      image:
        "https://images.unsplash.com/photo-1610348725531-843dff563e2c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
    },
    
    {
      name: "Vegetables",
      description: "Veggies...!",
      categoryId: 5,
      image:
        "https://images.unsplash.com/photo-1464226184884-fa280b87c399?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80",
    },
    {
      name: "Cerels",
      description: "Cerels",
      categoryId: 1,
      image:
        "http://daniel-trading.com/upload/images/id_74/wholegrains_shutterstock_Nov-2014_E.jpg",
    },
    
  ];

 
  return (
    <Grid container >
    <Box
      sx={{
        maxWidth: "100%",
        width: "100%",
         }}
    >
      {/* <Carousel indicators={false} cycleNavigation={true}>
        {items.map((item, i) => (
          <Grid item xs={12} key={i}>
          <Item  item={item} categoryId={item.categoryId} />
          </Grid>
        ))}
      </Carousel> */}

<Carousel
  additionalTransfrom={0}
  arrows
  autoPlay
  autoPlaySpeed={3600}
  centerMode={false}
  className=""
  customTransition="transform 1200ms ease-in-out"
  
  containerClass="container-with-dots"
  dotListClass=""
  draggable
  focusOnSelect={false}
  infinite = {true}
  itemClass=""
  keyBoardControl
  minimumTouchDrag={80}
  renderButtonGroupOutside={false}
  renderDotsOutside={false}
  responsive={{
    desktop: {
      breakpoint: {
        max: 3000,
        min: 1024
      },
      items: 1,
      partialVisibilityGutter: 40
    },
    mobile: {
      breakpoint: {
        max: 464,
        min: 0
      },
      items: 1,
      partialVisibilityGutter: 30
    },
    tablet: {
      breakpoint: {
        max: 1024,
        min: 464
      },
      items: 1,
      partialVisibilityGutter: 30
    }
  }}
  showDots={false}
  sliderClass=""
  slidesToSlide={1}
  swipeable
 
>
{items.map((item, i) => (
          <Grid item xs={12} key={i}>
          <Item  item={item} categoryId={item.categoryId} subCategory={subCategory}/>
          </Grid>
        ))}

</Carousel>
    </Box>
    </Grid>
  );
}

export default HomePageCarousel;
