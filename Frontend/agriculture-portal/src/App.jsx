import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import {  Routes, Route, HashRouter } from "react-router-dom";
import HomePage from "./Pages/HomePage";
 import SignUp from "./Pages/Signup";
import Signin from "./Pages/Signin";
import DashBoard from "./Components/Dashboard/Dashboard";

import UtilPage from "./Pages/Utils";
import { Container } from "@mui/material";
import { makeStyles } from "@mui/styles";
import "./App.css";
import FarmSchoolMachinerySubTopic from "./Components/FarmSchoolMachinerySubTopic"
import FarmSchoolAgricultureSubTopic from "./Components/FarmSchoolAgricultureSubTopic"
import FarmSchoolSubType from "./Components/FarmSchoolSubType"
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Logout from '../src/Components/Logout/logout'


function App() {
  return (
    <>
      <HashRouter>
       
          <Routes>
        
           {/* <Route path="/FarmSchoolHome/FarmSchoolSubType" element={<FarmSchoolSubType />} /> 
           <Route path="/FarmSchoolHome/FarmSchoolSubType/FarmSchoolMachinerySubVariety/FarmSchoolMachinerySubTopic" element={<FarmSchoolMachinerySubTopic/>} />
          <Route path="/FarmSchoolHome/FarmSchoolSubType/FarmSchoolAgricultureSubTopic" element={<FarmSchoolAgricultureSubTopic/>} /> */}
            <Route path="/*" element={<HomePage />} /> 
              <Route path="/signin" element={<Signin />} />
            <Route path="/signup" element={<SignUp />} />
            <Route path='/logout' element={<Logout/>} />
            <Route path="/utilpage/*" element={<UtilPage />} /> 
          </Routes>
          <ToastContainer/>
        
      </HashRouter>
    </>
  );
}
export default App;