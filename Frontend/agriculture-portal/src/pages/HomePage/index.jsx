import EcommHeader from '../../Components/EcommHeader'
import { Routes, Route } from 'react-router-dom'
import HomePageLayout from '../../Components/HomePageLayout/HomePageLayout'
import axios from 'axios'
import React, { createContext, useEffect, useState } from 'react'
import DashBoard from '../../Components/Dashboard/Dashboard'
import SubCategory from '../../Components/SubCategory'
import { makeStyles } from '@mui/styles'
import { Box } from '@mui/material'
import Product from '../../Components/Products/Product'
import Orders from '../../Components/Orders/Orders'
import AddProduct from '../../Components/AddProduct/AddProduct'
import EditProfile from '../../Components/EditProfile/EditProfile'
import OrderDetails from '../../Components/OrderDetails/OrderDetails'
import BlankPage from '../../Components/OrderDetails/BlankPage'
import ProductBlankPage from '../../Components/EditProduct/ProductBlankPage'
import Cart from '../../Components/Cart'
import Item from '../../Components/Item'
import jwt_decode from 'jwt-decode'
// import { Container } from "@mui/material";
//import Footer from "../../components/Footer"
export const CartQuantityFlagContext = createContext()
function HomePage() {
  const [category, setCategory] = useState([])
  const [subCategory, setsubCategory] = useState([])
  const [drawerState, setDrawerState] = useState(false)
  let [userId, setUserId] = useState()

  //for cart count update--> vedang
  const [cartQuantityUpdateFlag, setCartQuantityUpdateFlag] = useState(false)
  const flagContext = {
    cartQuantityUpdateFlag,
    setCartQuantityUpdateFlag,
  }
  // const { uid } = sessionStorage;
  // const UserId = 1
  useEffect(async () => {
    FetchUserDetails()

    fecthCategorySubCategoryList().catch((err) => {
      throw err
    })
    console.log(userId)
  }, [])

  function FetchUserDetails() {
    const Token = sessionStorage.getItem('token')
    if (Token == null) {
      setUserId(null)
    } else {
      const DecodedToken = jwt_decode(Token)

      setUserId(DecodedToken.data.userId)
      userId = DecodedToken.data.userId
      console.log(DecodedToken.data.userId)
      console.log(userId)
    }
  }
  async function fecthCategorySubCategoryList() {
    let URL = 'http://localhost:4000'
    let urlCategory = URL + '/item/fetch-category-list'
    axios.get(urlCategory).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        // console.log(result['data'])
        setCategory(result['data'])
      }
    })

    let urlSubCategory = URL + '/item/fetch-subCategory-list'
    let body = {
      userId: userId,
    }
    axios.post(urlSubCategory, body).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        // console.log(result['data'])
        setsubCategory(result['data'])
      }
    })
    // console.log(category)
    // console.log(subCategory)
  }
  function toggleDrawer() {
    setDrawerState(!drawerState)
    // console.log(drawerState)
  }

  return (
    <>
      <Box
        sx={(theme) => ({
          [theme.breakpoints.down('sm')]: {
            marginTop: '35%',
          },
          [theme.breakpoints.down('md')]: {
            marginTop: '25%',
          },
          [theme.breakpoints.down('lg')]: {
            marginTop: '16%',
          },
          [theme.breakpoints.up('lg')]: {
            marginTop: '12%',
          },
        })}
      >
        <CartQuantityFlagContext.Provider value={flagContext}>
          <EcommHeader subCategory={subCategory} toggleDrawer={toggleDrawer} />
          {/* <Container maxWidth={false} sx={{ maxWidth: "1280px" }}> */}
          {/* <HomePageLayout subCategory={subCategory}/> */}
          <Routes>
            <Route
              path='/*'
              element={
                <HomePageLayout
                  category={category}
                  subCategory={subCategory}
                  drawerState={drawerState}
                  toggleDrawer={toggleDrawer}
                  userId={userId}
                />
              }
            />
            <Route path='/dashboard/*' element={<DashBoard />} />
            <Route path='/cart/*' element={<Cart />} />
            {/* <Route path='/dashboard/product-list/*' element={<ProductBlankPage />} />
              <Route path='/dashboard/add-product' element={<AddProduct />} />
              <Route path='/dashboard/orders' element={<Orders />} />
              <Route path='/dashboard/orders/*' element={<BlankPage />} />
              <Route path='/dashboard/edit-profile' element={<EditProfile />} /> */}
            <Route path='/item' element={<Item />} />
          </Routes>
        </CartQuantityFlagContext.Provider>
        {/* </Container>
         */}
      </Box>
    </>
  )
}
export default HomePage
