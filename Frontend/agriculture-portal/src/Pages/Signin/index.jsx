import React from 'react'
import { Grid, Paper, Avatar, TextField, Button } from '@mui/material'
import LockIcon from '@mui/icons-material/Lock';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify'
import Container from '@mui/material/Container'
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight'
import axios from 'axios'
import jwt_decode from 'jwt-decode'
import { makeStyles } from '@mui/styles';

export default function Index() {

    const paperStyle = {
        // styling for paper or signin component
        padding: 20,
        height: '60%',
        width: 350,
        margin: "70px auto", //20 px from top and side auto
        borderRadius: '10px',
    }

    const avatarStyle = {
        // styling for lock logo
        backgroundColor: 'rgb(46, 125, 50)'
    }

    const welocmeField = {
        display: 'block',
        fontSize: '1.17em',
        marginBlockStart: '1em',
        marginBlockEnd: '1em',
        marginInlineStart: '0px',
        marginInlineEnd: '0px',
        fontWeight: ' bold',
        fontFamily: ' Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif'
    }

    const loginWithEmailField = {
        fontSize: '12px',
        lineHeight: 1.5,
        fontWeight: 600,
        color: '#373F50',
        textAlign: 'center',
        marginBottom: '36px',
        display: 'block',
        whiteSpace: 'normal',
        fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif'
    }

    const signUpFont = {
        fontSize: '13px',
        fontWeight: 600,
        lineHeight: '1.5',
        lineWeight: 600,
        color: 'rgb(43, 52, 69)',
        display: 'block',
        margin: "20px auto",
        whiteSpace: 'normal',
        fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif'
    }

    const useStyles = makeStyles({
        field: {
            marginTop: 20,
            marginBottom: 20,
            display: 'block'
        }
    })

    const classes = useStyles()
    const [emailError, setEmailError] = useState(false)
    const [passwordError, setPasswordError] = useState(false)


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleBlankSubmmit = (e) => {
        e.preventDefault()
        setEmailError(false)
        setPasswordError(false)

        if (email === '') {
            setEmailError(true)
        }

        if (password === '') {
            setPasswordError(true)
        }

        if (email && password) {
            signinUser();
        }

    }


    const navigate = useNavigate();


    const signinUser = () => {

        const body = { email, password }
        console.log(body);
        // url to make signin api call
        const url = `http://localhost:4000/user/signin`

        // make api call using axios
        axios.post(url, body).then((response) => {

            // get the access jwt token from result        
               
            const accessToken = response.data.accessToken;

            if (accessToken === undefined) {
                toast.error("Invalid email and password");
            }
            else {
                sessionStorage['token'] = accessToken;

                const decodedToken = jwt_decode(accessToken);
                
                if (decodedToken.status === 'success') {
                    
                    const result = decodedToken.data;
                    //console.log(result.firstName);
                    // sessionStorage['userId'] = result.userId
                    // sessionStorage['firstName'] = result.firstName
                    // sessionStorage['lastName'] = result.lastName
                    // sessionStorage['pincode'] = result.pincodeId
                    // sessionStorage['state'] = result.state
                    // sessionStorage['roleId'] = result.roleId
                    
                    toast.success(`Welcome ${result.firstName}`)
                    // navigate to home component
                   // navigate('/dashboard/product-list')
                   
                   navigate('/')
                   window.location.reload();
                } else {
                    toast.error('Invalid email or password')
                }
            }
        })
        // window.location.reload();
    }

    return (


        <div>
            <Grid>
                <Paper elevation={5} style={paperStyle}>
                    <Grid align='center'>
                        <Avatar style={avatarStyle}> <LockIcon ></LockIcon> </Avatar>
                        <h2 style={welocmeField}> Welcome to Agri-Portal</h2>
                        <h4 style={loginWithEmailField}> Login with email & password</h4>
                    </Grid>

                    <div className="col">
                        <div className="mb-3">

                            <Grid align='center'>
                                <Container size="sm">
                                    <form noValidate autoComplete='off' onSubmit={handleBlankSubmmit}>

                                        <div>
                                            <TextField className={classes.field} style={{ width: '270px' }} required id="outlined-required" label="email"
                                                fullWidth error={emailError}
                                                onChange={(e) => { setEmail(e.target.value) }} />
                                        </div>

                                        <div>
                                            <TextField type="password" autoComplete="current-password" style={{ width: '270px' }} required id="outlined-required" label="Password"
                                                error={passwordError} onChange={(e) => { setPassword(e.target.value) }} />
                                        </div>

                                        <br />

                                        <Button type="submit" endIcon={<KeyboardArrowRightIcon />} style={{ width: '270px', backgroundColor: 'rgb(50, 150, 70)', color: 'rgb(255, 255, 255)' }}
                                        > Login </Button>
                                    </form>
                                </Container>
                                <div>
                                    <h1 style={signUpFont}>Don’t have account? <Link to="/signup"
                                        style={{ color: 'black', borderTopColor: 'rgb(43, 52, 69)' }}> Sign Up </Link>  </h1>

                                    {/* <h1 style={signUpFont}>Forgot your password? <Link to="/reset"
                                        style={{ color: 'black', borderTopColor: 'rgb(43, 52, 69)' }}> Reset it </Link>  </h1> */}
                                </div>
                            </Grid>

                        </div>
                    </div>
                </Paper>
            </Grid>

        </div >
    );
}
