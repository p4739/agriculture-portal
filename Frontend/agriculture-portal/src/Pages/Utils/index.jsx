import { Route, Routes } from 'react-router-dom'
import FarmSchoolHome from '../../Components/FarmSchoolHome'
import FarmSchoolSubType from '../../Components/FarmSchoolSubType'
import FarmSchoolMachinerySubVariety from '../../Components/FarmSchoolMachinerySubVariety'
import ExpertsOpinionHome from '../../Components/ExpertOpinionHome'
import ExpertsOpinionDetail from '../../Components/ExpertOpinionDetail'
import UtilHeader from '../../Components/UtilHeader'
import Footer from '../../Components/Footer'
import Container from '@mui/material/Container'
import { Box } from '@mui/system'
import FarmSchoolAgricultureSubTopic from '../../Components/FarmSchoolAgricultureSubTopic'
import FarmSchoolMachinerySubTopic from '../../Components/FarmSchoolMachinerySubTopic'
const UtilPage = () => {
  return (
    <div>
      <Box
        bgcolor='#f6f9fc'
        sx={(theme) => ({
          [theme.breakpoints.down('sm')]: {
            marginTop: '9%',
          },
          [theme.breakpoints.down('md')]: {
            marginTop: '12%',
          },
          [theme.breakpoints.down('lg')]: {
            marginTop: '10%',
          },
          [theme.breakpoints.up('lg')]: {
            marginTop: '5.7%',
          },
        })}
      >
        <Container
          maxWidth={false}
          sx={{
            maxWidth: '1280px',
            //minWidth: "1280px",// creating issue in my laptop regarding responsiveness
            justifyContent: 'center',
            paddingTop: '1%',
            paddingBottom: '1%',
            marginLeft: 'auto',
            marginRight: 'auto',
          }}
        >
          <UtilHeader />
          {/* provided routing for components */}
          <Routes>
            <Route path='/FarmSchoolHome' element={<FarmSchoolHome />} />
            <Route
              path='/FarmSchoolHome/FarmSchoolSubType'
              element={<FarmSchoolSubType />}
            />
            <Route
              path='/FarmSchoolHome/FarmSchoolSubType/FarmSchoolMachinerySubVariety'
              element={<FarmSchoolMachinerySubVariety />}
            />
            <Route
              path='/FarmSchoolHome/FarmSchoolSubType/FarmSchoolMachinerySubVariety/FarmSchoolMachinerySubTopic'
              element={<FarmSchoolMachinerySubTopic />}
            />
            <Route
              path='/FarmSchoolHome/FarmSchoolSubType/FarmSchoolAgricultureSubTopic'
              element={<FarmSchoolAgricultureSubTopic />}
            />
            {/* <Route path="/ExpertsOpinion" element={<ExpertsOpinionHome />} /> */}
            <Route
              path='/ExpertsOpinionHome'
              element={<ExpertsOpinionHome />}
            />
            <Route
              path='/ExpertsOpinionHome/ExpertOpinionQuestionDetail'
              element={<ExpertsOpinionDetail />}
            />
          </Routes>

          <Footer />

          <Box
            sx={(theme) => ({
              [theme.breakpoints.down('md')]: {
                marginBottom: 7,
              },
              [theme.breakpoints.up('md')]: {
                marginBottom: 0,
              },
            })}
          ></Box>
        </Container>
      </Box>
    </div>
  )
}

export default UtilPage
