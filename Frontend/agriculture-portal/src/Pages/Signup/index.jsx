import { Paper, Grid, TextField, Button } from '@mui/material'

import React from 'react'
import { useState } from 'react'
import FormControl from '@mui/material/FormControl';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import FormHelperText from '@mui/material/FormHelperText';
import { Link } from 'react-router-dom';
import { useStyles } from '../../Components/Dashboard/Styles';
import axios from 'axios'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router'
import 'react-toastify/dist/ReactToastify.css';
import { makeStyles } from '@mui/styles'


export default function Index() {

    const classes = useStyles();
    const welocmeField = {
        display: 'block',
        fontSize: '1.17em',
        marginBlockStart: '1em',
        marginBlockEnd: '1em',
        marginInlineStart: '0px',
        marginInlineEnd: '0px',
        fontWeight: ' bold',
        fontFamily: ' Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif'
    }

    const fillDetailMessage = {
        fontSize: '12px',
        lineHeight: 1.5,
        fontWeight: 600,
        color: '#373F50',
        textAlign: 'center',
        marginBottom: '36px',
        display: 'block',
        whiteSpace: 'normal',
        fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif'
    }
    const signUpFont = {
        fontSize: '13px',
        fontWeight: 600,
        lineHeight: '1.5',
        lineWeight: 600,
        color: 'rgb(43, 52, 69)',
        display: 'block',
        margin: "20px auto",
        whiteSpace: 'normal',
        fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif'
    }

    // 1.for customer 2.Farmer 3. expert
    // const roles = ['customer', 'farmer', 'expert'];

    const [firstNameError, setFirstNameError] = useState(false);
    const [lastNameError, setLastNameError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [roleError, setRoleError] = useState(false);
    const [pincodeError, setPincodeError] = useState(false);


    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState(0);
    const [pincode, setPincode] = useState(0);

    const handleBlankSubmmit = (e) => {
        e.preventDefault()
        setEmailError(false)
        setPasswordError(false)

        if (email === '') {
            setEmailError(true)
        }
        if (password === '') {
            setPasswordError(true)
        }
        if (firstName === '') {
            setFirstNameError(true)
        }
        if (lastName === '') {
            setLastNameError(true)
        }
        if (role === 0) {
            setRoleError(true)
        }
        if (pincode === 0) {
            setPincodeError(true)
        }
    }

    const navigate = useNavigate();

    const signupUser = () => {
        const body = { firstName, lastName, email, password, pincode, role }

        console.log(body);

        const url = 'http://localhost:4000/user/signup'

        // http method: post
        // body: contains the data to be sent to the API
        axios.post(url, body).then((response) => {
            // get the data from the response
            const result = response.data
            console.log(result)
            if (result['status'] === 'success') {
                toast.success('Successfully signed up new user')
                // navigate to the signin page
                navigate('/signin')
            } else {
                toast.error(result['error'])
            }
        })
    }


    return (
        <div>

            <Grid>
                <Paper elevation={5} 
                // className={classes.signupPaper}
                sx={(theme) => ({
                    padding: 5,
                    height: '10%',
                    width: '30%',
                    margin: "70px auto", //20 px from top and side auto
                    borderRadius: '10px',
                    [theme.breakpoints.down("sm")]: {
                      height: '50%',
                      width: '90%',
                    },
                })}
                
                >

                    <form noValidate autoComplete='off' onSubmit={handleBlankSubmmit}>
                        <Grid align='center'>
                            <h2 style={welocmeField}> Create Your Account</h2>
                            <h4 style={fillDetailMessage}>Please fill all fields to continue</h4>
                        </Grid>

                        <Grid container spacing={8}>

                            <Grid item xs={6}>
                                <TextField style={{ marginTop: 0 }} required id="outlined-required" label="First Name"
                                    error={firstNameError} onChange={(e) => { setFirstName(e.target.value) }} />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField style={{ marginTop: 0 }} required id="outlined-required" label="Last Name"
                                    error={lastNameError} onChange={(e) => { setLastName(e.target.value) }} />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField style={{ marginTop: -40, width: '100%' }} required id="outlined-required" label="email"
                                    error={emailError} onChange={(e) => { setEmail(e.target.value) }} />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField type="password" style={{ marginTop: -60, width: '100%' }} required id="outlined-required" label="password"
                                    error={passwordError} onChange={(e) => { setPassword(e.target.value) }} />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField type="number" style={{ marginTop: -70, width: '100%' }} required id="outlined-required" label="Pincode"
                                    error={pincodeError} onChange={(e) => { setPincode(e.target.value) }} />
                            </Grid>
                            <Grid item xs={12}>
                                <FormControl fullWidth style={{ marginTop: -60, marginBottom: 20 }}>
                                    <InputLabel id="demo-simple-select-standard-label" required error={roleError}>Role</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={role}
                                        label="Role"
                                        onChange={(e) => { setRole(e.target.value) }}
                                    >
                                        <MenuItem value={1}>Customer</MenuItem>
                                        <MenuItem value={2}>Farmer</MenuItem>
                                        <MenuItem value={3}>Expert</MenuItem>

                                    </Select>

                                    {role === 3 ? <FormHelperText> Expert role will be added after verification</FormHelperText> :
                                        <FormHelperText>Verification may take time</FormHelperText>}

                                </FormControl>
                            </Grid>

                        </Grid>

                        <Grid align='center' >

                            <Button type="submit" style={{ width: '270px', backgroundColor: 'rgb(50, 150, 70)', color: 'rgb(255, 255, 255)' }}
                                onClick={signupUser}> Create Account </Button>

                        </Grid>
                    </form>

                    <Grid align='center'>
                        <div>
                            <h1 style={signUpFont}>Already have an account? <Link to="/signin"
                                style={{ color: 'black', borderTopColor: 'rgb(43, 52, 69)' }}> Sign in </Link>  </h1>

                            {/* <h1 style={signUpFont}>Forgot your password? <Link to="/reset"
                                style={{ color: 'black', borderTopColor: 'rgb(43, 52, 69)' }}> Reset it </Link>  </h1> */}
                        </div>
                    </Grid>

                </Paper>
            </Grid>


        </div>
    )
}
