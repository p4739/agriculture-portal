const db = require('mysql2/promise')

const connection = db.createPool({
  port: 3306,
  database: 'agricultureportal',
  user: 'root',
  password: 'root',
  connectionLimit: 10,
  waitForConnections: true,
  queueLimit: 0,
})

module.exports = connection
