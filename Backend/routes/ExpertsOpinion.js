const { request, response } = require('express')
const express = require('express')
const db = require('../db')

const router = express.Router()

//fetch question specific details and the user details of querier and expert
router.post('/fetch-question-specific-details', async (request, response) => {
  const { questionId } = request.body
  const fetchQuestionQuery =
    'select * from question where queId = ' + questionId
  try {
    const QuestionDetails = await db
      .execute(fetchQuestionQuery)
      .catch((err) => {
        error = err
        console.log(err.sqlMessage)
        // console.log(err)
        console.log(error)

        throw err
      })

    let responseToSend = QuestionDetails[0][0]
    //console.log(responseToSend)
    const expertId = responseToSend.expertId
    const querierId = responseToSend.querierId
    const fetchUserDetailQuery =
      'select firstName, lastName from user where userId = '
    const fetchQuerierResult = await db
      .execute(fetchUserDetailQuery + querierId)
      .catch((err) => {
        error = err
        console.log(err.sqlMessage)
        // console.log(err)
        console.log(error)

        throw err
      })
    responseToSend.querierId = fetchQuerierResult[0][0]
    if (expertId != null) {
      const fetchExpertResult = await db
        .execute(fetchUserDetailQuery + expertId)
        .catch((err) => {
          error = err
          console.log(err.sqlMessage)
          // console.log(err)
          console.log(error)

          throw err
        })
      responseToSend.expertId = fetchExpertResult[0][0]
    }
    //console.log(responseToSend)
    response.send({
      status: 'success',
      result: responseToSend,
    })
  } catch (error) {
    if (error.sqlMessage != null) {
      response.send({
        status: 'error',
        error: error.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: error,
      })
    }

    response.end()
  }
})

//insert comment for a specific question
router.post('/insert-comment', async (request, response) => {
  const { comment, userId, questionId } = request.body
  const insertCommentQuery =
    'insert into comment (questionId, userId, comment) values (' +
    questionId +
    ',' +
    userId +
    ',"' +
    comment +
    '")'
  //console.log(insertCommentQuery)

  try {
    const insertCommentResult = await db
      .execute(insertCommentQuery)
      .catch((err) => {
        error = err
        console.log(err.sqlMessage)
        // console.log(err)
        console.log(error)

        throw err
      })
    //console.log(insertCommentResult[0].affectedRows)
    if (insertCommentResult[0].affectedRows == 1) {
      response.send({
        status: 'success',
        message: 'Added comment successfully',
      })
    } else {
      throw 'Could not add the comment'
    }
  } catch (error) {
    if (error.sqlMessage != null) {
      response.send({
        status: 'error',
        error: error.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: error,
      })
    }

    response.end()
  }
})

//fetch question specific comment and details of the user who added the comment
router.post('/fetch-question-specific-comment', async (request, response) => {
  const { questionId } = request.body
  const fetchCommentQuery =
    'select * from comment where questionId = ' + questionId
  //console.log(fetchCommentQuery)
  try {
    const commentResult = await db.execute(fetchCommentQuery).catch((err) => {
      error = err
      console.log(err.sqlMessage)
      // console.log(err)
      console.log(error)

      throw err
    })
    //console.log(commentResult[0])
    let fetchUserDetailsQuery =
      'select userId, firstName, lastName from user where userId = '
    for (let index = 0; index < commentResult[0].length; index++) {
      let userId = commentResult[0][index].userId
      fetchUserDetailsQuery = fetchUserDetailsQuery + userId
      //console.log(fetchUserDetailsQuery)
      let UserDetailResult = await db
        .execute(fetchUserDetailsQuery)
        .catch((err) => {
          error = err
          console.log(err.sqlMessage)
          // console.log(err)
          console.log(error)

          throw err
        })
      //console.log(UserDetailResult[0][0])
      commentResult[0][index].userId = UserDetailResult[0][0]
      fetchUserDetailsQuery =
        'select userId, firstName, lastName from user where userId = '
    }
    //console.log(commentResult[0])
    if (commentResult[0].length == 0) {
      throw 'No comments for the given question'
    } else {
      response.send({
        status: 'success',
        comment: commentResult[0],
      })
    }
  } catch (error) {
    if (error.sqlMessage != null) {
      response.send({
        status: 'error',
        error: error.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: error,
      })
    }

    response.end()
  }
})

//Save answer added by expert
router.put('/save-answer', async (request, response) => {
  const { questionId, userId, answer } = request.body
  const updateAnswerQuery =
    'update question set answer = "' +
    answer +
    '", expertId = ' +
    userId +
    ' where queId = ' +
    questionId
  //console.log(updateAnswerQuery)
  try {
    const fetchAnswerUpdateResult = await db
      .execute(updateAnswerQuery)
      .catch((err) => {
        error = err
        console.log(err.sqlMessage)
        // console.log(err)
        console.log(error)

        throw err
      })
    //console.log(fetchAnswerUpdateResult[0].affectedRows)
    if ((fetchAnswerUpdateResult[0].affectedRows = 1)) {
      response.send({
        status: 'success',
        message: 'Added answer successfully',
      })
    } else {
      throw 'Could not add the answer'
    }
  } catch (error) {
    if (error.sqlMessage != null) {
      response.send({
        status: 'error',
        error: error.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: error,
      })
    }

    response.end()
  }
})

//delete comment specific to user
router.post('/delete-comment', async (request, response) => {
  const { commentId } = request.body
  const deleteCommentQuery =
    'delete from comment where commentId = ' + commentId
  try {
    const deleteCommentResult = await db
      .execute(deleteCommentQuery)
      .catch((err) => {
        error = err
        console.log(err.sqlMessage)
        // console.log(err)
        console.log(error)

        throw err
      })
    //console.log(deleteCommentResult[0])
    if (deleteCommentResult[0].affectedRows == 1) {
      response.send({
        status: 'success',
        message: 'Deleted comment successfully',
      })
    } else {
      throw 'Could not delete the comment'
    }
  } catch (error) {
    if (error.sqlMessage != null) {
      response.send({
        status: 'error',
        error: error.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: error,
      })
    }

    response.end()
  }
})
module.exports = router
