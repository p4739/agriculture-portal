const express = require("express");
const db = require("../db");
const utils = require("../utils");
const jwt = require("jsonwebtoken");
const cryptoJs = require("crypto-js");

// router which helps user module to add all the routes to the main app
const router = express.Router();

router.post('/signin', async (request, response) => {
  const { email, password } = request.body

  // encrypt the password
  const encryptedPassword = cryptoJs.SHA512(password);
  console.log(email + password);
  const query = `select userId, firstName, lastName, state, pincodeId, roleId from vw_user where 
            email = '${email}' and password = '${encryptedPassword}'`;

  var error = null
  var result = null

  try {
    result = await db.execute(query).catch((err) => {
      error = err
      console.log(err.sqlMessage)
      // console.log(err)
      console.log(error)

      //throw err
    })
  } catch (error) {
    response.send(utils.createResult(error, result))
  }

  if (result.length === 0) {
    // there is no user matching the criteria
    response.send(utils.createResult('user not found'))
  } else {
    // console.log(result[0]);
    const user = result[0]

    //add access token in responce and add to session storatge in react app
    const data = utils.createResult(null, user)
    console.log(data)
    //creating single access token using .env file(secret key)
    const accessToken = jwt.sign(data, "5d33a76ea83bc95f730d2b4bc5914137331c31e44620424a14c689fc1514643c00041446700b519f7671aa3ffbdbe38f9168250781dbddb785d2a75573aa3a02")

    response.json({ accessToken: accessToken })
  }
})

router.post('/signup', async (request, response) => {
  const { firstName, lastName, email, password, role, pincode } = request.body

  // check if the email sent by user already exists in the table
  const emailStatement = `select email from vw_user where email = '${email}'`;

  var error = null
  var result = null

  try {
    result = await db.execute(emailStatement).catch((err) => {
      error = err
      console.log(err.sqlMessage)
      // console.log(err)
      console.log(error)

     // throw err
    })
  } catch (error) {
    response.send(utils.createResult(error, result))
  }

  if (result[0].length === 0) {
    //check if pincode exist in db
    const pincodeExistQuery = `select pincodeId from pincode where pincodeId = ${pincode}`

    var res = null
    var innerErr = null
    try {
      res = await db.execute(pincodeExistQuery).catch((err) => {
        innerErr = err
        console.log(err.sqlMessage)
        // console.log(err)
        console.log(innerErr)

        //throw err
      })
    } catch (innerErr) {
      response.send(utils.createResult(innerErr, res))
    }

    if (res[0].length === 0) {
      //pincode doesnt exist in db
      //respose error we are not available in given pincode
      response.send(
        utils.createResult('Sorry we are not available at given Pincode !!')
      )
    } else {
      //if pincode already present then directly add the user
      //encrypt the password
      const encryptedPassword = cryptoJs.SHA512(password)
      // there is no user registered with this email
      const statement = `insert into user(firstName, lastName, email, password,roleId,pincodeId)
            values('${firstName}', '${lastName}', '${email}', '${encryptedPassword}',${role},${pincode})`

      var r = null
      var e = null
      try {
        r = await db.execute(statement).catch((err) => {
          e = err
          console.log(err.sqlMessage)
          // console.log(err)
          console.log(e)

         // throw err
        })
      } catch (e) {
        response.send(utils.createResult(e, r))
      }
      response.send(utils.createResult(e, r))
    }
  } else {
    response.send(
      utils.createResult('email address already exists, please use another')
    )
  }

  // -------------------------
})

router.put('/edit-profile', async (request, response) => {
  console.log('/edit-profile Getting called ')
  const {
    userId,
    firstName,
    lastName,
    email,
    password,
    contactNo,
    pincode,
    addressLine1,
    addressLine2,
  } = request.body;
  console.log(request.body);

  const encryptedPassword = cryptoJs.SHA512(password);

  //check if pincode exist in db
  const pincodeExistQuery = `select pincodeId from pincode where pincodeId = ${pincode}`;

  var error = null
  var result = null

  try {
    result = await db.execute(pincodeExistQuery).catch((err) => {
      error = err
      console.log(err.sqlMessage)
      // console.log(err)
      console.log(error)

      //throw err
    })
  } catch (error) {
    response.send(utils.createResult(error, result))
  }

  console.log(result[0])
  if (result[0].length === 0) {
    //pincode doesnt exist in db
    //respose error we are not available in given pincode
    response.send(
      utils.createResult('Sorry we are not available at given Pincode !!')
    )
  } else {
    const updateUserDetails = `update user set firstName='${firstName}',lastName='${lastName}',
            email = '${email}', password='${encryptedPassword}',contactNo='${contactNo}',pincodeId='${pincode}'    
            where userId=${userId}`;

    var res = null
    var innerErr = null

    try {
      res = await db.execute(updateUserDetails).catch((err) => {
        innerErr = err
        console.log(err.sqlMessage)
        // console.log(err)
        console.log(innerErr)

        //throw err
      })
    } catch (innerErr) {
      response.send(utils.createResult(innerErr, res))
    }

    const ifAddressExist = `select * from address where userId=${userId}`
    var r = null
    var e = null

    try {
      r = await db.execute(ifAddressExist).catch((err) => {
        e = err
        console.log(err.sqlMessage)
        // console.log(err)
        console.log(e)

        //throw err
      })
    } catch (e) {
      response.send(utils.createResult(e, r))
    }

    if (r[0].length === 0) {
      //user has not saved the address , insert the address
      var insertRes
      var insertErr

      const insertUserAddress = `insert into address(addressLine1,addressLine2,userId)
        values('${addressLine1}','${addressLine2}','${userId}')`

      try {
        insertRes = await db.execute(insertUserAddress).catch((err) => {
          insertErr = err
          console.log(err.sqlMessage)
          // console.log(err)
          console.log(e)

          //throw err
        })
      } catch (insertErr) {
        response.send(utils.createResult(e, r))
      }
      response.send(utils.createResult(insertErr, insertRes))
    } //user has already saved addres now update it
    else {
      var insertRes
      var insertErr

      const updateAddress = `update address set addressLine1='${addressLine1}',
        addressLine2='${addressLine2}' where userId=${userId}`
      try {
        insertRes = await db.execute(updateAddress).catch((err) => {
          insertErr = err
          console.log(err.sqlMessage)
          // console.log(err)
          console.log(e)

          //throw err
        })
      } catch (insertErr) {
        response.send(utils.createResult(e, r))
      }
      response.send(utils.createResult(insertErr, insertRes))
    }
  }
})

router.get('/get-details/:userId', async (request, response) => {
  console.log('Hello from get details')
  const { userId } = request.params
  console.log(userId)
  const getUserDetailsQuery = `select firstName, lastName, email, contactNo, pincodeId, 
    addressLine1,addressLine2,landmark from vw_user where userId=${userId}`;

  var error = null
  var result = null

  try {
    result = await db.execute(getUserDetailsQuery).catch((err) => {
      error = err
      console.log(err.sqlMessage)
      // console.log(err)
      console.log(error)

      //throw err
    })
  } catch (error) {
    response.send(utils.createResult(error, result))
  }

  response.send(utils.createResult(error, result))
  //-----------------------------------------------------
})
//   // console.log(getUserDetailsQuery);
//   var error = null;
//   const result = await db.query(getUserDetailsQuery).catch((err) => {
//     error = err;
//   });
//   console.log(result[0]);
//   response.send(utils.createResult(error, result[0]));
// });

async function executeQueryAndReturnPromise(query) {
  return db.execute(query);
}

async function addressExist(userId) {
  const check = `select userId from address where userId=${userId}`;
  console.log(check);
  const [address] = await executeQueryAndReturnPromise(check).catch((err) => {
    console.log(err);
    //throw err;
  });
  console.log(address.length);
  return address.length > 0;
}

router.post("/update-address", async (request, response) => {
  const { userId, addline1, addline2, landmark, pincodeId, contact } =
    request.body;

  if (await addressExist(userId)) {
    const updateAddress = `update address SET addressLine1='${addline1}',addressLine2='${addline2}',landmark='${landmark}' where userId=${userId}`;
    let error = null;
    const result = await executeQueryAndReturnPromise(updateAddress).catch(
      (err) => {
        console.log(err);
        error = err;
      }
    );
    console.log(error);
    if (error == null) {
      const updatePincode = `Update user SET pincodeID=${pincodeId},contactNo=${contact} where userId=${userId}`;
      let error1 = null;
      const result = await executeQueryAndReturnPromise(updatePincode).catch(
        (err) => {
          console.log(err);
          error1 = err;
        }
      );
      response.send(utils.createResult(error1, result));
    } else {
      response.send(utils.createResult(error, result));
    }
  } else {
    const insertAddress = `insert into address (addressLine1,addressLine2,userId,landmark) values ('${addline1}','${addline2}',${userId},'${landmark}')`;
    console.log(insertAddress);
    let error = null;
    const result = await executeQueryAndReturnPromise(insertAddress).catch(
      (err) => {
        console.log(err);
        error = err;
      }
    );
    if (error == null) {
      const updatePincode = `Update user SET pincodeID=${pincodeId},contactNo=${contact} where userId=${userId}`;
      console.log(updatePincode);
      let error1 = null;
      const result = await executeQueryAndReturnPromise(updatePincode).catch(
        (err) => {
          console.log(err);
          error1 = err;
        }
      );
      response.send(utils.createResult(error1, result));
    } else {
      response.send(utils.createResult(error, result));
    }
  }
});

router.post("/fetch-pincode-API", async (request, response) => {
  const { pincodeId } = request.body;
  // console.log(pincodeId);
  const fetchDataQuery = `select state,district,city from pincode where pincodeId=${pincodeId}`;
  try {
    const result = await executeQueryAndReturnPromise(fetchDataQuery).catch(
      (err) => {
        // throw err;
      }
    );
    response.send(utils.createResult(null, result));
  } catch (err) {
    response.send(utils.createResult(err));
  }
});

router.get("/fetch-pincodes", async (request, response) => {
  // console.log(pincodeId);
  const fetchDataQuery = `select pincodeId from pincode`;
  try {
    const result = await executeQueryAndReturnPromise(fetchDataQuery).catch(
      (err) => {
        // throw err;
      }
    );
    response.send(utils.createResult(null, result));
  } catch (err) {
    response.send(utils.createResult(err));
  }
});
// used to export the router which has all the apis added
module.exports = router;
