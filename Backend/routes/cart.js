const { request, response } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

async function executeQueryAndReturnPromise(query) {
  return db.execute(query)
}

async function fetchImages(results) {
  // console.log(results)
  for await (let result of results) {
    var imageString = result.imageId
    if (imageString != null) {
      var imagesArray = imageString.split('#')
      //console.log(imagesArray);
      var whereClauseForImages = parseInt(imagesArray[0])

      //if we want multiple images go for this logic and change In query from whereClauseForImages to finalWhereClauseForImages

      /* for await (let image of imagesArray) {
        whereClauseForImages = whereClauseForImages + parseInt(image) + ",";
      }
      var finalWhereClauseForImages = whereClauseForImages.slice(0, -1);
      //console.log(finalWhereClauseForImages); */

      let imageQuery =
        'select link from image where imageId IN (' + whereClauseForImages + ')'
      const images = await executeQueryAndReturnPromise(imageQuery).catch(
        (err) => {
          console.log(err.sqlMessage)
          throw err
        }
      )
      // console.log(images[0][0].link);
      //for getting imageId in terms of array
      // result.imageId = images[0];
      result.imageId = images[0][0].link
      console.log(result)
    }
  }
  //generate where clause
  //console.log(results);
  return results
}

router.post('/fetch-cart', async (request, response) => {
  const { userId } = request.body
  let query =
    "select c.cartId,p.subCategoryId,p.varietyId,if(isnull(p.varietyId),sb.price,v.price) as price,if(isnull(p.varietyId),sb.name,CONCAT(sb.name,'[ ', v.name,' ]')) as name,sb.imageId,c.quantity from cart c left join product p ON c.productId=p.productId left join sub_category sb ON sb.subCategoryId=p.subCategoryId left join variety v ON v.varietyId = p.varietyId where c.userId = " +
    userId
  try {
    const result = await executeQueryAndReturnPromise(query).catch((err) => {
      throw err
    })
    //console.log(result[0]);
    if (result[0].length > 0) {
      result[0] = await fetchImages(result[0])
      //console.log(result[0]);
      response.send(utils.createResult(null, result))
    } else {
      response.send({
        status: 'success',
        result: 'No items in the Cart',
      })
    }
  } catch (err) {
    response.send({
      status: 'error',
      error: err.sqlMessage,
    })
  }
})

router.delete('/remove-item', async (request, response) => {
  const { cartId } = request.body
  const removeItem = `delete from cart where cartId=${cartId}`
  var error = null
  const result = await executeQueryAndReturnPromise(removeItem).catch((err) => {
    console.log(err)
    error = err
  })
  response.send(utils.createResult(error, result))
})

router.post('/validate-quantity', async (request, response) => {
  const { userId } = request.body
  const validate = `select c.cartId,c.quantity as cartQ,p.quantity productQ from cart c left join product p on c.productId=p.productId where c.userId=${userId} AND c.quantity > p.quantity`
  var error = null
  const result = await executeQueryAndReturnPromise(validate).catch((err) => {
    console.log(err)
    error = err
  })
  /* response.send(utils.createResult(error, result)); */
  // console.log(result[0]);
  console.log(result[0].length)

  if (error == null) {
    if (result[0].length === 0) {
      response.send({
        status: 'success',
        data: 'validated',
      })
    } else {
      response.send({
        status: 'success',
        data: 'not sufficient product available',
      })
    }
  } else {
    response.send(utils.createResult(error))
  }
})

//fetch cart quantity
router.post('/fetch-cart-quantity', async (request, response) => {
  const { userId } = request.body
  const fetchCategoryUrl =
    'select p.subCategoryId, s.hasVariety from cart c ' +
    'left join product p on c.productId=p.productId ' +
    'left join sub_category s on p.subCategoryId = s.subCategoryId ' +
    'where c.userId = ' +
    userId +
    ' group by p.subCategoryId'

  try {
    const fetchCategoryResult = await db
      .execute(fetchCategoryUrl)
      .catch((err) => {
        //console.log(err.sqlMessage)
        throw err
      })
    let count_to_send = 0
    count_to_send = fetchCategoryResult[0].length
    //console.log(fetchCategoryResult[0])
    //console.log(count_to_send)

    for (let index = 0; index < fetchCategoryResult[0].length; index++) {
      if (fetchCategoryResult[0][index].hasVariety == 1) {
        count_to_send = count_to_send - 1
        let fetchVarietyCountUrl =
          'select p.varietyId from cart c left join product p on c.productId = p.productId where p.subCategoryId = ' +
          fetchCategoryResult[0][index].subCategoryId +
          ' and c.userId =' +
          userId +
          ' group by p.varietyId '
        let fetchVarietyCountResult = await db
          .execute(fetchVarietyCountUrl)
          .catch((err) => {
            //console.log(err.sqlMessage)
            throw err
          })
        //console.log(fetchVarietyCountResult[0])
        count_to_send = count_to_send + fetchVarietyCountResult[0].length
        fetchVarietyCountUrl = ''
      }
    }
    //console.log('count: ' + count_to_send)
    response.send({
      status: 'success',
      count: count_to_send,
    })
  } catch (error) {
    if (error.sqlMessage != null) {
      response.send({
        status: 'error',
        error: error.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: error,
      })
    }

    response.end()
  }
})
module.exports = router
