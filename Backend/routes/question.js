const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

async function executeQueryAndReturnPromise(query) {
  return db.execute(query);
}

router.get("/fetch-question-and-answer/:queId", async (request, response) => {
  const { queId } = request.params;
  const fetchQuetionAndAnswer = `select q.queId,q.question,q.answer,q.createdTimestamp,u.firstname,u.lastname from question q inner join user u ON q.querierId=u.userId where q.queID=${queId}`;
  let error = null;
  const result = await db.execute(fetchQuetionAndAnswer).catch((err) => {
    console.log(err.sqlMessage);
    error = err;
  });
  response.send(utils.createResult(error, result));
});

router.post("/add-question", async (request, response) => {
  const { userId, question } = request.body;
  console.log(userId + question);
  const insertQuestion = `insert into question (querierId,question) values (${userId},'${question}')`;
  console.log(insertQuestion);
  let error = null;
  const result = await executeQueryAndReturnPromise(insertQuestion).catch(
    (err) => {
      console.log(err.sqlMessage);
      error = err;
    }
  );
  response.send(utils.createResult(error, result));
});

module.exports = router;
