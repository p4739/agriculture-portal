const { response, request } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//execute the query and return promise
async function executeQueryAndReturnPromise(query) {
  return db.execute(query)
}

//Add item to the cart
router.post('/add-item-to-cart', async (request, response) => {
  const { subCategoryId, varietyId, userId } = request.body
  //console.log(subCategoryId, varietyId, userId)
  let fetchProductSqlQuery = 'select * from product '
  let whereClause = ''
  //Dynamic where clause
  if (subCategoryId != null || varietyId != null) {
    whereClause = 'where '
    if (subCategoryId != null && varietyId == null) {
      whereClause = whereClause + 'subCategoryId = ' + subCategoryId
    } else if (subCategoryId == null && varietyId != null) {
      whereClause = whereClause + 'varietyId = ' + varietyId
    } else if (subCategoryId != null && varietyId != null) {
      whereClause =
        whereClause +
        'subCategoryId = ' +
        subCategoryId +
        ' and varietyId = ' +
        varietyId
    } else {
      whereClause = ''
    }
    fetchProductSqlQuery = fetchProductSqlQuery + whereClause
    //console.log(fetchProductSqlQuery)
  }
  try {
    //fetching if product exist
    const productQueryResult = await executeQueryAndReturnPromise(
      fetchProductSqlQuery
    ).catch((err) => {
      //console.log(err.sqlMessage)
      throw err
    })

    let products = productQueryResult[0]
    for await (let product of products) {
      // console.log(product)
      // console.log(product.quantity)
      if (product.quantity > 0) {
        let productId = product.productId
        let productQuantity = product.quantity
        let checkProductInCartQuery =
          'select * from cart where productId = ' +
          productId +
          ' and userId = ' +
          userId
        //console.log(checkProductInCartQuery)
        const cartQueryResult = await executeQueryAndReturnPromise(
          checkProductInCartQuery
        ).catch((err) => {
          //console.log(err.sqlMessage)
          throw err
        })

        let cartItem = cartQueryResult[0]
        //console.log(cartItem)

        if (cartItem.length > 0) {
          //Product exist in cart
          //console.log(cartItem[0].quantity)
          let cartProductQuantity = cartItem[0].quantity
          const cartId = cartItem[0].cartId
          //The cart product quantity is less than the product quantity
          if (cartProductQuantity < productQuantity) {
            cartProductQuantity = cartProductQuantity + 1
            const updateCartQuantityQuery =
              'update cart set quantity = ' +
              cartProductQuantity +
              ' where cartId = ' +
              cartId
            //update the cart
            //console.log(updateCartQuantityQuery)
            const updateCartQueryResult = await executeQueryAndReturnPromise(
              updateCartQuantityQuery
            ).catch((err) => {
              //console.log(err.sqlMessage)
              throw err
            })
            response.send({
              status: 'success',
              result: 'Updated the cart quantity by 1',
            })
            break
          }
        }
        //Product doesn't exist in cart
        else {
          //console.log('no item present in cart')
          const insertProductInCartQuery = `INSERT INTO cart  VALUES (default,${productId}, 1, ${userId})`
          //console.log(insertProductInCartQuery)
          const insertQueryResult = await executeQueryAndReturnPromise(
            insertProductInCartQuery
          ).catch((err) => {
            //console.log(err.sqlMessage)
            throw err
          })
          response.send({
            status: 'success',
            result: 'Added the item in cart',
          })
          break
        }
      }
    }
    //If product quantity has been maxed out by one user
    if (!response.headersSent) {
      response.send({
        status: 'error',
        result: 'Not enough quantity is available',
      })
    }
  } catch (err) {
    response.send({
      status: 'error',
      error: err.sqlMessage,
    })
    response.end()
  }
  response.end()
})

//Delete item from cart
router.post('/del-item-from-cart', async (request, response) => {
  const { subCategoryId, varietyId, userId } = request.body
  //console.log(subCategoryId, varietyId, userId)
  let fetchProductSqlQuery = 'select * from product '
  let whereClause = ''
  //Dynamic where clause
  if (subCategoryId != null || varietyId != null) {
    whereClause = 'where '
    if (subCategoryId != null && varietyId == null) {
      whereClause = whereClause + 'subCategoryId = ' + subCategoryId
    } else if (subCategoryId == null && varietyId != null) {
      whereClause = whereClause + 'varietyId = ' + varietyId
    } else if (subCategoryId != null && varietyId != null) {
      whereClause =
        whereClause +
        'subCategoryId = ' +
        subCategoryId +
        ' and varietyId = ' +
        varietyId
    } else {
      whereClause = ''
    }
    fetchProductSqlQuery = fetchProductSqlQuery + whereClause
    //console.log(fetchProductSqlQuery)
  }
  try {
    //fetching if product exist
    const productQueryResult = await executeQueryAndReturnPromise(
      fetchProductSqlQuery
    ).catch((err) => {
      //console.log(err.sqlMessage)
      throw err
    })

    let products = productQueryResult[0]
    for await (let product of products) {
      // console.log(product)
      // console.log(product.quantity)
      let productId = product.productId
      let checkProductInCartQuery =
        'select * from cart where productId = ' +
        productId +
        ' and userId = ' +
        userId
      //console.log(checkProductInCartQuery)
      const cartQueryResult = await executeQueryAndReturnPromise(
        checkProductInCartQuery
      ).catch((err) => {
        //console.log(err.sqlMessage)
        throw err
      })
      let cartItem = cartQueryResult[0]
      if (cartItem.length > 0) {
        //Product exist in cart
        let cartProductQuantity = cartItem[0].quantity
        const cartId = cartItem[0].cartId
        if (cartProductQuantity > 1) {
          //reduce the cart quantity by 1
          cartProductQuantity = cartProductQuantity - 1
          const updateCartQuantityQuery =
            'update cart set quantity = ' +
            cartProductQuantity +
            ' where cartId = ' +
            cartId
          //console.log(updateCartQuantityQuery)
          const updateCartQueryResult = await executeQueryAndReturnPromise(
            updateCartQuantityQuery
          ).catch((err) => {
            //console.log(err.sqlMessage)
            throw err
          })
          response.send({
            status: 'success',
            result: 'Reduced the cart quantity by 1',
          })
          break
        } else {
          //only 1 quantity is present of that product so remove that product from cart
          const removeProductInCartQuery = `delete from cart where cartId = ${cartId}`
          //console.log(removeProductInCartQuery)
          const insertQueryResult = await executeQueryAndReturnPromise(
            removeProductInCartQuery
          ).catch((err) => {
            //console.log(err.sqlMessage)
            throw err
          })
          response.send({
            status: 'success',
            result: 'Removed the item in cart',
          })
          break
        }
      }
    }
    //If product doesnot exist in cart
    if (!response.headersSent) {
      response.send({
        status: 'error',
        result: 'Product doesnot exist in cart',
      })
    }
  } catch (err) {
    response.send({
      status: 'error',
      error: err.sqlMessage,
    })
    response.end()
  }
  response.end()
})

//Check if item is available for anonymous user and logged in user
router.post('/check-item-availability', async (request, response) => {
  const { subCategoryId, varietyId, userId } = request.body
  let fetchProductSqlQuery = 'select quantity, userId from product '
  let whereClause = ''
  let stateOfBuyer = null
  let totalItemQuantityInGivenState = 0
  let totalItemQuantity = 0
  //Dynamic where clause
  if (subCategoryId != null || varietyId != null) {
    whereClause = 'where '
    if (subCategoryId != null && varietyId == null) {
      whereClause = whereClause + 'subCategoryId = ' + subCategoryId
    } else if (subCategoryId == null && varietyId != null) {
      whereClause = whereClause + 'varietyId = ' + varietyId
    } else if (subCategoryId != null && varietyId != null) {
      whereClause =
        whereClause +
        'subCategoryId = ' +
        subCategoryId +
        ' and varietyId = ' +
        varietyId
    } else {
      whereClause = ''
    }
    fetchProductSqlQuery = fetchProductSqlQuery + whereClause
    //console.log(fetchProductSqlQuery)
  }
  try {
    //If user has logged in, fetch the state of that particular user.
    if (userId != null) {
      const fetchStateSqlQuery = `SELECT state FROM vw_user where userId = ${userId}`
      const fetchStateResult = await executeQueryAndReturnPromise(
        fetchStateSqlQuery
      ).catch((err) => {
        //console.log(err.sqlMessage)
        throw err
      })
      stateOfBuyer = fetchStateResult[0][0]
    }

    //fetch all the products from product table
    const productQueryResult = await executeQueryAndReturnPromise(
      fetchProductSqlQuery
    ).catch((err) => {
      //console.log(err.sqlMessage)
      throw err
    })

    let products = productQueryResult[0]
    // console.log(stateOfBuyer)
    for await (let product of products) {
      //if product quantity is available
      if (product.quantity > 0) {
        totalItemQuantity = totalItemQuantity + product.quantity
        //If user has logged in, then show quantity related to that user only
        if (stateOfBuyer != null) {
          let sellerUserId = product.userId
          let fetchSellerUserSqlQuery = `SELECT state FROM vw_user where userId = ${sellerUserId}`
          //console.log(fetchSellerUserSqlQuery)
          const fetchSellerStateResult = await executeQueryAndReturnPromise(
            fetchSellerUserSqlQuery
          ).catch((err) => {
            //console.log(err.sqlMessage)
            throw err
          })
          stateOfSeller = fetchSellerStateResult[0][0]
          //check if state of seller is same as that of state of buyer
          if (stateOfBuyer.state === stateOfSeller.state) {
            totalItemQuantityInGivenState =
              totalItemQuantityInGivenState + product.quantity
          }
        }
      }
    }

    //console.log('logged in user quantity =' + totalItemQuantityInGivenState)
    //console.log('item quantity =' + totalItemQuantity)

    //if no item is available
    if (totalItemQuantity === 0) {
      throw 'Item not available'
    }

    //only for logged in user
    if (totalItemQuantityInGivenState > 0) {
      response.send({
        status: 'success',
        result: 'Item available for the user',
        quantity: totalItemQuantityInGivenState,
      })
    } else {
      //if user has not logged in, then send all the product total quantity
      response.send({
        status: 'success',
        result: 'Item available for all the users',
        quantity: totalItemQuantity,
      })
    }
  } catch (err) {
    //console.log(err)
    if (err.sqlMessage != null) {
      response.send({
        status: 'error',
        error: err.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: err,
      })
    }

    response.end()
  }
  response.end()
})

//fetch the variety by checking if the variety is available to the user or it is for anonymous user
router.post('/fetch-variety', async (request, response) => {
  const { subCategoryId, userId } = request.body
  let productQuantityAnonymous = {}
  let productQuantityOfLoggedInUser = {}
  let fetchProductSqlQuery =
    'select varietyId, userId, quantity from product where subCategoryId = ' +
    subCategoryId
  let varietyOfLoggedInUser = []
  let varietyofAnonymousUser = []
  let fetchVarietySqlQuery =
    'select variety.varietyId as varietyId, variety.name as name, image.link as imageUrl, variety.price as price  from variety left join image on variety.imageId = image.imageId where varietyId in ( '
  let whereClauseForVariety = ''
  let stateOfBuyer = null
  let finalWhereClauseForVariety = ''
  try {
    //If user has logged in, fetch the state of that particular user.
    if (userId != null) {
      const fetchStateSqlQuery = `SELECT state FROM vw_user where userId = ${userId}`
      const fetchStateResult = await executeQueryAndReturnPromise(
        fetchStateSqlQuery
      ).catch((err) => {
        //console.log(err.sqlMessage)
        throw err
      })
      stateOfBuyer = fetchStateResult[0][0]
      //console.log(stateOfBuyer)
    }

    //fetch all the products from product table
    const productQueryResult = await executeQueryAndReturnPromise(
      fetchProductSqlQuery
    ).catch((err) => {
      //console.log(err.sqlMessage)
      throw err
    })

    let products = productQueryResult[0]
    for await (let product of products) {
      //if product quantity is available
      if (product.quantity > 0) {
        if (product.varietyId != null) {
          //Add product quantity in json object for anonymous user and also check if the quantity already exist, if so increment it
          if (product.varietyId in productQuantityAnonymous) {
            productQuantityAnonymous[product.varietyId] =
              productQuantityAnonymous[product.varietyId] + product.quantity
          } else {
            productQuantityAnonymous[product.varietyId] = product.quantity
          }

          //push the variety value in array, later to be used for join
          varietyofAnonymousUser.push(product.varietyId)
          //If user has logged in, then show quantity related to that user only
          if (stateOfBuyer != null) {
            let sellerUserId = product.userId
            let fetchSellerUserSqlQuery = `SELECT state FROM vw_user where userId = ${sellerUserId}`
            //console.log(fetchSellerUserSqlQuery)
            const fetchSellerStateResult = await executeQueryAndReturnPromise(
              fetchSellerUserSqlQuery
            ).catch((err) => {
              //console.log(err.sqlMessage)
              throw err
            })
            //state of seller
            stateOfSeller = fetchSellerStateResult[0][0]

            //check if state of seller is same as that of state of buyer
            if (stateOfBuyer.state === stateOfSeller.state) {
              //Add product quantity in json object for logged in user
              if (product.varietyId in productQuantityOfLoggedInUser) {
                productQuantityOfLoggedInUser[product.varietyId] =
                  productQuantityOfLoggedInUser[product.varietyId] +
                  product.quantity
              } else {
                productQuantityOfLoggedInUser[product.varietyId] =
                  product.quantity
              }
              varietyOfLoggedInUser.push(product.varietyId)
            }
          }
        }
      }
    }
    //check if variety exist (logged in user)
    if (userId != null) {
      if (varietyOfLoggedInUser.length > 0) {
        for await (let variety of varietyOfLoggedInUser) {
          whereClauseForVariety = whereClauseForVariety + variety + ','
        }
        //generate where clause
        finalWhereClauseForVariety = whereClauseForVariety.slice(0, -1)
      } else {
        //finalWhereClauseForVariety = 'null'
        throw 'No variety exist'
      }
    } else if (varietyofAnonymousUser.length > 0) {
      //check if variety exist (anonymous user)
      for await (let variety of varietyofAnonymousUser) {
        whereClauseForVariety = whereClauseForVariety + variety + ','
      }
      //generate where clause
      finalWhereClauseForVariety = whereClauseForVariety.slice(0, -1)
    } else {
      //if no variety exists, throw error
      throw 'No variety exist'
    }

    //create variety sql query
    fetchVarietySqlQuery =
      fetchVarietySqlQuery + finalWhereClauseForVariety + ')'

    //console.log(fetchVarietySqlQuery)
    const fetchVarietyResult = await executeQueryAndReturnPromise(
      fetchVarietySqlQuery
    ).catch((err) => {
      //console.log(err.sqlMessage)
      throw err
    })
    //get all varieties along with image link
    varieties = fetchVarietyResult[0]
    //console.log(varieties)
    //used to add quantity of each variety in object
    for await (let variety of varieties) {
      if (stateOfBuyer != null) {
        variety['quantity'] = await productQuantityOfLoggedInUser[
          variety.varietyId
        ]
      } else {
        variety['quantity'] = await productQuantityAnonymous[variety.varietyId]
      }
    }
    //console.log(varieties)
    response.send({
      status: 'success',
      result: varieties,
    })
  } catch (err) {
    if (err.sqlMessage != null) {
      response.status(405).send({
        status: 'error',
        error: err.sqlMessage,
      })
    } else {
      response.status(200).send({
        status: 'error',
        error: err,
      })
    }

    response.end()
  }
  response.end()
})

//fetch item count from cart if user has logged in
router.post('/fetch-item-count', async (request, response) => {
  const { subCategoryId, varietyId, userId } = request.body
  //console.log(subCategoryId, varietyId, userId)
  let fetchProductSqlQuery = 'select * from product '
  let whereClause = ''
  //Dynamic where clause
  if (subCategoryId != null && userId != null) {
    whereClause = 'where '
    if (subCategoryId != null && varietyId == null) {
      whereClause = whereClause + 'subCategoryId = ' + subCategoryId
    } else if (subCategoryId == null && varietyId != null) {
      whereClause = whereClause + 'varietyId = ' + varietyId
    } else if (subCategoryId != null && varietyId != null) {
      whereClause =
        whereClause +
        'subCategoryId = ' +
        subCategoryId +
        ' and varietyId = ' +
        varietyId
    } else {
      whereClause = ''
    }
    fetchProductSqlQuery = fetchProductSqlQuery + whereClause
    //console.log(fetchProductSqlQuery)
  }

  try {
    //fetching if product exist
    const productQueryResult = await executeQueryAndReturnPromise(
      fetchProductSqlQuery
    ).catch((err) => {
      //console.log(err.sqlMessage)
      throw err
    })

    let products = productQueryResult[0]
    let productIds = ''
    const varExistOrNot = products[0].varietyId == null
    //console.log(varExistOrNot)
    if (varietyId == undefined && varExistOrNot) {
      //console.log('Item with no variety, fetch the product ids')
      for (let i = 0; i < products.length; i++) {
        productIds = productIds + products.at(i).productId + ','
      }
      productIds = productIds.slice(0, -1)
      //console.log(productIds)

      const queryToFetchQuantity =
        'select sum(quantity) as sum from cart where userId = ' +
        userId +
        ' and productId in (' +
        productIds +
        ')'
      //console.log(queryToFetchQuantity)
      //fetching quantity
      const productQuantityResult = await executeQueryAndReturnPromise(
        queryToFetchQuantity
      ).catch((err) => {
        //console.log(err.sqlMessage)
        throw err
      })

      response.send({
        status: 'success',
        quantity: productQuantityResult[0][0].sum,
      })
    } else if (varietyId == undefined && !varExistOrNot) {
      // console.log(
      //   'no variety has been passed but variety exist for that subcategory'
      // )
      throw 'Select the variety'
    } else {
      // console.log(
      //   'variety has been passed, now we need to iterate and check which variety it matches'
      // )
      for (let j = 0; j < products.length; j++) {
        productIds = productIds + products.at(j).productId + ','
      }
      productIds = productIds.slice(0, -1)
      //console.log(productIds)

      const queryToFetchQuantity =
        'select sum(quantity) as sum from cart where userId = ' +
        userId +
        ' and productId in (' +
        productIds +
        ')'
      //console.log(queryToFetchQuantity)
      //fetching quantity
      const productQuantityResult = await executeQueryAndReturnPromise(
        queryToFetchQuantity
      ).catch((err) => {
        //console.log(err.sqlMessage)
        throw err
      })

      //console.log(productQuantityResult[0])
      response.send({
        status: 'success',
        quantity: productQuantityResult[0][0].sum,
      })
    }
  } catch (err) {
    if (err.sqlMessage != null) {
      response.send({
        status: 'error',
        error: err.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: err,
      })
    }

    response.end()
  }
})

//Fetch variety specific details like price etc
router.post('/fetch-variety-details', async (request, response) => {
  const { subCategoryId, varietyId } = request.body

  //console.log(subCategoryId, varietyId)
  if (varietyId == undefined || varietyId == null) {
    response.send({
      status: 'error',
      error: 'No variety selected',
    })
  } else {
    const fetchVarietyDetails =
      'select price from variety where subCategoryId = ' +
      subCategoryId +
      ' and varietyId = ' +
      varietyId
    //console.log(fetchVarietyDetails)
    try {
      const varietyResult = await db
        .execute(fetchVarietyDetails)
        .catch((err) => {
          //console.log(err.sqlMessage)
          throw err
        })
      //console.log(varietyResult[0])
      response.send({
        status: 'success',
        data: varietyResult[0][0],
      })
    } catch (error) {
      if (error.sqlMessage != null) {
        response.send({
          status: 'error',
          error: error.sqlMessage,
        })
      } else {
        response.send({
          status: 'error',
          error: error,
        })
      }

      response.end()
    }
  }
})

//Search the item based on search text. Also if logged in user has requested the item, it will
//check if it is available to the user, if so then only it will return the sub category item.
//For anaonymous user, all items will be sent
router.post('/search-item', async (request, response) => {
  const { userId, searchText } = request.body
  //console.log(userId, searchText)
  let stateOfBuyer = null
  let searchQuery =
    'select sc.subCategoryId, sc.name, sc.categoryId, sc.imageId, sc.description, sc.price, sc.hasVariety, meas.unit from sub_category sc ' +
    'left join measurement meas on sc.measurementId=meas.measurementId ' +
    'where subCategoryId in (select distinct(p.subCategoryId) from product p ' +
    'left join sub_category s on p.subCategoryId = s.subCategoryId ' +
    'left join user u on p.userId = u.userId ' +
    'left join pincode pin on u.pincodeId=pin.pincodeId ' +
    'where p.quantity>0 and s.name like "%' +
    searchText +
    '%")'

  try {
    //If user has logged in, fetch the state of that particular user.
    if (userId != null) {
      const fetchStateSqlQuery = `SELECT state FROM vw_user where userId = ${userId}`
      const fetchStateResult = await executeQueryAndReturnPromise(
        fetchStateSqlQuery
      ).catch((err) => {
        //console.log(err.sqlMessage)
        throw err
      })
      stateOfBuyer = fetchStateResult[0][0].state
      //console.log(stateOfBuyer)
      //whereClause = whereClause + 'and pin.state = "' + stateOfBuyer + '"'
      searchQuery =
        'select sc.subCategoryId, sc.name, sc.categoryId, sc.imageId, sc.description, sc.price, sc.hasVariety, meas.unit from sub_category sc ' +
        'left join measurement meas on sc.measurementId=meas.measurementId ' +
        'where subCategoryId in (select distinct(p.subCategoryId) from product p ' +
        'left join sub_category s on p.subCategoryId = s.subCategoryId ' +
        'left join user u on p.userId = u.userId ' +
        'left join pincode pin on u.pincodeId=pin.pincodeId ' +
        'where p.quantity>0 and s.name like "%' +
        searchText +
        '%" and pin.state = "' +
        stateOfBuyer +
        '")'
    }
    //console.log(searchQuery)
    const searchQueryResult = await db.execute(searchQuery).catch((err) => {
      //console.log(err.sqlMessage)
      throw err
    })
    //console.log(searchQueryResult[0].length)
    let imageIdValues = ''
    var imageLinkResult
    let fetchImageUrl = ''
    for (let index = 0; index < searchQueryResult[0].length; index++) {
      //console.log(index + ' image id ' + searchQueryResult[0][index].imageId)
      let imageString = searchQueryResult[0][index].imageId
      if (imageString != null) {
        let imageIdArray = await imageString.split('#')
        //console.log(imageIdArray)
        for (let j = 0; j < imageIdArray.length; j++) {
          imageIdValues = imageIdValues + imageIdArray[j] + ','
        }
        imageIdValues = imageIdValues.slice(0, -1)
        //console.log(imageIdValues)
        fetchImageUrl = 'SELECT link FROM image where imageId in ('
        fetchImageUrl = fetchImageUrl + imageIdValues + ')'
        imageLinkResult = await db.execute(fetchImageUrl).catch((err) => {
          //console.log(err.sqlMessage)
          throw err
        })
        imageIdValues = ''
        searchQueryResult[0][index].imageId = imageLinkResult[0]
      }
    }
    response.send({
      status: 'success',
      data: searchQueryResult[0],
    })
  } catch (error) {
    if (error.sqlMessage != null) {
      response.send({
        status: 'error',
        error: error.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: error,
      })
    }

    response.end()
  }
})

async function executeQuery(query) {
  return db.execute(query)
}
async function fetchImages(results) {
  // console.log(results)
  for await (let result of results) {
    var imageString = result.imageId
    if (imageString != null) {
      var imagesArray = imageString.split('#')
      // console.log(imagesArray)
      var whereClauseForImages = ''
      for await (let image of imagesArray) {
        debugger
        whereClauseForImages = whereClauseForImages + parseInt(image) + ','
      }
      var finalWhereClauseForImages = whereClauseForImages.slice(0, -1)
      //console.log(finalWhereClauseForImages)

      let imageQuery =
        'select link from image where imageId IN (' +
        finalWhereClauseForImages +
        ')'
      const images = await executeQuery(imageQuery).catch((err) => {
        console.log(err.sqlMessage)
        error = err
        //throw err
      })
      result.imageId = images[0]
      //console.log(result)
    }
  }
  //generate where clause
  // console.log(results)
  return results
}

router.get('/fetch-category-list', async (request, response) => {
  const fetchCategoriesQuery =
    'select c.categoryId,  c.name, i.link as url  ,c.isMachinery from category c inner join image i ON i.imageId = c.imageId'
  var error = null
  let result = null
  try {
    result = await executeQuery(fetchCategoriesQuery).catch((err) => {
      console.log(err.sqlMessage)
      error = err
      throw err
    })
    //console.log(result)
  } catch (err) {
    response.send(utils.createResult(err, result))
  }
  response.send(utils.createResult(error, result))
})

router.post('/fetch-subCategory-list', async (request, response) => {
  const  {userId}  = request.body
  //console.log(userId)
  let subcategoryQuery =
  'select distinct sb.subCategoryId, sb.name, sb.categoryId,sb.imageId,'+
  'sb.description,m.unit, sb.price, sb.hasVariety '+
  'from sub_category sb inner join product p on p.subCategoryId = sb.subCategoryId '+
  'inner join vw_user u on u.userId = p.userId ' +
  'inner join measurement m on m.measurementId = sb.measurementId' 
  let whereClaus = ''
  if (userId != null) {
    whereClaus =
      whereClaus +
      ' where u.state = (select p.state from user u inner join pincode p on p.pincodeId=u.pincodeId where u.userId = ' +
      userId +
      ')'
  }

  subcategoryQuery = subcategoryQuery + whereClaus
   console.log(subcategoryQuery)
  var error = null
  var result = null
  try {
    result = await executeQuery(subcategoryQuery).catch((err) => {
      console.log(err.sqlMessage)
      error = err
      throw err
    })
    //console.log(result)
    var newResults = await fetchImages(result[0])

    result[0] = newResults
  } catch (error) {
    response.send(utils.createResult(error, result))
  }
  response.send(utils.createResult(error, result))
})

module.exports = router
