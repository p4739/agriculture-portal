const express = require('express')
const db = require('../db')
const utils = require('../utils')
const jwt = require('jsonwebtoken')
const cryptoJs = require('crypto-js')
const e = require('express')

const app = express()

// router which helps user module to add all the routes to the main app
const router = express.Router()

//get order of specific user by user id
router.get('/:userId', async (request, response) => {
  const { userId } = request.params
  console.log(userId)
  const getOrdersQuery = `select o.*,os.statusName from orders o inner join order_status os on o.statusId = os.orderStatusId where o.userId= ${userId}`

  var error = null
  var result = null

  try {
    result = await db.execute(getOrdersQuery).catch((err) => {
      error = err
      console.log(err.sqlMessage)
      // console.log(err)
      console.log(error)

      
    })
  } catch (error) {
    response.send(utils.createResult(error, result))
  }

  response.send(utils.createResult(error, result))
})

router.get('/order-details/:orderId', async (request, response) => {
  const { orderId } = request.params
  console.log(orderId)

  const getOrderDetails = `select * from vw_orderdetails where orderId = ${orderId}`

  var error = null
  var result = null

  try {
    result = await db.execute(getOrderDetails).catch((err) => {
      error = err
      console.log(err.sqlMessage)
      // console.log(err)
      console.log(error)

      
    })
  } catch (error) {
    response.send(utils.createResult(error, result))
  }

  response.send(utils.createResult(error, result))
  //----
})

// used to export the router which has all the apis added
module.exports = router
