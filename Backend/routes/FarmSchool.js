const { response } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//Fetch the machinery varieties
router.post('/fetch-machinery-variety', async (request, response) => {
  const { SubCategoryId } = request.body
  const fetchMachineryVarietyQuery =
    'select v.varietyId, v.name, v.price, i.link from variety v left join image i on v.imageId = i.imageId where v.subCategoryId = ' +
    SubCategoryId
  //console.log(fetchMachineryVarietyQuery)
  try {
    const varietyResult = await db
      .execute(fetchMachineryVarietyQuery)
      .catch((err) => {
        //console.log(err.sqlMessage)
        throw err
      })
    //console.log(varietyResult[0])
    response.send({
      status: 'success',
      variety: varietyResult[0],
    })
  } catch (error) {
    if (error.sqlMessage != null) {
      response.send({
        status: 'error',
        error: error.sqlMessage,
      })
    } else {
      response.send({
        status: 'error',
        error: error,
      })
    }

    response.end()
  }
})

async function executeQuery(query) {
  return db.execute(query)
}

async function fetchImages(results) {
  // console.log(results)
  for await (let result of results) {
    var imageString = result.imageId
    if (imageString != null) {
      debugger
      var imagesArray = imageString.split('#')
      // console.log(imagesArray)
      var whereClauseForImages = ''
      for await (let image of imagesArray) {
        debugger
        whereClauseForImages = whereClauseForImages + parseInt(image) + ','
      }
      var finalWhereClauseForImages = whereClauseForImages.slice(0, -1)
      //console.log(finalWhereClauseForImages)

      let imageQuery =
        'select link from image where imageId IN (' +
        finalWhereClauseForImages +
        ')'
      const images = await executeQuery(imageQuery).catch((err) => {
        console.log(err.sqlMessage)
        error = err
        //throw err
      })
      result.imageId = images[0]
      //console.log(result)
    }
  }
  //generate where clause
  // console.log(results)
  return results
}

router.post('/fetch-subtype', async (request, response) => {
  let { categoryId } = request.body
  // console.log(categoryId)
  let fetchSubTypeQuery =
    'select sb.subCategoryId,sb.name,sb.imageId from sub_category sb ' +
    'where sb.categoryId = ' +
    categoryId
  var error = null
  var result = null
  try {
    result = await executeQuery(fetchSubTypeQuery).catch((err) => {
      error = err
      console.log(err.sqlMessage)
      // console.log(err)
      console.log(error)

      throw err
    })
    result[0] = await fetchImages(result[0])
  } catch (error) {
    response.send(utils.createResult(error, result))
  }

  console.log(error)
  response.send(utils.createResult(error, result))
})

router.post('/fetch-crop-detail', async (request, response) => {
  const { subCategoryId } = request.body
  //console.log(subCategoryId)
  const fetchCropDetailsQuery =
    'select sbt.subTopicAgriId, sbt.subCategoryId,sb.name, sbt.imageId, sbt.seasons, sbt.irrigation, sbt.diseases,sbt.weedManagement,sbt.fertiliezersAndPesticides from sub_topic_agri sbt ' +
    'inner join sub_category sb on sb.subCategoryId = sbt.subCategoryId ' +
    'where sbt.subCategoryId = ' +
    subCategoryId

  var error = null
  var result = null
  try {
    result = await executeQuery(fetchCropDetailsQuery).catch((err) => {
      console.log(err.sqlMessage)
      error = err
      throw err
    })
    result[0] = await fetchImages(result[0])
  } catch (error) {
    response.send(utils.createResult(error, result))
  }
  response.send(utils.createResult(error, result))
})

router.post('/fetch-machinery-detail', async (request, response) => {
  //variety instead of subcat
  const { varietyId } = request.body
  //console.log(varietyId+"machinery")
  const fetchMachineryDetailsQuery =
    'select sbt.subTopicMcId, sbt.varietyId,v.name, sbt.imageId, sbt.functions, sbt.specType, sbt.specPower,sbt.specDimension,sbt.specWeight,sbt.specCapacity,sbt.feature,sbt.info from sub_topic_machine sbt ' +
    'inner join variety v on v.varietyId = sbt.varietyId ' +
    'where sbt.varietyId = ' +
    varietyId
  var error = null
  var result = null
  try {
    result = await executeQuery(fetchMachineryDetailsQuery).catch((err) => {
      console.log(err.sqlMessage)
      error = err
      throw err
    })
    result[0] = await fetchImages(result[0])
  } catch (error) {
    response.send(utils.createResult(error, result))
  }
  //console.log(result[0])
  response.send(utils.createResult(error, result))
})

module.exports = router
