const express = require('express');
const db = require('../db');
const utils = require('../utils');
const jwt = require('jsonwebtoken')
const cryptoJs = require('crypto-js');
const e = require('express');

const app = express();

// router which helps user module to add all the routes to the main app
const router = express.Router()

router.get('/:userId', async (request, response) => {

    const { userId } = request.params;
    console.log(userId)
    const getProductsByUserId = `select productId,subName,vName,vPrice,quantity,subPrice,userId,hasVariety from vw_products where userId=${userId}
    and quantity > 0`


    var error = null;
    var result = null;

    try {

        result =
            await db.execute(getProductsByUserId).catch((err) => {
                error = err
                console.log(err.sqlMessage)
                // console.log(err)
                console.log(error)

                
            })


    } catch (error) {
        response.send(utils.createResult(error, result))
    }

    console.log(result[0]);
    response.send(utils.createResult(error, result));

})

router.get('/get/category', async (request, response) => {

    const getAllCategory = `select * from category`;


    var error = null;
    var result = null;

    try {

        result =
            await db.execute(getAllCategory).catch((err) => {
                error = err
                console.log(err.sqlMessage)
                // console.log(err)
                console.log(error)

                
            })


    } catch (error) {
        response.send(utils.createResult(error, result))
    }
    response.send(utils.createResult(error, result));
    //-------------------------------
});

router.get('/get/sub-category/:categoryId', async (request, response) => {

    const { categoryId } = request.params;

    const getSubCategory = `select * from sub_category where categoryId = ${categoryId}`;


    var error = null;
    var result = null;

    try {

        result =
            await db.execute(getSubCategory).catch((err) => {
                error = err
                console.log(err.sqlMessage)
                // console.log(err)
                console.log(error)

                
            })


    } catch (error) {
        response.send(utils.createResult(error, result))
    }
    response.send(utils.createResult(error, result));
    //--------------------------------
});



router.get('/get/variety/:subId', async (request, response) => {

    const { subId } = request.params;

    const getVariety = `select * from variety where subCategoryId = ${subId}`;


    var error = null;
    var result = null;

    try {

        result =
            await db.execute(getVariety).catch((err) => {
                error = err
                console.log(err.sqlMessage)
                // console.log(err)
                console.log(error)

                
            })


    } catch (error) {
        response.send(utils.createResult(error, result))
    }
    response.send(utils.createResult(error, result));
    //-----------------------

});

router.post('/add',async (request, response) => {

    const { subCategory, variety, price, stock, userId } = request.body;

    const insertProduct = `insert into product(subCategoryId,varietyId,quantity,userId,expectedPrice) 
                values(${subCategory},${variety},${stock},${userId},${price})`;

    var error = null;
    var result = null;

    try {

        result =
            await db.execute(insertProduct).catch((err) => {
                error = err
                console.log(err.sqlMessage)
                // console.log(err)
                console.log(error)

                
            })


    } catch (error) {
        response.send(utils.createResult(error, result))
    } 
    response.send(utils.createResult(error, result));
    
});


router.get('/get-product/:productId',async (request, response) => {

    const { productId } = request.params;

    const getProductDetails = `select * from vw_products where productId = ${productId}`;

    var error = null;
    var result = null;

    try {

        result =
            await db.execute(getProductDetails).catch((err) => {
                error = err
                console.log(err.sqlMessage)
                // console.log(err)
                console.log(error)

                
            })


    } catch (error) {
        response.send(utils.createResult(error, result))
    }

    response.send(utils.createResult(error, result));
    //--------------------------------

});



router.put('/edit-product', async(request, response) => {

    const { varietyId, productId, price, stock, userId } = request.body;

    const updateProductQuantity = `update product set quantity=${stock}, expectedPrice = ${price} where productId = ${productId}`;


    var error = null;
    var result = null;

    try {

        result =
            await db.execute(updateProductQuantity).catch((err) => {
                error = err
                console.log(err.sqlMessage)
                // console.log(err)
                console.log(error)

                
            })


    } catch (error) {
        response.send(utils.createResult(error, result))
    }


    response.send(utils.createResult(error, result));

    
});


router.delete('/delete-product/:productId', async(request, response) => {


    const { productId } = request.params;
    console.log(productId);

    const deleteProductQuantity = `update product set quantity= 0  where productId = ${productId}`;

    var error = null;
    var result = null;

    try {

        result =
            await db.execute(deleteProductQuantity).catch((err) => {
                error = err
                console.log(err.sqlMessage)
                // console.log(err)
                console.log(error)

                
            })


    } catch (error) {
        response.send(utils.createResult(error, result))
    }


    response.send(utils.createResult(error, result));    
});




///no use
router.put('/edit-subcategory', (request, response) => {

    const { subCategoryId, productId, price, stock, userId } = request.body;

    const updateProductQuantity = `update product set quantity=${stock} where productId = ${productId}`;

    db.query(updateProductQuantity, (error, result) => {
        if (error) {
            response.send(utils.createResult(error));
        } else {
            const updateSubCategoryPrice = `update sub_category set price =${price} where subCategoryId=${subCategoryId}`

            db.query(updateSubCategoryPrice, (error, result) => {
                if (error) {
                    response.send(utils.createResult(error));
                } else {
                    response.send(utils.createResult(error, result));
                }
            })
        }
    });


});




// used to export the router which has all the apis added
module.exports = router

