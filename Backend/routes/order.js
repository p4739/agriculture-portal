const express = require("express");
const db = require("../db");
const utils = require("../utils");
const jwt = require("jsonwebtoken");
const cryptoJs = require("crypto-js");
const e = require("express");

// router which helps user module to add all the routes to the main app
const router = express.Router();

//get order of specific user by user id
router.get("/:userId", (request, response) => {
  const { userId } = request.params;
  console.log(userId);
  const getOrdersQuery = `select o.*,os.statusName from orders o inner join order_status os on o.statusId = os.orderStatusId where o.userId= ${userId}`;

  db.query(getOrdersQuery, (error, result) => {
    if (error) {
      response.send(utils.createResult(error));
    } else {
      console.log(result);
      response.send(utils.createResult(error, result));
    }
  });
});

router.get("/order-details/:orderId", (request, response) => {
  const { orderId } = request.params;
  console.log(orderId);

  const getOrderDetails = `select * from vw_orderdetails where orderId = ${orderId}`;

  db.query(getOrderDetails, (error, result) => {
    if (error) {
      response.send(utils.createResult(error));
    } else {
      // response.send({"orderId":orderId});
      response.send(utils.createResult(error, result));
    }
  });
});

async function executeQueryAndReturnPromise(query) {
  return db.execute(query);
}

router.post("/place-order", async (request, response) => {
  const { userId } = request.body;

  /*  create view vw_cartdetails as select c.cartId,c.productId,c.userId,if(isnull(p.varietyId),sb.price,v.price) as price,sb.imageId,c.quantity from cart c left join product p ON c.productId=p.productId left join sub_category sb ON sb.subCategoryId=p.subCategoryId left join variety v ON v.varietyId = p.varietyId; */
  // above is the query to create view which gives=> | cartId |productId| userId | price | imageId | quantity |

  try {
    const totalAmountQuery =
      "select sum(price*quantity) as totalAmount from vw_cartdetails where userId=" +
      userId;
    const totalAmountQueryResult = await executeQueryAndReturnPromise(
      totalAmountQuery
    ).catch((err) => {
      //console.log(err, sqlMessage);
      throw err;
    });
    // console.log(totalAmountQueryResult[0][0].totalAmount);
    const cartdetailsQuery =
      "select productId,quantity,price from vw_cartdetails where userId=" +
      userId;
    const [orderDetails] = await db.execute(cartdetailsQuery).catch((err) => {
      throw err;
    });
    //console.log(orderDetails);
    let productId = [];
    let quantity = [];
    let price = [];
    let i = 0;
    for await (let order of orderDetails) {
      productId[i] = order.productId;
      quantity[i] = order.quantity;
      price[i] = parseInt(order.price);
      i = i + 1;
    }
    totalAmount = totalAmountQueryResult[0][0].totalAmount;
    const statusId = 1;
    const paymentMethod = "COD";
    const connection = await db.getConnection();
    try {
      await connection.query("START TRANSACTION");
      const placeOrderQuery = `insert into orders (userID,totalAmount,paymentMethod,statusId) values (${userId},${totalAmount},'${paymentMethod}',${statusId})`;
      const res1 = await connection.query(placeOrderQuery).catch((err) => {
        throw err;
      });
      //console.log(res1);
      for (let i = 0; i < productId.length; i++) {
        const [rows] = await connection.execute(
          "SELECT LAST_INSERT_ID() as orderId"
        );
        orderId = rows[0].orderId;
        //console.log("order created with id " + orderId);
        const updateQuantity =
          "UPDATE product SET quantity = quantity - " +
          quantity[i] +
          " WHERE productId = " +
          productId[i];
        const res2 = await connection.query(updateQuantity).catch((err) => {
          throw err;
        });
        //console.log(res2);
        const insertOrderDetails =
          "insert into order_details (orderId,productId,quantity,productTotalAmount) values (" +
          orderId +
          "," +
          productId[i] +
          "," +
          quantity[i] +
          "," +
          price[i] * quantity[i] +
          ")";
        //console.log(insertOrderDetails);
        const res3 = await connection.query(insertOrderDetails).catch((err) => {
          throw err;
        });
        //console.log(res3);
      }
      const deleteEntries = "delete from cart where userId = " + userId;
      const res4 = await connection.query(deleteEntries).catch((err) => {
        throw err;
      });
      //console.log(res4);
      await connection.query("COMMIT");
      connection.release();
      response.send({
        status: "success",
        result: "Order Placed Successfully",
      });
    } catch (err) {
      await connection.query("ROLLBACK");
      connection.release();
      response.send({
        status: "error",
        result: err.sqlMessage,
      });
    }
  } catch (err) {
    response.send({
      status: "error",
      result: err.sqlMessage,
    });
  }
});

// used to export the router which has all the apis added
module.exports = router;
